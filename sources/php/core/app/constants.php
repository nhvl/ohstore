<?php

define('EMAIL_ADDRESS', 'EMAIL_ADDRESS');
define('USER_ID', 'USER_ID');
define('PASSWORD', 'PASSWORD');
define('IS_ADMIN', 'IS_ADMIN');
define('FULL_NAME', 'FULL_NAME');
define('DEFAULT_LINK', 'DEFAULT_LINK'); //default link for each user
define('ACTIVE', 'ACTIVE'); //active
define('SESSION_MESSAGE', 'SESSION_MESSAGE');
define('FIRSTTIME', 'FIRSTTIME');
define('SECRET_KEY', 'SECRET_KEY');



define('PROJECT_ID', 'PROJECT_ID');
define('PAGE', 'PAGE');
define('ROWS', 'ROWS');
define('ORDERBY', 'ORDERBY');
define('SORT', 'SORT');
define('CLOSED_HIDE', 'CLOSED_HIDE');
define('SET_COOKIE', 'SET_COOKIE');
define('BACK_URL', 'BACK_URL');
define('LOGIN', 'LOGIN');
define('LOGOUT', 'LOGOUT');
define('CLOSED_ISSUE_STATUS_ID', 6);
define('RESOLVED_ISSUE_STATUS_ID', 4);
define('TITLE_CHAR_LIMIT', 50);
define('MAX_FILE_SIZE_UPLOAD', 20000000); //20 MB

define('TIME_OUT_SESSION', 'TIME_OUT_SESSION');

?>
