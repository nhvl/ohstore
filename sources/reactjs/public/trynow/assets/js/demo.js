//do not include in your production env
//for demo purpose Only

setTimeout(function () {
    $(".modal:not(.auto-off)").modal("show");
},200);

(function($) {
    //For demo purpose adding positions to modal for preview

    $(document).on("click","[data-modal-position]",function (e) {
        e.preventDefault();
        //removing previously added classes
        $("#positionModal").removeAttr("class");
        // adding back modal class and the selected position
        $("#positionModal").addClass( "modal fade " + $(this).attr('data-modal-position'));
        //making the modal visible
        $("#positionModal").modal("show");

    })
})(window.jQuery);


$(document).on("click",".open-frame",function (e) {
    if(window.innerWidth > 780){
        e.preventDefault();
        $("#frame").attr("src",$(this).attr("href"));
    }
});
$('a[href^="#license"]').on('click',function (e) {
    e.preventDefault();
    var target = this.hash;
    $target = $(target);
    $('html, body').stop().animate({
        'scrollTop':  $target.offset().top //no need of parseInt here
    }, 900, 'swing', function () {
        window.location.hash = target;
    });
});

function validateMobile(phone) {
  var flag = false;
  phone = phone.replace('(+84)', '0');
  phone = phone.replace('+84', '0');
  phone = phone.replace('0084', '0');
  phone = phone.replace(/ /g, '');
  if (phone != '') {
      var firstNumber = phone.substring(0, 2);
      if ((firstNumber == '09' || firstNumber == '08') && phone.length == 10) {
          if (phone.match(/^\d{10}/)) {
              flag = true;
          }
      } else if (firstNumber == '01' && phone.length == 11) {
          if (phone.match(/^\d{11}/)) {
              flag = true;
          }
      }
  }
  return flag;
}

function validateForm(data) {
  if(!data.phoneNumber) {
    return {status: false, message: "Vui lòng nhập số điện thoại"}
  }
  let result = validateMobile(data.phoneNumber);
  if(!result) return {status: false, message: "Số điện thoại không hợp lệ"}
  return {status: true};
}

function onSubmit() {
  let data = {};
  $('#register-form').serializeArray().forEach(item => {
    data[item.name] = item.value;
  })
  let validation = validateForm(data);
  if(!validation.status) {
    return $('#validateion-message').text(validation.message);
  }
  
  $.ajax({
    headers: {          
      "Accept-Language": "vn"  
    },
    type: "POST",
    url: appConfig.REGISTER_API,
    data: data,
    success: function(result){
      if (!result.status) {
        $('#validateion-message').text(result.message);
      } else {
        window.location.href = appConfig.DEMO_URL;
      }
    },
    error: function() {
      
    }
  });
}

$("form#register-form").on('submit',function(e){
    e.preventDefault();
    onSubmit();
});

