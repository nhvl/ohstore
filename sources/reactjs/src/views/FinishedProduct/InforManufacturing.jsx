import React, { Component, Fragment } from 'react';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import withStyles from "@material-ui/core/styles/withStyles";
import regularFormsStyle from "assets/jss/material-dashboard-pro-react/views/regularFormsStyle.jsx";
import { withTranslation } from 'react-i18next';
import ExtendFunction, { trans } from 'lib/ExtendFunction.js';
import { notifyError } from 'components/Oh/OhUtils.js';
import FinishedEstimatesService from 'services/FinishedEstimatesService.js';
import OhTable from 'components/Oh/OhTable.jsx';
import OhNumberInput from 'components/Oh/OhNumberInput';
import Constants from 'variables/Constants';


class InforManufacturing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      importValues: {},
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.id !== prevProps.id || this.props.importValues[this.props.id] !== prevProps.importValues[prevProps.id]) {
      this.getData();
    }    
    
  }

  componentDidMount() {
    this.getData()
  }

  async getData() {

    const { stock_lists, t, importValues, } = this.props;

    let stocks = [];

    if (stock_lists.length) {
      stock_lists.map(item => stocks.push(item.id));
    }

    const query = {
      id: this.props.id,
      stockId: stocks,
      numberToProduct: importValues && Number(importValues[this.props?.id]?.value),
    };

    let getExpandFinishedEstimates = await FinishedEstimatesService.getExpandFinishedEstimatesDate(query);

    if (getExpandFinishedEstimates.status) {
      this.setState({
        data: getExpandFinishedEstimates.data,
      })
    } else notifyError(t("Lỗi xử lý dữ liệu"))
  }


  handleClick = () => {
    this.props.onChangeImportValue(this.state.importValues);
}

  render() {
    const { t,importStock } = this.props
    let { data } = this.state

    let quantityToProduct=data?.quantityToProduct ? data?.quantityToProduct:0;
    
    let columns1 = importStock ? [
      {
        title: t('Mã'),
        align: 'left',
        width: '16%',
        dataIndex: 'code',
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => a.code.localeCompare(b.code),
        key: "code",
        render: (value) => value
      },
      {
        title: t("NVL"),
        align: 'left',
        width: '40%',
        dataIndex: 'name',
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => trans(a.name, true).localeCompare(trans(b.name, true)),
        render: (value, record) => trans(value)

      },
      {
        title: t("ĐVT"),
        align: 'left',
        width: '12%',
        dataIndex: 'unitName',
        sorter: (a, b) => a.unitName.localeCompare(b.unitName),
        sortDirections: ["descend", "ascend"],
      },
      {
        title: t("Tồn kho"),
        dataIndex: "stockQuantity",
        align: "right",
        width: "15%",
        render: value => {
          return <div className="ellipsis-not-span">{value ? ExtendFunction.FormatNumber(Math.round(value * 100000) / 100000) : "0"}</div>;
        },
      },
      {
        title: t("Tồn kho sản xuất"),
        dataIndex: "manufacturingQuantity",
        align: "right",
        width: "13%",
        render: value => {
          return <div className="ellipsis-not-span">{value ? ExtendFunction.FormatNumber(Math.round(value * 100000) / 100000) : "0"}</div>;
        },
      },
      {
        title: t("SL cần để sx"),
        align: 'right',
        width: 100,
        dataIndex: 'quantityToProduct',
        key: "quantityToProduct",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.quantityToProduct - b.quantityToProduct),
        render: (value, record) => {
          return <span
            style={{ color: record.quantityToProduct > record.manufacturingQuantity ? "red" : null }}
                  >
                    {value ? ExtendFunction.FormatNumber(value) : 0}
                    </span>
        }
      },
      {
        title: t("Số lượng nhập "),
        dataIndex: "inputQuantity",
        align: "right",
        width: "17%",
        sorter: false,
        render: (value, record) => {
          let { importValues } = this.state;
       
          return <OhNumberInput
            defaultValue={(importValues[record.materialId] || {}).value}
            onChange={val => this.setState({
              importValues: {           
                ...importValues,
                [record.materialId]: { value: val, record }
              }             
            }, () => this.handleClick())
          }
            onClick={(e) => {
              e.stopPropagation()
            }}
            isNegative={false}
            permission={{
              name: Constants.PERMISSION_NAME.MANUFACTURE_PRODUCT,
              type: Constants.PERMISSION_TYPE.TYPE_ALL
            }}
          />;
        },
      }
    ]:
    [{
      title: t('Mã'),
      align: 'left',
      width: '16%',
      dataIndex: 'code',
      sortDirections: ["descend", "ascend"],
      sorter: (a, b) => a.code.localeCompare(b.code),
      key: "code",
      render: (value) => value
    },
    {
      title: t("NVL"),
      align: 'left',
      width: '40%',
      dataIndex: 'name',
      sortDirections: ["descend", "ascend"],
      sorter: (a, b) => trans(a.name, true).localeCompare(trans(b.name, true)),
      render: (value, record) => trans(value)

    },
    {
      title: t("ĐVT"),
      align: 'left',
      width: '12%',
      dataIndex: 'unitName',
      sorter: (a, b) => a.unitName.localeCompare(b.unitName),
      sortDirections: ["descend", "ascend"],
    }, 
    {
      title: t("SL trong kho sx"),
      align: 'right',
      width: 120,
      dataIndex: 'manufacturingQuantity',
      key: "manufacturingQuantity",
      sortDirections: ["descend", "ascend"],
      sorter: (a, b) => (a.manufacturingQuantity - b.manufacturingQuantity),
      render: value => {
        return value ? ExtendFunction.FormatNumber(value) : 0
      }
    },
    {
      title: t("SL cần để sx"),
      align: 'right',
      width: 120,
      dataIndex: 'quantityToProduct',
      key: "quantityToProduct",
      sortDirections: ["descend", "ascend"],
      sorter: (a, b) => (a.quantityToProduct - b.quantityToProduct),
      render: (value, record) => {
        return <span
          style={{ color: record.quantityToProduct > record.manufacturingQuantity ? "red" : null }}
                >
                  {value ? ExtendFunction.FormatNumber(value) : 0}
                  </span>
      }
    },];
    return (
      <div>
        <Fragment>
          <OhTable
            id={"infor-manu-table" + this.props.id}
            columns={columns1}
            dataSource={data || []}
            isNonePagination={true}
            isExpandable={false}
            hasCheckbox={false}
            scrollToFirstRowOnChange={false}
          />
        </Fragment>
      </div>
    );
  }
}

InforManufacturing.propTypes = {
  classes: PropTypes.object.isRequired
};


export default (
  connect(function (state) {
    return {
      stockList: state.stockListReducer.stockList
    };
  })
)(
  withTranslation("translations")(
    withStyles(theme => ({
      ...regularFormsStyle
    }))(InforManufacturing)
  )
);