import React, { Component } from 'react';
import CardBody from 'components/Card/CardBody';
import OhTable from 'components/Oh/OhTable';
import ExtendFunction , { customExpandIcon } from 'lib/ExtendFunction';
import { withTranslation } from "react-i18next";
import Constants from 'variables/Constants';
import { notifyError } from 'components/Oh/OhUtils';
import _ from 'lodash';
import moment from 'moment';
import ReturnReportService from 'services/ReturnReportService';
import Expend2Table from './Expend2Table';

class Expend1Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      totalTable: 0,
      loading: false
    }
    this.filters = {}
  }

  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(this.props.data) !== JSON.stringify(prevProps.data)) {
      this.getDataExpend()
    }
  }

  componentDidMount() {
    this.getDataExpend()
  }

  getDataExpend = async () => {
    let { options, startTime, endTime, data, t } = this.props;

    this.setState({
      loading: true,
      dataSource: []
    });

    let filter = {};

    try {
      switch (options) {
        case Constants.OPTIONS_RETURN_REPORT.CUSTOMER:
          filter = _.extend(filter || {}, { importedAt: {">=": startTime, "<=": endTime }, recipientId: data.id })
          break;        
        case  Constants.OPTIONS_RETURN_REPORT.PRODUCT:
          filter = _.extend(filter || {}, { importedAt: {">=": startTime, "<=": endTime }, "importcardproduct.productId": data.productId } )
          break;        
        default:
          break;
      }

      let query = {
        filter,
        options
      }

      let getData = await ReturnReportService.getExpandReturnReportData(query);
      
      if(getData.status) {
        this.setState({
          dataSource: getData.data,
          totalTable: getData.count
        })
      }
      else throw getData.message
    }
    catch(err) {
      if (typeof err === "string") notifyError(err)
      else notifyError(t("Lỗi lấy dữ liệu"))
    } 

    this.setState({
      loading: false
    })
  }

  render() {
    let { dataSource, totalTable, loading } = this.state;
    let { options, t } = this.props;

    let columnProducts = [
      {
        title: t("Mã HD"),
        align: "left",
        dataIndex: "code",
        key: "code",
        width: "16%",
        sorter: (a,b) =>  a.code.localeCompare(b.code),
        render: value => {
          return <div title={value}>{value}</div>
        },
      },
      {
        title: t("Thời gian"),
        align: "left",
        dataIndex: "time",
        key: "time",
        width: "22%",
        sorter: (a,b) => a.time - b.time,
        render: value => moment(value).format(Constants.DISPLAY_DATE_TIME_FORMAT_STRING_TIME_FORMAT)
      },
      {
        title: t("Số lượng"),
        align: "right",
        dataIndex: "quantity",
        key: "quantity",
        width: "16%",
        sorter: (a,b) => a.quantity - b.quantity,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Thành tiền"),
        align: "right",
        dataIndex: "finalAmountProduct",
        key: "finalAmountProduct",
        sorter: (a,b) => a.finalAmountProduct - b.finalAmountProduct,
        width: "16%",
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      }
    ]

    let columns = [
      {
        title: t("Mã HD"),
        align: "left",
        dataIndex: "code",
        key: "code",
        width: "16%",
        sorter: (a,b) =>  a.code.localeCompare(b.code),
        render: value => {
          return <div title={value}>{value}</div>
        },
      },
      {
        title: t("Thời gian"),
        align: "left",
        dataIndex: "time",
        key: "time",
        width: "22%",
        sorter: (a,b) => a.time - b.time,
        render: value => moment(value).format(Constants.DISPLAY_DATE_TIME_FORMAT_STRING_TIME_FORMAT)
      },
      {
        title: t("Tổng tiền"),
        align: "right",
        dataIndex: "totalAmount",
        key: "totalAmount",
        width: "16%",
        sorter: (a,b) => a.finalAmount - b.finalAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Chiết khấu"),
        align: "right",
        dataIndex: "discountAmount",
        key: "discountAmount",
        width: "16%",
        sorter: (a,b) => a.finalAmount - b.finalAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Thành tiền"),
        align: "right",
        dataIndex: "finalAmount",
        key: "finalAmount",
        width: "16%",
        sorter: (a,b) => a.finalAmount - b.finalAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Thanh toán"),
        align: "right",
        dataIndex: "paidAmount",
        key: "paidAmount",
        width: "16%",
        sorter: (a,b) => a.paidAmount - b.paidAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      }
    ]

    return (
      <CardBody className="table-return-report-view-expand">
        <OhTable
          id= {"expend-return-report"}
          dataSource={dataSource}
          loading={loading}
          total={totalTable}
          columns={options === Constants.OPTIONS_RETURN_REPORT.PRODUCT ? columnProducts : columns}
          className="table-return-report-view-1"
          isNonePagination={totalTable > 10 ? false : true}
          isKeepExpand={true}
          expandIcon={(props) => customExpandIcon(props)}
          isExpandable={true}
          expandedRowRender={(record) => (
              <CardBody >
                <Expend2Table data={record} options={options}/>
              </CardBody>
            )
          }
          rowClassName={() => {
            return 'rowOhTable';
          }}
        />
      </CardBody>
    );
  }
}

export default withTranslation("translations")(Expend1Table);