import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import { withTranslation } from 'react-i18next';
import moment from "moment";
import { Spin } from "antd";
import OhDateTimePicker from 'components/Oh/OhDateTimePicker';
import OhMultiChoice from "components/Oh/OhMultiChoice.jsx";
import _ from "lodash";
import { trans } from "lib/ExtendFunction";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import OhToolbar from "components/Oh/OhToolbar";
import { MdVerticalAlignBottom, MdViewList } from "react-icons/md";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import OhSelect from "components/Oh/OhSelect";
import Constants from "variables/Constants/";
import OhTable from "components/Oh/OhTable";
import ExtendFunction, { customExpandIcon } from "lib/ExtendFunction";
import ReturnReportService from 'services/ReturnReportService';
import { notifyError } from 'components/Oh/OhUtils';
import Expend1Table from "./Expend1Table.jsx";

class Management extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataReturnReport: [],
      InputValue: [moment().startOf('month'),moment().endOf('month')],
      startTime: moment().startOf('month').valueOf(),
      endTime: moment().endOf('month').valueOf(),
      optionReport: 1,
      selectFilters: [],
      loading: false
    }
    this.oldOptionReport = 1
  }

  dataSelectMulti = () => {
    let { optionReport } = this.state;
    let dataOptions = [];

    switch (optionReport) {
      case Constants.OPTIONS_RETURN_REPORT.PRODUCT:
        dataOptions = _.cloneDeep(this.props.productList).map(item => ({...item, name: trans(item.name, true)}))
        break;
      case Constants.OPTIONS_RETURN_REPORT.CUSTOMER:
        dataOptions = _.cloneDeep(this.props.customers)
        break;
      default:
        break;
    }

    return dataOptions;
  }
  
  componentWillMount() {
    this.getData(this.state.startTime, this.state.endTime)
  }

  getData = async (startTime, endTime) => {
    let { optionReport, selectFilters } = this.state;
    let { t } = this.props;

    this.setState({loading: true, dataReturnReport: []})

    try {
      let getDataReport = await ReturnReportService.getReturnReportData({
        startDate: startTime,
        endDate: endTime,
        options: optionReport,
        selects: selectFilters
      });

      if (getDataReport.status) {
        this.setState({
          dataReturnReport: getDataReport.data,
          loading: false
        })
      }
      else throw getDataReport.message
    }
    catch(err) {
      notifyError(t("Lấy dữ liệu lỗi"))
    }
  }

  exportExcel() {
    let { dataReturnReport } = this.state;
    let { t } = this.props;

    let dataProductExcel = this.oldOptionReport === Constants.OPTIONS_RETURN_REPORT.PRODUCT ? [
      [
        t("Mã sản phẩm"),
        t("Tên sản phẩm"),
        t("ĐVT"),
        t("Số lượng"),
        t("Giá trị trả"),
        t("Giảm giá"),
        t("Thành tiền"),
      ]
    ] : [
      [
        t("Mã khách hàng"),
        t("Tên khách hàng"),
        t("Số đơn trả"),
        t("Giá trị trả"),
        t("Giảm giá"),
        t("Thành tiền"),
      ]
    ];
    
    _.forEach(dataReturnReport, item => {
      this.oldOptionReport === Constants.OPTIONS_RETURN_REPORT.PRODUCT ? 
        dataProductExcel.push([
          item.productCode,
          trans(item.productName, true),
          item.unitName,
          ExtendFunction.FormatNumber(item.quantity),
          ExtendFunction.FormatNumber(item.totalAmount),
          ExtendFunction.FormatNumber(item.discountAmount),
          ExtendFunction.FormatNumber(item.finalAmount),
        ]) : dataProductExcel.push([
          item.customerCode,
          item.customerName,
          ExtendFunction.FormatNumber(item.count),
          ExtendFunction.FormatNumber(item.totalAmount),
          ExtendFunction.FormatNumber(item.discountAmount),
          ExtendFunction.FormatNumber(item.finalAmount),
        ])
    })

    ExtendFunction.exportToCSV(dataProductExcel, t("BaoCaoTraHang"));
  }

  renderPlaceHolder() {
    let { optionReport } = this.state;
    let { t } = this.props;
    let placeHolder = ""

    switch (optionReport) {
      case Constants.OPTIONS_RETURN_REPORT.PRODUCT:
        placeHolder = t("Chọn tên sản phẩm")
        break;
      case Constants.OPTIONS_RETURN_REPORT.CUSTOMER:
        placeHolder = t("Chọn tên khách hàng")
        break;
      default:
        break;
    }

    return placeHolder;
  }

  render() {
    const { t } = this.props
    const { InputValue, optionReport, startTime, endTime, dataReturnReport, loading } = this.state;

    let columns = [
      {
        title: t("Mã khách hàng"),
        dataIndex: "customerCode",
        key: "customerCode",
        align: "left",
        width: "20%",
        sorter: (a,b) => a.customerCode.localeCompare(b.customerCode)
      },     
      {
        title: t("Tên khách hàng"),
        dataIndex: "customerName",
        key: "customerName",
        align: "left",
        width: "20%",
        sorter: (a,b) => a.customerName.localeCompare(b.customerName),
      },
      {
        title: t("Số đơn trả"),
        dataIndex: "count",
        key: "count",
        align: "left",
        width: "20%",
        sorter: (a,b) => a.countReturn - b.countReturn,
      },
      {
        title: t("Giá trị trả"),
        dataIndex: "totalAmount",
        key: "totalAmount",
        align: "right",
        width: "10%",
        sorter: (a,b) => a.totalAmount - b.totalAmount,
        render: value => {
          return (
            <div className="ellipsis-not-span">
              {value ? ExtendFunction.FormatNumber(value) : 0}
            </div>
          );
        }
      },
      {
        title: t("Giảm giá"),
        dataIndex: "discountAmount",
        key: "discountAmount",
        align: "right",
        width: "10%",
        sorter: (a,b) => a.discountAmount - b.discountAmount,
        render: value => {
          return (
            <div className="ellipsis-not-span">
              {value ? ExtendFunction.FormatNumber(value) : 0}
            </div>
          );
        }
      },
      {
        title: t("Thành tiền"),
        dataIndex: "finalAmount",
        key: "finalAmount",
        align: "right",
        width: "10%",
        sorter: (a,b) => a.finalAmount - b.finalAmount,
        render: (value,record)=> {
          return (
            <div className="ellipsis-not-span">
                {value ? ExtendFunction.FormatNumber(value) : 0}
            </div>
          );
        }
      }      
    ];
    
    let columnProducts = [
      {
        title: t("Mã sản phẩm"),
        dataIndex: "productCode",
        key: "productCode",
        align: "left",
        width: "20%",
        sorter: (a,b) => a.productCode.localeCompare(b.productCode)
      },     
      {
        title: t("Tên sản phẩm"),
        dataIndex: "productName",
        key: "productName",
        align: "left",
        width: "20%",
        sorter: (a,b) => a.productName.localeCompare(b.productName),
        render: value => trans(value) 
      },
      {
        title: t("ĐVT"),
        dataIndex: "unitName",
        key: "unitName",
        align: "right",
        sorter: (a,b) => a.unitName.localeCompare(b.unitName),
        width: "10%",
      },

      {
        title: t("Số lượng"),
        dataIndex: "quantity",
        key: "quantity",
        align: "right",
        width: "10%",
        sorter: (a,b) => a.quantity - b.quantity,
        render: value => {
          return (
            <div className="ellipsis-not-span">
              {value ? ExtendFunction.FormatNumber(value) : 0}
            </div>
          );
        }
      },
      {
        title: t("Giá trị trả"),
        dataIndex: "totalAmount",
        key: "totalAmount",
        align: "right",
        width: "10%",
        sorter: (a,b) => a.totalAmount - b.totalAmount,
        render: value => {
          return (
            <div className="ellipsis-not-span">
              {value ? ExtendFunction.FormatNumber(value) : 0}
            </div>
          );
        }
      },
      {
        title: t("Giảm giá"),
        dataIndex: "discountAmount",
        key: "discountAmount",
        align: "right",
        width: "10%",
        sorter: (a,b) => a.discountAmount - b.discountAmount,
        render: value => {
          return (
            <div className="ellipsis-not-span">
              {value ? ExtendFunction.FormatNumber(value) : 0}
            </div>
          );
        }
      },
      {
        title: t("Thành tiền"),
        dataIndex: "finalAmount",
        key: "finalAmount",
        align: "right",
        width: "10%",
        sorter: (a,b) => a.finalAmount - b.finalAmount,
        render: (value,record)=> {
          return (
            <div className="ellipsis-not-span">
                {value ? ExtendFunction.FormatNumber(value) : 0}
            </div>
          );
        }
      }      
    ];

    return (
      <Fragment>
        <Spin spinning={loading}>      
          <Card>        
            <CardBody>        
              <div>            
                <GridContainer style={{ padding: '0px 10px 0px 20px' }} alignItems="center">
                  <OhToolbar
                    left={[
                      {
                        label: t("Xuất báo cáo"),
                        type: "button",
                        typeButton:"export",
                        onClick: () => this.exportExcel(),
                        icon: <MdVerticalAlignBottom />,
                      }
                    ]}
                  />
                  <>
                    <GridItem >
                      <OhDateTimePicker defaultValue={InputValue} onChange={(start,end) => {
                          this.setState({
                            startTime: new Date(start).getTime(),
                            endTime: new Date(end).getTime()
                          })
                        }}
                      />
                    </GridItem>
                  </>
                  <>
                    <GridItem>
                      <span className="TitleInfoForm">{t("Theo")}</span>
                    </GridItem>
                    <GridItem>
                      <OhSelect
                        options={Constants.OPTIONS_SELECT_RETURN_REPORT.map(item => ({...item, title: t(item.title)}))}
                        value={optionReport}
                        onChange={value => {
                          this.setState({
                            optionReport: value,
                            selectFilters: []
                          })
                        }}
                        className="OptionReturnReport"
                      />
                    </GridItem>
                    <GridItem style={{ width: "185px", marginRight: 20 }}>
                      <OhMultiChoice 
                        dataSourcePType={this.dataSelectMulti()}
                        key={optionReport}
                        placeholder={this.renderPlaceHolder()}
                        onChange={(selectFilters) => {
                          this.setState({ selectFilters })
                        }}
                        defaultValue={this.state.selectFilters}
                        className='reportSelect'
                      />
                    </GridItem>
                    <GridItem >
                      <OhToolbar
                        left={[
                          {
                            type: "button",
                            label: t("Xem báo cáo"),
                            onClick: () => {
                              this.getData(startTime, endTime);
                              this.oldOptionReport = optionReport;
                            },
                            icon: <MdViewList />,
                            simple: true,
                            typeButton:"add",
                          },
                        ]}
                      />
                    </GridItem>
                  </>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12}>
                    <OhTable
                      id= {"return-report" + optionReport}
                      columns={this.oldOptionReport === Constants.OPTIONS_RETURN_REPORT.PRODUCT ? columnProducts : columns}
                      dataSource={dataReturnReport.length > 500 ? dataReturnReport.slice(0,500) : dataReturnReport}
                      isNonePagination={true}
                      isExpandable={true}
                      rowClassName={(record, index) => {
                        return 'rowOhTable';
                      }}
                      isKeepExpand={true}
                      expandedRowRender={(record) => <Expend1Table data={record} options={this.oldOptionReport} startTime={startTime} endTime={endTime} />}
                      expandIcon={(props) => customExpandIcon(props)}
                      className="table-return-report-view"
                    />
                  </GridItem>
                </GridContainer>
              </div>
            </CardBody>
          </Card>
        </Spin>
      </Fragment>
    );
  }
}

export default (
  connect(function(state) {
    return {      
      users: state.userListReducer.users,
      productList: state.productListReducer.products,
      customers: state.customerListReducer.customers
    };
  })
)(withTranslation("translations")(Management));