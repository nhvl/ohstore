import React, { Component } from 'react';
import OhTable from 'components/Oh/OhTable';
import ReturnReportService from 'services/ReturnReportService';
import { notifyError } from 'components/Oh/OhUtils';
import ExtendFunction, { trans } from 'lib/ExtendFunction';
import { withTranslation } from "react-i18next";

class Expend2Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      loading: false
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(this.props.data) !== JSON.stringify(prevProps.data)) {
      this.getData()
    }
  }

  componentDidMount() {
    this.getData()
  }

  getData = async () => {
    let { data, options, t } = this.props;

    this.setState({ loading: true });

    try {    
      let getDataExpend = await ReturnReportService.getExpand2ReturnReportData(data.id, {options});      

      if(getDataExpend.status)
        this.setState({
          dataSource: getDataExpend.data
        })
      else throw getDataExpend.message      
    }
    catch(err) {
      if (typeof err === "string") notifyError(err)
      else notifyError(t("Lấy dữ liệu lỗi"))
    }   
    this.setState({ loading: false })
  }

  render() {
    let { dataSource, loading } = this.state;
    let { data, options, t } = this.props;

    let columns = [
      {
        title: t("Mã - Tên SP"),
        align: "left",
        dataIndex: "productName",
        key: "productName",
        width: "16%",
        sorter: (a, b) => a.productName.localeCompare(b.productName),
        render: (value, record) => { 
          return <div title={value}>
            <span>{`${record.productCode} - `}</span>
            <span>{trans(value)}</span>
          </div>
        }
      },
      {
        title: t("Số lượng"),
        align: "right",
        dataIndex: "quantity",
        key: "quantity",
        width: "16%",
        sorter: (a,b) => a.quantity - b.quantity,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: data.isReturn ? t("Giá trả") : t("Giá bán"),
        align: "right",
        dataIndex: "unitPrice",
        key: "unitPrice",
        width: "16%",
        sorter: (a,b) => a.unitPrice - b.unitPrice,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Chiết khấu"),
        align: "right",
        dataIndex: "discount",
        key: "discount",
        width: "16%",
        sorter: (a,b) => a.discount - b.discount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Thành tiền"),
        align: "right",
        dataIndex: "finalAmount",
        key: "finalAmount",
        width: "16%",
        sorter: (a,b) => a.finalAmount - b.finalAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      }
    ]

    return (
      <OhTable
        id= {"expend-expend-return-report-" + options + "-" + data.code}
        dataSource={dataSource}
        loading={loading}
        isNonePagination={true}
        columns={columns}
      />
    );
  }
}

export default withTranslation("translations")(Expend2Table);