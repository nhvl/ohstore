import React from "react";
import { connect } from "react-redux";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import regularFormsStyle from "assets/jss/material-dashboard-pro-react/views/regularFormsStyle";

// multilingual
import { withTranslation } from "react-i18next";
import Constants from 'variables/Constants/';
import "date-fns";
import moment from "moment";
import _ from "lodash";
import OhForm from 'components/Oh/OhForm';
import {routes} from 'routes.js';
import { getUser } from "lib/ExtendFunction";


class OrderInfoForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orderInfo: {
        orderAt: moment(),
        expectedAt: moment(),
        status: 1,
      },
      users: this.props.listUser || []
    };

  }

  componentDidUpdate = (prevProps, prevState) => {
    const { dataEdit, dataUser } = this.props;
    if (prevProps.dataEdit !== dataEdit && dataEdit){
      this.setState({
        orderInfo: {
          code: dataEdit.code,
          notes: dataEdit.notes,
          orderAt: moment(dataEdit.orderAt),
          expectedAt: moment(dataEdit.expectedAt),
          status: dataEdit.status,
          createdBy: dataEdit.createdBy,
          fullName: dataEdit.user_order ? dataEdit.user_order.fullName : this.props.currentUser.user.fullName 
        }
      })
    }
    if (prevProps.dataUser !== dataUser && dataUser && !this.props.listUser.length){
      this.setState({
        users: dataUser
      })
    }

      
  }

  onChange = (value) => {    
    this.setState({
      orderInfo: value
    }, () => this.props.sendOrderInfo(this.state.orderInfo))
  }
  
  render() {
    let { t, dataEdit, isEdit, currentUser, isCanceledCard} = this.props;
    let { orderInfo, users } = this.state;
    let checkEmployees = this.props.Employees_Options && parseInt(this.props.Employees_Options) === Constants.EMPLOYEES_OPTIONS.ON;
    let createdBy = isEdit ? (orderInfo || {}).createdBy : (checkEmployees && orderInfo.createdBy ? orderInfo.createdBy : this.props.currentUser.user.id ) ;
    let fullName = checkEmployees ? "" : (isEdit ? (orderInfo || {}).fullName : getUser(currentUser.user.id, users ));

    return (
      <GridItem float='right' xs={12} sm={12} md={6} lg={6}>
        <Card className = 'invoice-info-card' style={{height: "100%"}}>
          <CardBody xs={12} style={{ padding: 0 }}>
            <OhForm
              title={t("Thông tin đơn hàng")}
              defaultFormData={ _.extend(orderInfo, {createdBy: createdBy, fullName: fullName}) }
              onRef={ref => this.ohFormRef = ref}
              tag={isCanceledCard ? Constants.ORDER_CARD_STATUS_NAME[dataEdit.status] : null}
              columns={[
                [
                  {
                    name: "code",
                    label: t("Mã đơn hàng"),
                    ohtype: "input",
                    disabled: true,
                    placeholder: t(Constants.PLACEHOLDER_AUTO_GENERATE_CODE),
          
                  },
                  checkEmployees ?
                  {
                    name: "createdBy",
                    label: t("Người tạo"),
                    ohtype: "select",
                    options: users.map(item => ({value: item.id, title: item.fullName, data: item})),
                    placeholder: t("Chọn một nhân viên"),
                  }: {
                    name: "fullName",
                    label: t("Người tạo"),
                    ohtype: "label",
                  },
                  {
                    name: "orderAt",
                    label: t("Ngày đặt"),
                    ohtype: "date-picker",
                    showTime: true,
                    disabled: isCanceledCard,
                    isDisabledDate:true,
                    
                  },
                  {
                    name: "expectedAt",
                    label: t("Ngày giao"),
                    ohtype: "date-picker",
                    disabled: isCanceledCard,
                    showTime: true,
                    isDisabledDate:true,
                    timeDisableStart: orderInfo.orderAt
                  },
                  dataEdit.status === Constants.ORDER_CARD_STATUS.CANCELLED ? {} :
                  {
                    name: "status",
                    label: t("Trạng thái"),
                    ohtype: "label",
                    format: value => t(Constants.ORDER_CARD_STATUS_NAME[value]),
                  },
                  dataEdit.status === Constants.ORDER_CARD_STATUS.FINISHED && dataEdit.referenceInvoiceId ? 
                  {
                    name: "referenceBillId",
                    label: t("Hóa đơn"),
                    ohtype: "link",
                    format: value => dataEdit.referenceInvoiceId.code,
                    onClick: () => this.props.history.push(routes.EditInvoice + '/' + dataEdit.referenceInvoiceId.id)
                  } : {},
                  {
                    name: "notes",
                    label: t("Ghi chú"),
                    ohtype: isCanceledCard ? "label" : "textarea",
                    minRows: 1,
                    maxRows: 2
                  },
                ],
              ]}
              onChange={value => {
                this.onChange(value);
              }}
            />
          </CardBody>
        </Card>
      </GridItem>
    );
  }
}

export default connect(function(state) {
  return ({
    currentUser: state.userReducer.currentUser,
    Employees_Options: state.reducer_user.Employees_Options,
    listUser: state.userListReducer.users
  });
})(
  withTranslation("translations")(
    withStyles(theme => ({
      ...regularFormsStyle
    }))(OrderInfoForm)
  )
);
