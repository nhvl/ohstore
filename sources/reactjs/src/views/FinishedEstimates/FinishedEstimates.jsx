import React, { Component, Fragment } from 'react';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import withStyles from "@material-ui/core/styles/withStyles";
import regularFormsStyle from "assets/jss/material-dashboard-pro-react/views/regularFormsStyle.jsx";
import { withTranslation } from 'react-i18next';
import ExtendFunction, { customExpandIcon } from "lib/ExtendFunction.js";
import Card from "components/Card/Card.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import CardBody from "components/Card/CardBody.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import FormLabel from "@material-ui/core/FormLabel";
import OhTable from 'components/Oh/OhTable.jsx';
import FinishedEstimatesService from 'services/FinishedEstimatesService.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import Constants from "variables/Constants/";
import OhToolbar from "components/Oh/OhToolbar.jsx";
import { notifyError } from 'components/Oh/OhUtils.js';
import { trans } from "lib/ExtendFunction.js";
import OhMultiChoice from "components/Oh/OhMultiChoice.jsx";
import InforFinishedEstimates from "./inforFinishedEstimates.jsx"
import ManualSortFilter from "MyFunction/ManualSortFilter";
import { MdViewList, MdVerticalAlignBottom} from "react-icons/md";
import OhButton from 'components/Oh/OhButton.jsx';

class EstimatesReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      FinishedEstimatesReport: [],
      productType: {},
      selectTypes: [],
      selectProducts: [],
      selectStockId: [],
      selectNameType: [],
      dataSourcePType: this.props.productTypes || [],
      dataSourceProduct: (this.props.productList || []).map(item => ({...item, name: trans(item.name, true)})),
      stock_lists: ExtendFunction.getSelectStockList(this.props.stockList, false),
      loading: false

    }
  }

  componentDidMount() {
    this.getData();
  }

  async componentDidUpdate(prevProps, prevState) {
    if ((this.props.productList.length && this.props.productList.length !== prevProps.productList.length) || (this.props.productTypes.length && this.props.productTypes.length !== prevProps.productTypes.length)){
      this.setState({
        dataSourcePType: this.props.productTypes || [],
        dataSourceProduct: (this.props.productList || []).map(item => ({...item, name: trans(item.name, true)})),
      })
    }
  }

  async getData() { 
    this.setState({
      loading: true
    })   
    let { t } = this.props;
    let { stock_lists, selectStockId } = this.state;
    let stocks = [];

    if (stock_lists.length) {
      stock_lists.map( item => stocks.push(item.id));
    }

    const query = {
      selectProduct: this.state.selectProducts,
      type: this.state.selectTypes,
      stockId: selectStockId.length === 0 ? stocks : selectStockId
    };

    let FinishedEstimatesReport = await FinishedEstimatesService.getFinishedEstimatesDate(query)
    if (FinishedEstimatesReport.status) {
      this.setState({
        loading: false,
        FinishedEstimatesReport: FinishedEstimatesReport.data
      })
    } else {
      notifyError(t("Lỗi xử lý dữ liệu"))
      this.setState({
        loading: false
      })
    }
  }

  getDataFilterProductByType() {
    let { selectTypes } = this.state;

    if (!selectTypes.length) {
      this.setState({
        dataSourceProduct: (this.props.productList || []).map(item => ({...item, name: trans(item.name, true)}))
      })
    }
    else {
      let productTypeFilter = "productTypeId.id";
      let dataFilter = ManualSortFilter.ManualSortFilter(this.props.productList || [], {[productTypeFilter]: {in:selectTypes }}, {});

      this.setState({
        dataSourceProduct: dataFilter.map(item => ({...item, name: trans(item.name, true)}))
      })
    }
  }

  export = () => {
    let { t } = this.props;
    const { FinishedEstimatesReport } = this.state;
    let dataExcel = [[
      '#',
      t('Mã'),
      t('Tên thành phẩm'),
      t('ĐVT'),
      t('Tồn kho'),
      t('Tồn kho sx'),
      t('Có thể sx')
    ]];
    for (let item in FinishedEstimatesReport) {            
      dataExcel.push(
        [
          parseInt(item) + 1,
          FinishedEstimatesReport[item].code,
          trans(FinishedEstimatesReport[item].name, true),
          FinishedEstimatesReport[item].unitName,
          ExtendFunction.FormatNumber(Number(FinishedEstimatesReport[item].stockQuantity).toFixed(0)),
          ExtendFunction.FormatNumber(Number(FinishedEstimatesReport[item].manufacturingQuantity).toFixed(0)),
          ExtendFunction.FormatNumber(Number(FinishedEstimatesReport[item].productionMay).toFixed(0)),
        ]
      )
    }
    return dataExcel
  }

  
  showInfor = (record) => {
    let URL = window.location.origin;
    return (
      URL + Constants.ADMIN_LINK + Constants.EDIT_PRODUCT + "/" + record.id
    ); 
  }

  render() {
    const { t } = this.props;
    const { FinishedEstimatesReport, stock_lists } = this.state;
        
    let columns = [
      {
        title: t('Mã'),
        align: 'left',
        width: '16%',
        dataIndex: 'code',
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => a.code.localeCompare(b.code),
        key: "code",
        render: (value, record) => { 
          return (
          <div className = "finished-estimates">
            <a href={this.showInfor(record)} target="_blank" rel="noopener noreferrer" >{value}</a>
            </div>
          )
        }
      },
      {
        title: t("Tên thành phẩm"),
        align: 'left',
        width: '40%',
        dataIndex: 'name',
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) =>  trans(a.name, true).localeCompare(trans(b.name, true)),
        render: (value, record) => trans(value)
      },
      {
        title: t("ĐVT"),
        align: 'left',
        width: '12%',
        dataIndex: 'unitName',
        sorter: (a, b) =>  a.unitName.localeCompare(b.unitName),
        sortDirections: ["descend", "ascend"],
      },
      {
        title: t("Tồn kho"),
        align: 'right',
        width: 120,
        dataIndex: 'stockQuantity',
        key: "stockQuantity",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.stockQuantity - b.stockQuantity),
        render: value => {
          return value ? ExtendFunction.FormatNumber(value) : 0
        }
      },
      {
        title: t("Tồn kho sx"),
        align: 'right',
        width: 120,
        dataIndex: 'manufacturingQuantity',
        key: "manufacturingQuantity",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.manufacturingQuantity - b.manufacturingQuantity),
        render: value => {
          return value ? ExtendFunction.FormatNumber(value) : 0
        }
      },
      {
        title: t("Có thể sx"),
        align: 'right',
        width: 135,
        dataIndex: 'productionMay',
        key: "productionMay",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.productionMay - b.productionMay),
        render: value => {
          return value ? ExtendFunction.FormatNumber(value) : 0
        }
      },
    ];

    
    
    return (
      <div>
        <Fragment>
        {this.state.redirect}
          <Card>
            <CardBody >
              <GridContainer style={{ padding: '0px 15px' }} alignItems="center">
                <OhToolbar
                  left={[
                    {
                      type: "csvlink",
                      label: t("Xuất báo cáo"),
                      typeButton: "export",
                      csvData: this.export(),
                      fileName: t("BaoCaoUocTinhThanhPham") +".xls",
                      onClick: () => { },
                      icon: <MdVerticalAlignBottom />,
                    },
                  ]}
                />
                { stock_lists.length > 1 ?
                <GridItem style={{ width: "190px" }} >
                    <OhMultiChoice
                      dataSourcePType={stock_lists}
                      placeholder={t("Chọn theo kho")}
                      onChange= {(selectStockId) => {
                        
                        this.setState({
                          selectStockId: selectStockId,
                        })
                      }}
                      defaultValue={this.state.selectStockId}
                    />
                </GridItem>: null}
                <GridItem>
                  <span className="TitleInfoForm">{t("Theo")}</span>
                </GridItem>
                <GridItem style={{width:"280px"}}>
                  <OhMultiChoice
                    dataSourcePType={this.state.dataSourcePType}
                    placeholder={t("Chọn theo nhóm sản phẩm")}
                    onChange= {(selectTypes) => {
                      this.setState({
                        selectTypes: selectTypes,
                      }, () => this.getDataFilterProductByType())
                    }}
                    defaultValue={this.state.selectTypes}
                  />
                </GridItem>
                <GridItem>
                  <GridContainer>
                    <GridItem>
                      <span className="TitleInfoForm" style={{verticalAlign: "-webkit-baseline-middle"}}>{t("Sản phẩm")}</span>
                    </GridItem>
                    <GridItem>

                    <OhMultiChoice
                        dataSourcePType={this.state.dataSourceProduct}
                        placeholder={t("Chọn tên sản phẩm")}
                        onChange={(selectProducts) => {
                          this.setState({
                            selectProducts: selectProducts,
                          })
                        }}
                        defaultValue={this.state.selectProducts}
                        maxValue={10}
                        className='reportSelect'
                      />
                    </GridItem>
                    <GridItem>
                      <OhButton
                        type="add"
                        onClick={() => this.getData()}
                        icon={<MdViewList/>}
                      >
                        {t("Xem báo cáo")}
                      </OhButton>
                    </GridItem>
                  </GridContainer>
                </GridItem>
              </GridContainer>
              <GridContainer>
                <CardBody>
                  {FinishedEstimatesReport.length > 500 ?
                    <GridItem >
                      <GridContainer >
                        <FormLabel className="ProductFormAddEdit" style={{ margin: 0, marginRight: 15 }}>
                          <b className='HeaderForm'>{t("Hiển thị 1–500 của 500+ kết quả. Để thu hẹp kết quả, bạn hãy chọn loại nhóm hoặc chọn Xuất báo cáo để xem đầy đủ")}</b>
                        </FormLabel>
                      </GridContainer>
                    </GridItem>
                    : null
                  }
                  <OhTable
                    id="finished-estimates-report"
                    columns={columns}
                    dataSource={FinishedEstimatesReport.length > 500 ? FinishedEstimatesReport.slice(0,500) : FinishedEstimatesReport}
                    isNonePagination={true} 
                    isExpandable={true}
                    hasCheckbox={false} 
                    loading={this.state.loading}
                    rowClassName={() => { return 'rowOhTable'}} 
                    className="table-finished-estimates-report"
                    expandIcon={(props) => customExpandIcon(props)}
                    scrollToFirstRowOnChange={false}
                    expandedRowRender={(record) => {
                                         
                      return (
                          <CardBody style ={{marginTop:"4px", padding:"5px 0px"}} key = {"card-table-finish-estimates-"+ record.id}>
                            <InforFinishedEstimates
                             stock_lists = {stock_lists}
                             selectStockId = {this.state.selectStockId}
                             id = {record.id}
                            />
                          </CardBody>
                      )
                    }}                 
                  />
                </CardBody>
              </GridContainer>
            </CardBody>
          </Card>
        </Fragment>
      </div>
    );
  }
}

EstimatesReport.propTypes = {
  classes: PropTypes.object.isRequired
};

export default (
  connect(function (state) {
    return {
      stockList: state.stockListReducer.stockList,
      productList: state.productListReducer.products,
      productTypes: state.productTypeReducer.productTypes,
    };
  })
) (
  withTranslation("translations")(
    withStyles(theme => ({
      ...regularFormsStyle
    }))(EstimatesReport)
  )
);
