import React, { Component } from 'react';
import OhForm from 'components/Oh/OhForm';
import ProductTypeService from "services/ProductTypeService";
import { notifyError } from 'components/Oh/OhUtils';
import { Modal } from 'antd';
import { Modal as ModalMobile, Picker } from 'antd-mobile';
import {BrowserView, MobileView, isBrowser, isMobile} from "react-device-detect";
import { MdCancel ,MdAddCircle} from "react-icons/md";
import OhToolbar from 'components/Oh/OhToolbar';
import { withTranslation } from "react-i18next";

class ModalClickGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ProductType: [],
      data: {}
    }
  }
  async componentDidMount() {
    try {
      let getProductTypes = await ProductTypeService.getProductTypes();

      if (getProductTypes.status)
        this.setState({
          ProductType: getProductTypes.data
        })
      else throw getProductTypes.error
    }
    catch (err) {
      if (typeof err === "string") notifyError(err)
      notifyError()
    }
  }

  onChange = (obj) => {
    this.setState({
      data: {...obj}
    })
  }

  render() {
    let { ProductType, data } = this.state;
    let { visible, t } = this.props;

    return (<>
      {isMobile ? (<ModalMobile
        visible={visible}
        onCancel={() => this.props.handleCloseModal(false)}
        onClose={() => this.props.handleCloseModal(false)}
        transparent
        maskClosable={false}
        title={t("Thêm từ nhóm sản phẩm")}
        footer={[{ text: t("Thêm vào"), onPress: () => {
                if (this.ohFormRef.allValid()){
                  this.props.transferData(false, data);
                }
              } },{
                text: t("Thoát"), onPress: () => {
                  this.props.handleCloseModal(false);
                }
              }]}
      >
        <OhForm
          labelPadding={"mobile-oh-form-label"}
          defaultFormData={data}
          onRef={ref => this.ohFormRef = ref}
          columns={[
            [
              {
                name: "productTypeId",
                label: t("Nhóm"),
                ohtype: "select",
                validation: "required",
                autoFocus: true,
                message: t("Thông tin nhập vào bị lỗi"),
                options: ProductType.map(item => ({ title: item.name, value: item.id, data: item, code : item.code }))
              },
            ]
          ]}
          onChange={obj => this.onChange(obj)}
        />
      </ModalMobile>):(<Modal
        title={t("Thêm từ nhóm sản phẩm")}
        visible={visible}
        onCancel={() => this.props.handleCloseModal(false)}
        style={{overflow: "visible"}}       
        footer={[
          <OhToolbar
            key="product-form-toolbar"
            right={[
              {
                type: "button",
                label: t("Thêm vào"),
                onClick: () => {
                  if (this.ohFormRef.allValid()){
                    this.props.transferData(false, data)
                  }
                },
                icon: <MdAddCircle />,
                simple: true,
                typeButton: "add"
              },
              {
                type: "button",
                label: t("Thoát"),
                icon: <MdCancel />,
                onClick: () => this.props.handleCloseModal(false),
                simple: true,
                typeButton: "exit"
              },
            ]}
          />
        ]}
      >
        <OhForm
          defaultFormData={data}
          onRef={ref => this.ohFormRef = ref}
          columns={[
            [
              {
                name: "productTypeId",
                label: t("Nhóm"),
                ohtype: "select",
                validation: "required",
                autoFocus: true,
                message: t("Thông tin nhập vào bị lỗi"),
                options: ProductType.map(item => ({ title: item.name, value: item.id, data: item, code : item.code }))
              },
            ]
          ]}
          onChange={obj => this.onChange(obj)}
        />
      </Modal>)}
      </>
      
    );
  }
}

export default withTranslation("translations")(ModalClickGroup);