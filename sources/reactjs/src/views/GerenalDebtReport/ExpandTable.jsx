import React, { Component } from 'react';
import CardBody from 'components/Card/CardBody';
import OhTable from 'components/Oh/OhTable';
import ExtendFunction from 'lib/ExtendFunction';
import moment from 'moment';
import { withTranslation } from "react-i18next";
import Constants from 'variables/Constants';
import { notifyError } from 'components/Oh/OhUtils';
import _ from 'lodash';
import GeneralDebtService from 'services/GeneralDebtService';

class ExpendTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      loading: false
    }
    this.filters = {}
  }

  componentDidUpdate(prevProps, prevState) {
    if(!_.isEqual(this.props.data, prevProps.data))
      this.getDataExpend()
  }

  componentDidMount() {
    this.getDataExpend()
  }

  getDataExpend = async () => {
    let { dateTime, data, t } = this.props;
    this.setState({
      loading: true,
    });

    try {
      let getDebtCards = await GeneralDebtService.getGeneralDebtExpand({
        customerId: data.customerId,
        startDate: new Date(dateTime.start).getTime(),
        endDate: new Date(dateTime.end).getTime()
      })
      if (getDebtCards.status)
        this.setState({
          dataSource: getDebtCards.data,
          loading: false
        })
      else {
        notifyError(getDebtCards)
        this.setState({
          loading: false,
        });
      }
    }
    catch(err) {
      if (typeof err === "string") notifyError(err)
      else notifyError(t("Lỗi lấy dữ liệu"))
      this.setState({
        loading: false,
      });
    } 

    this.setState({
      loading: false
    })
  }

  getColumn = () => {
    let { t } = this.props;
    
    let columns = [
      {
        title: t("Tên giao dịch"),
        align: "left",
        dataIndex: "name",
        key: "name",
        sorter: (a,b) =>  a.name.localeCompare(b.name),
        render: value => {
          return t(value)
        },
      },
      {
        title: t("Mã giao dịch"),
        align: "left",
        dataIndex: "code",
        key: "code",
        sorter: (a,b) =>  a.code.localeCompare(b.code),
        render: value => {
          return <div title={value}>{value}</div>
        },
      },
      {
        title: t("Thời gian"),
        align: "left",
        dataIndex: "time",
        key: "time",
        sorter: (a,b) => a.time - b.time,
        render: value => moment(value).format(Constants.DISPLAY_DATE_TIME_FORMAT_STRING_TIME_FORMAT)
      },
      {
        title: t("Giá trị mua hàng"),
        align: "right",
        dataIndex: "finalAmount",
        key: "finalAmount",
        sorter: (a,b) => a.finalAmount - b.finalAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Giá trị thanh toán"),
        align: "right",
        dataIndex: "paidAmount",
        key: "paidAmount",
        sorter: (a,b) => a.paidAmount - b.paidAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Nhân viên"),
        align: "left",
        dataIndex: "createdName",
        key: "createdName",
        sorter: (a,b) =>  a.createdName.localeCompare(b.createdName),
        render: value => {
          return <div title={value}>{value}</div>
        },
      },
    ]
    return columns
  }

  render() {
    let { dataSource, loading } = this.state;
    let { data } = this.props;
    
    return (
      <CardBody>
        <OhTable
          id= {"expend-general-report" + data.key}
          dataSource={dataSource}
          loading={loading}
          total={dataSource.length}
          columns={this.getColumn()}
          isNonePagination={dataSource.length > 10 ? false : true}
          rowClassName={() => {
            return 'rowOhTable';
          }}
          className="table-sale-report-view-1"
        />
      </CardBody>
    );
  }
}

export default withTranslation("translations")(ExpendTable);