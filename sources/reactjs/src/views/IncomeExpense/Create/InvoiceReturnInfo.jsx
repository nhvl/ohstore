import "date-fns";
import React from "react";
import Constants from "variables/Constants/";
import OhTable from 'components/Oh/OhTable';
import InvoiceReturnService from 'services/InvoiceReturnService';
import GridItem from "components/Grid/GridItem.jsx";
import { Row, Col } from "react-grid-system";
import { connect } from "react-redux"
import { withTranslation } from "react-i18next";
import ExtendFunction from "lib/ExtendFunction";
import { notifyError } from 'components/Oh/OhUtils';
import { trans } from "lib/ExtendFunction";
import GridContainer from "components/Grid/GridContainer.jsx";
import _ from 'lodash';

class CardList extends React.Component {
  constructor(props) {
    super(props);
    let {cardId} = this.props;
    this.cardId = cardId;
    this.state = {
      dataSource: [],
      cardInfo: {}
    };
  }

  getColunms = () => {
    const { t } = this.props;

    let columns = [
      {
        title: t("Mã"),
        dataIndex: "productCode",
        key: "productCode",
        width: "11%",
        align: "left",
      },
      {
        title: t("Tên sản phẩm"),
        dataIndex: "productName",
        key: "productName",
        width: "20%",
        align: "left",
        render: value => trans(value)
      },
      {
        title: t("Đơn vị"),
        dataIndex: "unit",
        key: "unit",
        width: "10%",
        align: "left",
      },
      {
        title: t("Số lượng"),
        dataIndex: "quantity",
        key: "quantity",
        align: 'right',
        width: "10%",
        render: value => {
          return ExtendFunction.FormatNumber(Math.round(value)) || 0;
        },
      },
      {
        title: t("Giá nhập"),
        dataIndex: "finalAmount",
        key: "finalAmount",
        width: "10%",
        align: "right",
        render: value => {
          return ExtendFunction.FormatNumber(Math.round(value)) || 0;
        },
      },
      {
        title: t("Thành tiền"),
        dataIndex: "totalAmount",
        key: "totalAmount",
        align: 'right',
        render: value => {
          return ExtendFunction.FormatNumber(Math.round(value)) || 0;
        },
        width: "15%"
      },
    ];

    return columns
  }

  componentDidMount = () => {
    this.getData();
  }
  
  getData = async () => {
    
    let getCard = await InvoiceReturnService.getInvoiceReturn(this.cardId);
    if(!getCard.status) {
      return notifyError(getCard.message)
    }

    this.setState({
      cardInfo: {
        ...getCard.data,
        totalQuantity: _.sum(getCard.invoiceReturnProductArray.map(item => item.quantity))
      },
      dataSource: getCard.invoiceReturnProductArray.map(item => ({...item, totalAmount: item.finalAmount * item.quantity})),
    })
  }

  render() {
    const { dataSource, cardInfo} = this.state;
    const { t } = this.props;
    
    return (
      <>
        <GridContainer style={{ width: "100%", margin: 0 }}>
          <GridItem xs={12} >
            <OhTable
              onRef={ref => (this.tableRef = ref)}
              columns={this.getColunms()}
              dataSource={dataSource}
              isNonePagination={true}
              id={"product-form-table"}
              emptyDescription={Constants.NO_PRODUCT}
            />
          </GridItem>
        </GridContainer>
        <GridContainer justify='flex-end' style={{width: '100%'}}>
          <GridItem xs={5}>
            <Row>
              <Col style={{textAlign: "right"}}>{t("Tổng số lượng") + ":"}</Col>
              <Col className="Colums">
                {ExtendFunction.FormatNumber(cardInfo.totalQuantity)}
              </Col>
            </Row>
            <Row>
              <Col style={{ textAlign: "right", whiteSpace: 'nowrap' }}>{t("Tổng số mặt hàng") + ":"}</Col>
              <Col className="Colums">
                {dataSource.length}
              </Col>
            </Row>
            <Row>
              <Col style={{ textAlign: "right" }}>{t("Tổng tiền hàng") + ":"}</Col>
              <Col className="Colums">
                {cardInfo.totalAmount ? ExtendFunction.FormatNumber(Math.round(cardInfo.totalAmount)) : 0}
              </Col>
            </Row>
            <Row>
              <Col style={{ textAlign: "right" }}>
                {t("Chiết khấu") + ":"}
              </Col>
              <Col className="Colums">
                {cardInfo.discountAmount ? ExtendFunction.FormatNumber(Math.round(cardInfo.discountAmount)) : 0}
              </Col>
            </Row>
            <Row>
              <Col style={{ textAlign: "right" }}>{t("Phải thu từ đối tác") + ":"}</Col>
              <Col className="Colums">
                {cardInfo.finalAmount ? ExtendFunction.FormatNumber(Math.round(cardInfo.finalAmount)) : 0}
              </Col>
            </Row>
          </GridItem>
        </GridContainer>
      </>
    );
  }
}

export default connect()(withTranslation("translations")(CardList));
