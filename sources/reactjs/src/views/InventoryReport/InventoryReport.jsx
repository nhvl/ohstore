import React, { Component, Fragment } from 'react';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import withStyles from "@material-ui/core/styles/withStyles";
import regularFormsStyle from "assets/jss/material-dashboard-pro-react/views/regularFormsStyle";
import { withTranslation } from 'react-i18next';
import ExtendFunction from "lib/ExtendFunction";
import Card from "components/Card/Card.jsx";
import { Typography } from "antd";
import GridContainer from "components/Grid/GridContainer.jsx";
import CardBody from "components/Card/CardBody.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import moment from "moment";
import FormLabel from "@material-ui/core/FormLabel";
import OhTable from 'components/Oh/OhTable';
import inventoryReport from 'services/InventoryReportService';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import Constants from "variables/Constants/";
import { MdViewList, MdVerticalAlignBottom } from "react-icons/md";
import { AiOutlineSetting } from "react-icons/ai";
import OhToolbar from "components/Oh/OhToolbar";
import { trans } from "lib/ExtendFunction";
import OhMultiChoice from "components/Oh/OhMultiChoice";
import { ExportCSV } from 'ExportExcel/ExportExcel';
import _ from "lodash";
import { notifyError } from 'components/Oh/OhUtils';


const { Paragraph } = Typography;
class InventoryReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startTime: moment().startOf('month').format("DD/MM/YYYY"),
      endTime: moment().endOf('month').format("DD/MM/YYYY"),
      dateTime: {
        start: new Date(moment().startOf('month')).getTime(),
        end: new Date(moment().endOf('month')).getTime()
      },
      InputValue: [moment().startOf('month'), moment().endOf('month')],
      dataSourcePType: this.props.productTypes || [],
      fileList: [],
      MessageError: "",
      dataInventoryReport: [],
      totalInventory: 0,
      alert: null,
      br: null,
      brerror: null,
      productType: {},
      checkedBoxProduct: false,
      selectProducts: [],
      selectTypes: [],
      selectStockId: [],
      loading: false,
      stock_lists: ExtendFunction.getSelectStockList(this.props.stockList, false),
      dataSourceProduct: (this.props.productList || []).map(item => ({ ...item, name: trans(item.name, true) })),
      totalProduct: 0,
      hasTotal: true

    }
    this.viewColumn = [];
    this.isGetData = true;

  }

  componentWillUnmount() {
    this.isGetData = false;
  }

  componentDidMount() {
    let start = moment().startOf('month')
    let end = moment().endOf('month')
    this.getData(start, end, 0);
  }

  async componentDidUpdate(prevProps, prevState) {
    if (this.props.productTypes.length && this.props.productTypes.length !== prevProps.productTypes.length) {
      this.setState({
        dataSourcePType: this.props.productTypes || [],
        dataSourceProduct: (this.props.productList || []).map(item => ({ ...item, name: trans(item.name, true) })),
      })
    }
  }

  getData = async (start, end, skip) => {
    if (skip === 0) {
      this.setState({
        dataInventoryReport: [],
        totalInventory: 0,
        totalProduct: 0,
        loading: true,
        hasTotal: false
      })
      this.isGetData = true;
    }

    let { stock_lists, selectStockId, selectTypes, selectProducts, } = this.state;

    let stocks = [];

    if (stock_lists.length) {
      stock_lists.map(item => stocks.push(item.id));
    }

    let query = {
      startDate: new Date(start).getTime(),
      endDate: new Date(end).getTime(),
      type: selectTypes,
      selectProduct: selectProducts,
      stockId: selectStockId.length === 0 ? stocks : selectStockId
    };

    query = _.extend(query, skip || skip === 0 ? { limit: 100, skip: skip } : {});

    let dataInventoryReport = await inventoryReport.getInventoryReportData(query)

    if (dataInventoryReport.status) {
      if (!dataInventoryReport.data.length) {
        this.setState({
         
          loading: false
        })
        return;

      } else {
        let data = this.state.dataInventoryReport.concat(dataInventoryReport.data);
        
        if (this.isGetData) this.getData(start, end, data.length)
        this.setState({
          dataInventoryReport: data,
          totalInventory: !skip || skip === 0 ? dataInventoryReport.totalAmountStock : this.state.totalInventory,
          totalProduct: dataInventoryReport.totalProduct,
          loading: false,
          hasTotal: true,
        })
      }
    } else {
      notifyError(dataInventoryReport.message);
      this.setState({
        loading: false
      })
    }
  }

  formatted_date() {
    var result = "";
    result += moment().format(Constants.DISPLAY_DATE_TIME_FORMAT_STRING);
    return result;
  }

  export = async () => {
    const { t, nameBranch, stockList } = this.props;
    let { dateTime } = this.state;
    let dataInventoryReport = [];
    let totalInventory = 0;
    let checkInventory = (this.viewColumn.length && (this.viewColumn[this.viewColumn.length - 1].visible === true || this.viewColumn[this.viewColumn.length - 1].visible === undefined)) || this.viewColumn.length === 0;

    let { stock_lists, selectStockId, selectTypes, selectProducts } = this.state;

    let stocks = [];

    if (stock_lists.length) {
      stock_lists.map(item => stocks.push(item.id));
    }

    let query = {
      startDate: new Date(dateTime.start).getTime(),
      endDate: new Date(dateTime.end).getTime(),
      type: selectTypes,
      selectProduct: selectProducts,
      stockId: selectStockId.length === 0 ? stocks : selectStockId
    };

    let getInventoryReport = await inventoryReport.getInventoryReportData(query)

    if (getInventoryReport.status) {
      dataInventoryReport = getInventoryReport.data
      totalInventory = getInventoryReport.totalAmountStock
    }

    let dataExcel = [[t("Chi nhánh"), nameBranch], [t("Thời gian xuất"), this.formatted_date()],
    [t("Thời gian khảo sát"), moment(dateTime.start).format(Constants.DISPLAY_DATE_TIME_FORMAT_STRING_TIME_FORMAT) + "-" + moment(dateTime.end).format(Constants.DISPLAY_DATE_TIME_FORMAT_STRING_TIME_FORMAT)], checkInventory ? [t('Tổng tồn kho'), ExtendFunction.FormatNumber(totalInventory)] : ""];
    let headerExcel = ["#"];

    if (this.viewColumn.length > 0) {
      this.viewColumn.map(item => {
        if (item.visible === true || item.visible === undefined)
          headerExcel.push(t(item.title))
        return item;
      })
      dataExcel.push(headerExcel)
    }
    else {
      let columnStock = [];
      for (let index in stockList) {
        if (stockList[index].deletedAt === 0) {
          columnStock.push(`${stockList[index].name} \n`)
        }
      }
      dataExcel.push([
        '#',
        t('Sản phẩm'),
        t('Mã'),
        t('ĐVT'),
        ...columnStock,
        t('Kho sản xuất'),
        t('Giá vốn'),
        t('Tồn kho'),
        t('Giá trị tồn kho')
      ])
    }
    for (let item in dataInventoryReport) {
      if (this.viewColumn.length > 0) {
        let bodyExcel = [parseInt(item) + 1];
        this.viewColumn.map(column => {
          if (column.visible || column.visible === undefined) {
            if (column.dataIndex === "name")
              bodyExcel.push(trans(dataInventoryReport[item][column.dataIndex], true))
            else
              bodyExcel.push(dataInventoryReport[item][column.dataIndex])
          }
          return column;
        })
        dataExcel.push(bodyExcel)
      }
      else {
          let boExcel = [];
          for (let index in stockList) {
            if (stockList[index].deletedAt === 0) {
              boExcel.push(dataInventoryReport[item][stockList[index].stockColumnName])
            }
          }
          dataExcel.push([
            parseInt(item) + 1,
            trans(dataInventoryReport[item].name, true),
            dataInventoryReport[item].code,
            dataInventoryReport[item].unitName,
            ...boExcel,
            dataInventoryReport[item].manufacturingQuantity !== undefined ? ExtendFunction.FormatNumber(Number(dataInventoryReport[item].manufacturingQuantity).toFixed(0)) : 0,
            ExtendFunction.FormatNumber(Number(dataInventoryReport[item].costUnitPrice).toFixed(0)),
            ExtendFunction.FormatNumber(Number(dataInventoryReport[item].totalStockQuantity).toFixed(0)),
            ExtendFunction.FormatNumber(Number(dataInventoryReport[item].total).toFixed(0)),
          ])
      }
    }

    ExportCSV([{ data: dataExcel }], t("BaoCaoTonKho"));
  }

  render() {

    const { t, stockList } = this.props;

    const { dateTime, dataInventoryReport, totalInventory, loading } = this.state;

    let columnStock = [];

    for (let index in stockList) {
      if (stockList[index].deletedAt === 0) {
        columnStock.push({
          title: stockList[index].name,
          align: "center",
          dataIndex: stockList[index].stockColumnName,
          sortDirections: ["descend", "ascend"],
          sorter: (a, b) => a[stockList[index].stockColumnName] - b[stockList[index].stockColumnName]
        })
      }
    }

    let columns = [
      {
        title: "Sản phẩm",
        align: 'left',
        width: 150,
        dataIndex: 'name',
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.name ? a.name.localeCompare(b.name) : -1),
        render: value => {
          return (
            <Paragraph title={trans(value)} style={{ wordWrap: "break-word", wordBreak: "break-word", maxWidth: 200 }} ellipsis={{ rows: 4 }}>
              {trans(value)}
            </Paragraph>
          );
        },
      },
      {
        title: 'Mã',
        align: 'left',
        width: 70,
        dataIndex: 'code',
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.code ? a.code.localeCompare(b.code) : -1),
        key: "code",
        render: value => {
          return <div title={value}>{value}</div>;
        },
      },
      {
        title: 'ĐVT',
        dataIndex: 'unitName',
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.unitName ? a.unitName.localeCompare(b.unitName) : -1),
        key: "unitName",
      },
      ...columnStock,
      parseInt(this.props.Manufacture) === Constants.MANUFACTURE_OPTIONS.ON ?
        {
          title: "Kho sản xuất",
          align: 'center',
          dataIndex: 'manufacturingQuantity',
          key: "manufacturingQuantity",
          sortDirections: ["descend", "ascend"],
          sorter: (a, b) => (a.manufacturingQuantity - b.manufacturingQuantity),
          render: value => {
            return value ? ExtendFunction.FormatNumber(value) : 0
          }
        } : {},
      {
        title: "Giá vốn",
        align: 'right',
        width: 120,
        dataIndex: 'costUnitPrice',
        key: "costUnitPrice",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.costUnitPrice - b.costUnitPrice),
        render: value => {
          return value ? ExtendFunction.FormatNumber(Number(value).toFixed(0)) : 0
        }
      },
      {
        title: "SL tồn kho",
        align: 'right',
        dataIndex: 'totalStockQuantity',
        key: "totalStockQuantity",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.totalStockQuantity - b.totalStockQuantity),
        render: value => {
          return value ? ExtendFunction.FormatNumber(value) : 0
        }
      },
      {
        title: "Giá trị tồn kho",
        align: 'center',
        width: 140,
        dataIndex: 'total',
        key: "total",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.total - b.total),
        render: value => {
          return value ? ExtendFunction.FormatNumber(Number(value).toFixed(0)) : 0
        }
      },
    ];
    return (
      <div>
        <Fragment>
          <Card>
            <CardBody >
              <GridContainer style={{ padding: '0px 15px' }} alignItems="center">
                <OhToolbar
                  left={[
                    {
                      type: "button",
                      label: t("Xuất báo cáo"),
                      typeButton: "export",
                      onClick: () => this.export(),
                      icon: <MdVerticalAlignBottom />,
                    },
                  ]}
                />
                <GridItem>
                  <span className="TitleInfoForm">{t("Theo")}</span>
                </GridItem>

                <GridItem style={{ width: "310px" }} >
                  <OhMultiChoice
                    dataSourcePType={this.state.dataSourcePType}
                    placeholder={t("Chọn theo nhóm sản phẩm")}
                    onChange={(selectTypes) => {
                      this.setState({
                        selectTypes: selectTypes,
                      })
                    }}
                    defaultValue={this.state.selectTypes}
                  />
                </GridItem>
                <GridItem style={{ width: "190px" }} >
                  <OhMultiChoice
                    dataSourcePType={this.state.dataSourceProduct}
                    placeholder={t("Chọn theo sản phẩm")}
                    onChange={(selectProducts) => {
                      this.setState({
                        selectProducts: selectProducts,
                      })
                    }}
                    defaultValue={this.state.selectProducts}
                    maxValue={10}
                    className='reportSelect'
                  />
                </GridItem>
                <GridItem >
                  <OhToolbar
                    right={[
                      {
                        type: "button",
                        label: t("Xem báo cáo"),
                        onClick: () => this.getData(dateTime.start, dateTime.end, 0),
                        icon: <MdViewList />,
                        simple: true,
                        typeButton: "add",
                      },
                      {
                        type: "button",
                        label: t("Tùy chỉnh hiển thị"),
                        icon: <AiOutlineSetting />,
                        onClick: () => this.tableRef.openSetting(),
                        typeButton: "export",
                        simple: true,
                      },
                    ]}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <CardBody style={{ width: "100%" }}>
                  {this.state.totalProduct > 1000 ?
                    <GridItem >
                      <GridContainer >
                        <FormLabel className="ProductFormAddEdit" style={{ margin: 0, marginRight: 15 }}>
                          <b className='HeaderForm'>{t("Hiển thị 1–{{maxLength}} của {{total}} kết quả. Để thu hẹp kết quả, bạn hãy chỉnh sửa phạm vi thời gian hoặc chọn Xuất báo cáo để xem đầy đủ", { maxLength: 1000, total: this.state.totalProduct })}</b>
                        </FormLabel>
                      </GridContainer>
                    </GridItem>
                    :
                    <GridItem>
                      <GridContainer justify="flex-end">
                        <FormLabel className="ProductFormAddEdit" style={{ margin: 0, marginRight: 15 }}>
                          {this.state.hasTotal ?
                            <b className='HeaderForm'>{t("Tổng giá trị tồn kho")}: {ExtendFunction.FormatNumberToInt(totalInventory)}</b> :
                            <b className='HeaderForm'>{t("Tổng")}: {0}</b>
                          }
                        </FormLabel>
                      </GridContainer>
                    </GridItem>
                  }
                  <OhTable
                    id="inventory-report"
                    onRef={ref => this.tableRef = ref}
                    columns={columns}
                    dataSource={dataInventoryReport.length > 1000 ? dataInventoryReport.slice(0, 1000) : dataInventoryReport}
                    isNonePagination={true}
                    loading={loading}
                    onChangeViewColumn={(column) => {
                      this.viewColumn = column;
                    }}
                  />
                </CardBody>
              </GridContainer>
            </CardBody>
          </Card>
        </Fragment>
      </div>
    );
  }
}

InventoryReport.propTypes = {
  classes: PropTypes.object.isRequired
};

export default (
  connect(function (state) {
    return {
      stockList: state.stockListReducer.stockList,
      productList: state.productListReducer.products,
      nameBranch: state.branchReducer.nameBranch,
      productTypes: state.productTypeReducer.productTypes,
      Manufacture: state.reducer_user.Manufacture,
    };
  })
)(
  withTranslation("translations")(
    withStyles(theme => ({
      ...regularFormsStyle
    }))(InventoryReport)
  )
);
