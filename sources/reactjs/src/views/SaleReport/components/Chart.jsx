
import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import { withTranslation } from "react-i18next";
import ReactEcharts from "echarts-for-react";
import Constants from "variables/Constants/";
import ExtendFunction, { trans } from "lib/ExtendFunction";
import { connect } from "react-redux";
import chartsStyle from "assets/jss/material-dashboard-pro-react/views/chartsStyle.jsx";

const option_pie = {
  title: {
  },
  tooltip: {
    trigger: 'item',
  },
  color: ['#3398DB', '#2552d9', '#5173d6', '#5184d6', '#6e9ae0', '#6eabe0', '#65add6', "#65bcd6", '#65c3d6', '#65cdd6', '#65d6d4', '#65d6cd', '#65d6c3', '#65d6b8', '#65d6ad', '#65d6a1', '#65d68b'], 
  grid: {
    left: '3%',
    right: '4%',
    top: "0%",
    containLabel: true
  },
  // legend: {
  //   type: 'scroll',
  //   orient: 'vertical',
  //   right: 10,
  //   top: 20,
  //   bottom: 20,
  //   data: [],
  // },
  series: {
    type: 'pie',
    radius: '80%',
    center: ['50%', '50%'],
    emphasis: {
      itemStyle: {
        shadowBlur: 10,
        shadowOffsetX: 0,
        shadowColor: 'rgba(0, 0, 0, 0.5)'
      }
    },
    label: {
      position: 'outer',
      alignTo: 'none',
      bleedMargin: 5
    },
    data: []
  }
}

const option_bar = {
  title: {
    left: 'center',
  },
  color: ['#3398DB', '#047823', '#f3ec2a'],  
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow',
      animation: false
    },
    confine: true
  },
  legend: {},
  axisPointer: {
    link: {xAxisIndex: 'all'}
  },
  dataZoom: [
    {
      type: 'slider',
      show: true,
      realtime: true,
    },
    {
      type: 'inside',
      realtime: true,
    }
  ],
  grid: {
    left: '3%',
    right: '4%',
    containLabel: true
  },
  xAxis: {
    type: 'category',
    data: [],
    axisTick: {
      alignWithLabel: true
    },
    splitLine: {
      show: false
    },
  },
  yAxis:
  {
    type: 'value',
    data: []
  },
  series: []
};


class Charts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSaleReport: [],
      isCheckData: false
    }

  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.data.length !== this.props.data.length
      || JSON.stringify(prevProps.data) !== JSON.stringify(this.props.data)
      || (this.props.isChangeChart !== prevProps.isChangeChart && this.props.isChangeChart)
    ) {
      this.setState({
        dataSaleReport: this.props.data,
        options: this.props.options,
        selects: this.props.selects
      }, () => {
        this.props.onChangeChart(false);

        if (this.props.options === Constants.OPTIONS_SALE_REPORT.PRODUCT_GROUP) {
          this.prepare_pie_option(option_pie)
        }
        else this.prepare_bar_option(option_bar)
      })

    }

    if (this.props.language !== prevProps.language) {
      if (this.props.options === Constants.OPTIONS_SALE_REPORT.PRODUCT_GROUP) {
        this.prepare_pie_option(option_pie)
      }
      else this.prepare_bar_option(option_bar)
    }
  }

  prepare_pie_option = option_pie => {
    let { t } = this.props;
    let dataPie = [];
    let dataPieAmount =[];
    let { dataSaleReport } = this.state;    

    if (dataSaleReport.length < 10) {
      for (let item of dataSaleReport) {
        dataPie.push({name: item.productTypeName, value: item.quantity});
        dataPieAmount.push({ name: item.productTypeName, value: item.finalAmount });
      }
    }
    else {
      let total = dataSaleReport.reduce((total, item) => {
        total[0] += item.quantity;
        total[1] += item.finalAmount;
        return total;
      }, [0, 0]);

      let valueAmountOther = 0;

      for (let item of dataSaleReport) {
        if (total && item.finalAmount/total[1] < 0.01 ) {
          valueAmountOther += item.finalAmount;
        }
        else {
          dataPieAmount.push({ name: item.productTypeName, value: item.finalAmount })
        }
      }

      if (valueAmountOther) {
        dataPieAmount.push({ name: "Nhóm sản phẩm khác", value: valueAmountOther })
      }
    }

    option_pie.tooltip = {
      ...option_pie.tooltip,
      formatter: (value) => `${value.name}: ${ExtendFunction.FormatNumber(value.value)} (${value.percent}%)`
    }

    option_pie.title = {
      ...option_pie.title,
      text: t("Doanh thu")
    }

    option_pie.series = {
      ...option_pie.series,
      data: dataPieAmount
    }

    if (this.refPieTwo)
      this.refPieTwo.getEchartsInstance().setOption(option_pie)
  }

  prepare_bar_option = option_bar => {
    let dataXAxis = [];
    let dataYAxis = [];
    let dataYAxisPaidAmount = [];
    let dataYAxisProfit = [];
    let { t } = this.props;
    let { dataSaleReport, options } = this.state;

    switch (options) {
      case Constants.OPTIONS_SALE_REPORT.HOUR: {
        for (let item of dataSaleReport) {
          let split = item.time.split(" ");

          dataXAxis.push(`${split[0]}:00\n${split[1]}`);
          dataYAxis.push(item.finalAmount);
          dataYAxisPaidAmount.push(item.paidAmount);
          dataYAxisProfit.push(item.profitAmount);
        }

        break;
      }
      case Constants.OPTIONS_SALE_REPORT.DAY:
      case Constants.OPTIONS_SALE_REPORT.MONTH:
      case Constants.OPTIONS_SALE_REPORT.YEAR: {
        for (let item of dataSaleReport) {
          dataXAxis.push(item.time);
          dataYAxis.push(item.finalAmount);
          dataYAxisPaidAmount.push(item.paidAmount);
          dataYAxisProfit.push(item.profitAmount);
        }

        break;
      }
      case Constants.OPTIONS_SALE_REPORT.PRODUCT: {
        for (let item of dataSaleReport) {
          dataXAxis.push(trans(item.productName, true));
          dataYAxis.push(item.finalAmount);
          dataYAxisProfit.push(item.profitAmount);
        }

        break;
      }
      case Constants.OPTIONS_SALE_REPORT.USER:
      case Constants.OPTIONS_SALE_REPORT.CUSTOMER: {
        for (let item of dataSaleReport) {
          dataXAxis.push(item.name);
          dataYAxis.push(item.finalAmount);
          dataYAxisPaidAmount.push(item.paidAmount);
          dataYAxisProfit.push(item.profitAmount);
        }

        break;
      }
      default:
        break;
    }

    option_bar.yAxis.name = t("Số tiền")
    option_bar.xAxis = {
      ...option_bar.xAxis,
      data: dataXAxis,
      axisLabel: {
        ...option_bar.xAxis.axisLabel,
        rotate: dataXAxis.length > 8 ? 50 : 0,
        formatter: (function (value) {
          return dataXAxis.length > 13 ? ""
            : value.length > 16 ? value.slice(0, 16) + "..." : value
        })
      },
      axisLine: {onZero: false}
    }
    option_bar.legend.data =  [t("Lợi nhuận"), t("Doanh thu"), t("Thu tiền")];
    option_bar.series = [
      {
        type: 'bar',
        name: t("Lợi nhuận"),
        data: dataYAxisProfit,
        areaStyle: {}
      },     
      {
        type: 'bar',
        name: t("Doanh thu"),
        data: dataYAxis,
        areaStyle: {}
      },
      {
        type: 'bar',
        name: t("Thu tiền"),
        data: dataYAxisPaidAmount,
        areaStyle: {}
      },
    ];

    if (this.ref)
      this.ref.getEchartsInstance().setOption(option_bar)
  }

  render() {
    let { options, dataSaleReport} = this.state;
    return (
      <> 
      { dataSaleReport.length ?        
          options === Constants.OPTIONS_SALE_REPORT.PRODUCT_GROUP ? 
            <ReactEcharts
              key="option_pie_two"
              option={ option_pie}
              style={{ height: 500, width: "100%" }}
              ref={(ref) => this.refPieTwo = ref}
            />
            :
            <ReactEcharts
              key="option_bar"
              option={ option_bar}
              style={{ height: 360, width: "100%" }}
              ref={(ref) => this.ref = ref}            
            />        
       : null }
      </>
    );
  }
}

Charts.propTypes = {
  classes: PropTypes.object
};

export default connect(
  function (state) {
    return {
      languageCurrent: state.languageReducer.language
    };
  }
)(withTranslation("translations")(withStyles(chartsStyle)(Charts)));




