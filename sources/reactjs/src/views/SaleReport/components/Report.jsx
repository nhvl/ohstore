import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import { withTranslation } from 'react-i18next';
import dashboardStyle from "assets/jss/material-dashboard-pro-react/views/dashboardStyle";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import moment from "moment";
import ExtendFunction from "lib/ExtendFunction";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import { Select } from "antd";
import TableReport from "./TableReport.jsx";
import Chart from "./Chart.jsx"
import Constants from "variables/Constants/";
import Export from "../Export/SaleReportExcel.js";
import { MdViewList, MdVerticalAlignBottom} from "react-icons/md";
import OhToolbar from "components/Oh/OhToolbar";
import OhDateTimePicker from 'components/Oh/OhDateTimePicker';
import OhMultiChoice from "components/Oh/OhMultiChoice.jsx";
import _ from "lodash";
import { trans } from "lib/ExtendFunction";

const { Option } = Select;
const SELECT_VALUE_RETURN = 2;
const SELECT_NAME = "selects";
const SELECT_OPTIONS = "options";

class Report extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSaleReport: [],
      options: 2,
      selects: 1,
      InputValue: [moment().startOf('month'),moment().endOf('month')],
      options_get_data: 2,
      isChangeChart: false,
      isViewChart: true,
      isDisableSelect: false,
      startTime: moment().startOf('month').valueOf(),
      endTime: moment().endOf('month').valueOf(),
      isCheckGetData: true,
      selectFilters: []
    }

  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.data.length !== this.props.data.length 
      || JSON.stringify(prevProps.data) !== JSON.stringify(this.props.data)
      || (prevProps.isChange !== this.props.isChange && this.props.isChange)
    ) {
      this.getData()
    }

    if (prevProps.options_get_data !== this.props.options_get_data) {
      this.setOption(this.props.options_get_data)
    }
  }

  setOption(options_get_data) {
    this.setState({ options_get_data })
  }

  getData = async () => {
    let dataSource = this.props.data || [{}];
    let dataSaleReport = [];
    for (let i in dataSource) {
      dataSaleReport.push({
        ...dataSource[i],
        finalAmount: (dataSource[i].totalAmount + dataSource[i].taxAmount + dataSource[i].deliveryAmount - (dataSource[i].discountAmount + dataSource[i].returnAmount))
      })
    }
    this.setState({
      dataSaleReport: dataSaleReport,
      isChangeChart: true,
      isCheckGetData: false
    }, () => this.props.onChangeData(false));
  }

  onChange = async (value, name) => {
    this.setState({
      [name]: value
    }, () => name === SELECT_NAME && value === SELECT_VALUE_RETURN
      && (this.state.options === Constants.OPTIONS_SALE_REPORT.USER || this.state.options === Constants.OPTIONS_SALE_REPORT.CUSTOMER) ?
      this.setState({ options: Constants.OPTIONS_SALE_REPORT.HOUR }) : null);
  }

  getToltal() {
    let { dataSaleReport } = this.state;
    return dataSaleReport.reduce((total, item) => {
      return total += item.finalAmount
    }, 0)
  }

  getToltalProfit() {
    let { dataSaleReport } = this.state;
    return dataSaleReport.reduce((total, item) => {
      return total += item.profitAmount
    }, 0)
  }

  getToltalTax() {
    let { dataSaleReport } = this.state;
    return dataSaleReport.reduce((total, item) => {
      return total += item.taxAmount
    }, 0)
  }

  getToltalDiscount() {
    let { dataSaleReport } = this.state;
    return dataSaleReport.reduce((total, item) => {
      return total += item.discountAmount
    }, 0)
  }

  renderPlaceHolder() {
    let { options } = this.state;
    let { t } = this.props;
    let placeHolder = ""

    switch (options) {
      case Constants.OPTIONS_SALE_REPORT.PRODUCT:
        placeHolder = t("Chọn tên sản phẩm")
        break;
      case Constants.OPTIONS_SALE_REPORT.PRODUCT_GROUP:
        placeHolder = t("Chọn nhóm sản phẩm")
        break;
      case Constants.OPTIONS_SALE_REPORT.USER:
        placeHolder = t("Chọn tên nhân viên")
        break;
      case Constants.OPTIONS_SALE_REPORT.CUSTOMER:
        placeHolder = t("Chọn tên khách hàng")
        break;
      default:
        break;
    }

    return placeHolder;
  }

  dataSelectMulti = () => {
    let { options } = this.state;
    let dataOptions = [];

    switch (options) {
      case Constants.OPTIONS_SALE_REPORT.PRODUCT:
        dataOptions = _.cloneDeep(this.props.productList).map(item => ({...item, name: trans(item.name, true)}))
        break;
      case Constants.OPTIONS_SALE_REPORT.PRODUCT_GROUP:
        dataOptions = _.cloneDeep(this.props.productTypes)
        break;
      case Constants.OPTIONS_SALE_REPORT.USER:
        dataOptions = _.cloneDeep(this.props.users).map(item => ({ ...item, name: item.fullName }))
        break;
      case Constants.OPTIONS_SALE_REPORT.CUSTOMER:
        dataOptions = _.cloneDeep(this.props.customers)
        break;
      default:
        break;
    }

    return dataOptions;
  }

  render() {
    const { t } = this.props
    const { dataSaleReport, options, selects, options_get_data, InputValue, isViewChart, isDisableSelect, startTime, endTime } = this.state;
    let total = this.getToltal();
    let totalProfit = this.getToltalProfit();
    let totalDiscount = this.getToltalDiscount();
    let totalTax = this.getToltalTax();

    const selectFollow = [
      <Option value={1} >{t("Giờ")}</Option>,
      <Option value={2} >{t("Loại ngày")}</Option>,
      <Option value={3} >{t("Tháng")}</Option>,
      <Option value={4} >{t("Năm")}</Option>,
      <Option value={5} >{t("Sản phẩm")}</Option>,
      <Option value={6} >{t("Nhóm sản phẩm")}</Option>,
      <Option value={7} >{t("Những nhân viên")}</Option>,
      <Option value={8} >{t("Khách hàng")}</Option>
    ];

    return (
      <div>
        <GridContainer style={{ padding: '0px 10px 0px 20px' }}alignItems="center">
              <OhToolbar
                left={[
                  {
                    label: t("Xuất báo cáo"),
                    type: "csvlink",
                    typeButton:"export",
                    csvData: Export.SaleReportExcel(dataSaleReport, options_get_data, total, this.props.languageCurrent, t, totalProfit, totalDiscount, totalTax),
                    fileName: t("BaoCaoBanHang") + ".xls",
                    onClick: () => { },
                    icon: <MdVerticalAlignBottom />,
                  }
                ]}
              />
              <>
                <GridItem >
                  <OhDateTimePicker defaultValue={InputValue} onChange={(start,end) => {
                    let dateTime = {start: start, end: end};
                    this.setState({
                      startTime: new Date(start).getTime(),
                      endTime: new Date(end).getTime()
                    })
                    this.props.onChangeTime(dateTime)
                    }}
                  />
                </GridItem>
              </>
              <>
                <GridItem>
                  <span className="TitleInfoForm">{t("Theo")}</span>
                </GridItem>
                <GridItem style={{ width: "185px" }} id="autocompleteItem">
                  <Select
                    getPopupContainer={() => document.getElementById('autocompleteItem')}
                    disabled={false}
                    style={{ width: 200, height: "35px"}}
                    size={"large"}
                    placeholder={t("Theo")}
                    optionFilterProp="children"
                    onChange={(e) => {
                      this.onChange(e, SELECT_OPTIONS);
                      this.setState({
                        selectFilters: []
                      })
                    }}
                    name="Options"
                    value={options}
                    filterOption={(input, option) => {
                      return option.props.children
                        ? ExtendFunction.removeSign(option.props.children
                          .toLowerCase())
                          .indexOf(ExtendFunction.removeSign(input.toLowerCase())) >= 0
                        : false;
                    }}
                  >
                    {selectFollow}
                  </Select>
                </GridItem>
                { 
                  isDisableSelect ?
                    <GridItem style={{ width: "185px", marginRight: 20 }}>
                      <OhMultiChoice 
                        dataSourcePType={this.dataSelectMulti()}
                        key={options}
                        placeholder={this.renderPlaceHolder()}
                        onChange={(selectFilters) => {
                          this.setState({
                            selectFilters,
                          })
                        }}
                        defaultValue={this.state.selectFilters}
                        className='reportSelect'
                      />
                    </GridItem> 
                    : null 
                }
                <GridItem >
                  <OhToolbar
                    left={[
                      {
                        type: "button",
                        label: t("Xem báo cáo"),
                        onClick: () => {
                          let check = options === Constants.OPTIONS_SALE_REPORT.PRODUCT || options === Constants.OPTIONS_SALE_REPORT.USER || options === Constants.OPTIONS_SALE_REPORT.CUSTOMER
                          this.setState({ 
                            isViewChart: check ? false : true,
                            isDisableSelect: check || options === Constants.OPTIONS_SALE_REPORT.PRODUCT_GROUP ? true : false,
                            isCheckGetData: true
                          })
                          this.props.getData(this.state.options, this.state.selectFilters)
                        },
                        icon: <MdViewList />,
                        simple: true,
                        typeButton:"add",
                      },
                    ]}
                  />
                </GridItem>
              </>
        </GridContainer>
        <GridContainer >
          { isViewChart ? 
            <GridItem xs={12} >            
                <Chart
                  data={dataSaleReport}
                  options={options}
                  selects={selects}
                  language={this.props.languageCurrent}
                  isChangeChart={this.state.isChangeChart}
                  onChangeChart={isChangeChart => this.setState({isChangeChart})}
                /> 
            </GridItem> : null
          }
          <GridItem xs={12} >
            <TableReport
              data={dataSaleReport}
              options={options_get_data}
              total={total}
              totalProfit={totalProfit}
              totalDiscount={totalDiscount}
              totalTax={totalTax}
              startTime={startTime}
              endTime={endTime}
            />
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

Report.propTypes = {
  classes: PropTypes.object.isRequired
};

export default connect(
  function (state) {
    return {
      languageCurrent: state.languageReducer.language,
      users: state.userListReducer.users,
      productList: state.productListReducer.products,
      productTypes: state.productTypeReducer.productTypes,
      customers: state.customerListReducer.customers
    };
  }
)(withTranslation("translations")(withStyles(dashboardStyle)(Report)));
