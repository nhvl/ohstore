import React, { Component } from 'react';
import CardBody from 'components/Card/CardBody';
import OhTable from 'components/Oh/OhTable';
import ExtendFunction , { customExpandIcon } from 'lib/ExtendFunction';
import moment from 'moment';
import { withTranslation } from "react-i18next";
import Constants from 'variables/Constants';
import { notifyError } from 'components/Oh/OhUtils';
import _ from 'lodash';
import SaleReportService from 'services/SaleReportService';
import Expend2Table from './Expend2Table';

class ExpendTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      totalTable: 0,
      loading: false
    }
    this.filters = {}
  }

  componentDidUpdate(prevProps, prevState) {
    if (JSON.stringify(this.props.data) !== JSON.stringify(prevProps.data)) {
      this.getDataExpend()
    }
  }

  componentDidMount() {
    this.getDataExpend()
  }

  getDataExpend = async () => {
    let { options, startTime, endTime, data, t } = this.props;

    this.setState({
      loading: true,
      dataSource: []
    });

    let filterInvoice = {};
    let filterInvoiceReturn = {};

    try {
      switch (options) {
        case Constants.OPTIONS_SALE_REPORT.HOUR: 
          let startHour = moment(data.time, "HH DD/MM/YYYY").startOf('hour').valueOf();
          let endHour = moment(data.time, "HH DD/MM/YYYY").endOf('hour').valueOf();

          filterInvoice = _.extend(filterInvoice || {}, { invoiceAt: {"<=": endHour, ">=": startHour } })
          filterInvoiceReturn = _.extend(filterInvoiceReturn || {}, { importedAt: {"<=": endHour, ">=": startHour } })
          break;
        case Constants.OPTIONS_SALE_REPORT.DAY:
          let startDay = moment(data.time, "DD/MM/YYYY").startOf('day').valueOf();
          let endDay = moment(data.time, "DD/MM/YYYY").endOf('day').valueOf();
        
          filterInvoice = _.extend(filterInvoice || {}, { invoiceAt: {">=": startDay, "<=": endDay } })
          filterInvoiceReturn = _.extend(filterInvoiceReturn || {}, { importedAt: {">=": startDay, "<=": endDay } })
          break;
        case Constants.OPTIONS_SALE_REPORT.MONTH:
          let startMonth = moment(data.time, "MM/YYYY").startOf('month').valueOf();
          let endMonth = moment(data.time, "MM/YYYY").endOf('month').valueOf();

          filterInvoice = _.extend(filterInvoice || {}, { invoiceAt: {">=": startMonth, "<=": endMonth } })
          filterInvoiceReturn = _.extend(filterInvoiceReturn || {}, { importedAt: {">=": startMonth, "<=": endMonth } })
          break;
        case Constants.OPTIONS_SALE_REPORT.YEAR:
          let startYear = moment(data.time, "YYYY").startOf('year').valueOf();
          let endYear = moment(data.time, "YYYY").endOf('year').valueOf();

          filterInvoice = _.extend(filterInvoice || {}, { invoiceAt: {">=": startYear, "<=": endYear } })
          filterInvoiceReturn = _.extend(filterInvoiceReturn || {}, { importedAt: {">=": startYear, "<=": endYear } })
          break;     
        case Constants.OPTIONS_SALE_REPORT.CUSTOMER:
          filterInvoice = _.extend(filterInvoice || {}, { invoiceAt: {">=": startTime, "<=": endTime }, customerId: data.id })
          filterInvoiceReturn = _.extend(filterInvoiceReturn || {}, { importedAt: {">=": startTime, "<=": endTime }, recipientId: data.id })
          break;
        case Constants.OPTIONS_SALE_REPORT.USER:
          filterInvoice = _.extend(filterInvoice || {}, { invoiceAt: {">=": startTime, "<=": endTime }, createdBy: data.id })
          filterInvoiceReturn = _.extend(filterInvoiceReturn || {}, { importedAt: {">=": startTime, "<=": endTime }, createdBy: data.id })
          break;
        case  Constants.OPTIONS_SALE_REPORT.PRODUCT:
          let filterInvoiceProductId = "invoiceproduct.productId";
          let filterImportProductId = "importcardproduct.productId";
          filterInvoice = _.extend(filterInvoice || {}, { invoiceAt: {">=": startTime, "<=": endTime }, [filterInvoiceProductId]: data.productId } )
          filterInvoiceReturn = _.extend(filterInvoiceReturn || {}, { importedAt: {">=": startTime, "<=": endTime }, [filterImportProductId]: data.productId } )
          break;
        case  Constants.OPTIONS_SALE_REPORT.PRODUCT_GROUP:
          let filterProductTypeId = "product.productTypeId";
          let filterInvoiceTime = "invoice.invoiceAt";
          let filterImportTime = "importcard.importedAt";
          filterInvoice = _.extend(filterInvoice || {}, { [filterInvoiceTime]: {">=": startTime, "<=": endTime }, [filterProductTypeId]: data.productTypeId } )
          filterInvoiceReturn = _.extend(filterInvoiceReturn || {}, { [filterImportTime]: {">=": startTime, "<=": endTime }, [filterProductTypeId]: data.productTypeId } )
          break;  
        default:
          break;
      }

      let query = {
        filterInvoice,
        filterInvoiceReturn,
        options
      }

      let getData = await SaleReportService.getExpandSaleReportData(query);
      
      if(getData.status) {
        this.setState({
          dataSource: getData.data,
          totalTable: getData.count
        })
      }
      else throw getData.message
    }
    catch(err) {
      if (typeof err === "string") notifyError(err)
      else notifyError(t("Lỗi lấy dữ liệu"))
    } 

    this.setState({
      loading: false
    })
  }

  getColumn = () => {
    let { t, options } = this.props;
    
    let columns = [
      {
        title: t("Mã HD"),
        align: "left",
        dataIndex: "code",
        key: "code",
        width: "16%",
        sorter: (a,b) =>  a.code.localeCompare(b.code),
        render: value => {
          return <div title={value}>{value}</div>
        },
      },
      {
        title: t("Thời gian"),
        align: "left",
        dataIndex: "time",
        key: "time",
        width: "22%",
        sorter: (a,b) => a.time - b.time,
        render: value => moment(value).format(Constants.DISPLAY_DATE_TIME_FORMAT_STRING_TIME_FORMAT)
      },
      {
        title: t("Tổng tiền"),
        align: "right",
        dataIndex: "totalAmount",
        key: "totalAmount",
        width: "16%",
        sorter: (a,b) => a.finalAmount - b.finalAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Chiết khấu"),
        align: "right",
        dataIndex: "discountAmount",
        key: "discountAmount",
        width: "16%",
        sorter: (a,b) => a.finalAmount - b.finalAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Thuế"),
        align: "right",
        dataIndex: "taxAmount",
        key: "taxAmount",
        width: "16%",
        sorter: (a,b) => a.taxAmount - b.taxAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Thành tiền"),
        align: "right",
        dataIndex: "finalAmount",
        key: "finalAmount",
        width: "16%",
        sorter: (a,b) => a.finalAmount - b.finalAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Thanh toán"),
        align: "right",
        dataIndex: "paidAmount",
        key: "paidAmount",
        width: "16%",
        sorter: (a,b) => a.paidAmount - b.paidAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      }
    ]

    let columnProducts = [
      {
        title: t("Mã HD"),
        align: "left",
        dataIndex: "code",
        key: "code",
        width: "16%",
        sorter: (a,b) =>  a.code.localeCompare(b.code),
        render: value => {
          return <div title={value}>{value}</div>
        },
      },
      {
        title: t("Thời gian"),
        align: "left",
        dataIndex: "time",
        key: "time",
        width: "22%",
        sorter: (a,b) => a.time - b.time,
        render: value => moment(value).format(Constants.DISPLAY_DATE_TIME_FORMAT_STRING_TIME_FORMAT)
      },
      {
        title: t("Số lượng"),
        align: "right",
        dataIndex: "quantity",
        key: "quantity",
        width: "16%",
        sorter: (a,b) => a.quantity - b.quantity,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Thành tiền"),
        align: "right",
        dataIndex: "finalAmountProduct",
        key: "finalAmountProduct",
        sorter: (a,b) => a.finalAmountProduct - b.finalAmountProduct,
        width: "16%",
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      }
    ]

    let columnProductType = [
      {
        title: t("Mã SP"),
        align: "left",
        dataIndex: "code",
        key: "code",
        sorter: (a,b) =>  a.code.localeCompare(b.code),
        width: "16%",
        render: value => {
          return <div title={value}>{value}</div>
        },
      },
      {
        title: t("Số lượng bán"),
        align: "right",
        dataIndex: "quantity",
        key: "quantity",
        width: "16%",
        sorter: (a,b) => a.quantity - b.quantity,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Tổng tiền bán"),
        align: "right",
        dataIndex: "totalAmount",
        key: "totalAmount",
        sorter: (a,b) => a.totalAmount - b.totalAmount,
        width: "16%",
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Chiết khấu bán"),
        align: "right",
        dataIndex: "discountAmount",
        key: "discountAmount",
        width: "16%",
        sorter: (a,b) => a.discountAmount - b.discountAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Thành tiền bán"),
        align: "right",
        dataIndex: "finalAmount",
        key: "finalAmount",
        width: "16%",
        sorter: (a,b) => a.finalAmount - b.finalAmount,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },     
      {
        title: t("Số lượng trả"),
        align: "right",
        dataIndex: "quantityReturn",
        key: "quantityReturn",
        width: "16%",
        sorter: (a,b) => a.quantityReturn - b.quantityReturn,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Chiết khấu trả"),
        align: "right",
        dataIndex: "discountAmountReturn",
        key: "discountAmountReturn",
        width: "16%",
        sorter: (a,b) => a.discountAmountReturn - b.discountAmountReturn,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
      {
        title: t("Tổng tiền trả"),
        align: "right",
        dataIndex: "finalAmountReturn",
        key: "finalAmountReturn",
        width: "16%",
        sorter: (a,b) => a.finalAmountReturn - b.finalAmountReturn,
        render: (value, record) => {
          return ExtendFunction.FormatNumber(value)
        }
      },
    ];

    if (options === Constants.OPTIONS_SALE_REPORT.PRODUCT_GROUP) {
      return columnProductType
    } 
    else if (options === Constants.OPTIONS_SALE_REPORT.PRODUCT) {
      return columnProducts
    }
    else return columns
  }

  render() {
    let { dataSource, totalTable, loading } = this.state;
    let { data, options } = this.props;
    let checkProductType = options === Constants.OPTIONS_SALE_REPORT.PRODUCT_GROUP;
    return (
      <CardBody className="table-sale-report-view-expand">
        <OhTable
          id= {"expend-sale-report" + (data.time || data.productName || data.name || data.productTypeName)}
          dataSource={dataSource}
          loading={loading}
          total={totalTable}
          columns={this.getColumn()}
          className="table-sale-report-view-1"
          isNonePagination={totalTable > 10 ? false : true}
          isKeepExpand={true}
          expandIcon={(props) => checkProductType ? "" : customExpandIcon(props)}
          isExpandable={!checkProductType}
          expandedRowRender={(record) => (
              <CardBody >
                <Expend2Table data={record} options={options}/>
              </CardBody>
            )
          }
          rowClassName={() => {
            if (!checkProductType)
            return 'rowOhTable';
          }}
        />
      </CardBody>
    );
  }
}

export default withTranslation("translations")(ExpendTable);