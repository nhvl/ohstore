import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Button from "components/CustomButtons/Button.jsx";
import { connect } from "react-redux";
import adminNavbarLinksStyle from "assets/jss/material-dashboard-pro-react/components/adminNavbarLinksStyle.jsx";
import { Redirect } from "react-router-dom";
import { withTranslation } from "react-i18next";
import userService from "services/UserService";
import { Popover} from "antd";
import { IoIosMenu } from "react-icons/io";

class MenuSales extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openNotification: false,
      openProfile: false,
      redirect: false,
      path: "",
    };

  }
  
  handleClickNotification = () => {
    this.setState({ openNotification: !this.state.openNotification });
  };
  handleCloseNotification = () => {
    this.setState({ openNotification: false });
  };

  handleClickMyProfile = () => {
    if (this.state.redirect) {
      this.setState({ redirect: false, openProfile: false });
      return <Redirect to={this.state.path} />;
    } else {
      this.setState({ openProfile: false });
    }
  };
  setRedirect = path => {
    this.setState({
      redirect: true,
      path: path
    });
  };
  logOut = () => {
    userService.logout();
  };
  handleClickProfile = () => {
    this.setState({ openProfile: !this.state.openProfile });
  };

  render = () => {
    const { t, classes, rtlActive } = this.props;
    const { openNotification} = this.state;

    const dropdownItem = classNames(classes.dropdownItem, classes.infoHover, { [classes.dropdownItemRTL]: rtlActive });

    const wrapper = classNames({
      [classes.wrapperRTL]: rtlActive
    });
    const managerClasses = classNames({
      [classes.managerClasses]: true
    });
    if (this.state.redirect) {
      this.setState({ redirect: false, openNotification: false });
      return <Redirect to={this.state.path} />;
    }
    
    return (
      <div className={wrapper + " customAvatar"}>
        <div className={managerClasses}>   
          <Popover
            trigger="click"
            getPopupContainer={trigger => trigger.parentNode}
            placement="bottomLeft"
            content={
              <MenuList 
                role="menu"
                
              >
                <MenuItem
                  className={dropdownItem}
                  onClick={() => { this.setRedirect("/admin/my-profile"); this.handleClickMyProfile() }}
                >
                  {t("Hồ sơ cá nhân")}
                </MenuItem>

                {/* <Divider light /> */}
                <MenuItem
                  onClick={this.logOut}
                  className={dropdownItem}
                >
                  {t("Đăng xuất")}
                </MenuItem>
              </MenuList>
            }
          >
            <Button
              color="transparent"
              aria-label={t("Person")}
              justIcon
              aria-owns={openNotification ? "profile-menu-list" : null}
              aria-haspopup="true"
              onClick={this.handleClickProfile}
              className={rtlActive ? classes.buttonLinkRTL : classes.buttonLink}
              muiClasses={{
                label: rtlActive ? classes.labelRTL : ""
              }}
              buttonRef={node => {
                this.anchorProfile = node;
              }}
              style={{ paddingTop: "4px", justifyContent: 'left' }}
            >
              <div className="User" title={t("Menu")}>
                <IoIosMenu color={"#ffffff"} size={30} />
              </div>
            </Button>
          </Popover>
        </div>
      </div>
    );
  };
}

MenuSales.propTypes = {
  classes: PropTypes.object.isRequired,
  rtlActive: PropTypes.bool,
  routerName: PropTypes.string,
};

export default connect(state => {
  return {
    User: state.reducer_user.User,
    state: state,
    language: state.languageReducer.language,
    currentUser: state.userReducer.currentUser,
  };
})(withTranslation("translations")(withStyles(adminNavbarLinksStyle)(MenuSales)));
