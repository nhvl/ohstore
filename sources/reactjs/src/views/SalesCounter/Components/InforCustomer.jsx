import React, { Fragment } from "react";
import { connect } from "react-redux";
import { withTranslation, } from 'react-i18next';
import { Popover } from "antd";
import withStyles from "@material-ui/core/styles/withStyles";
import extendedTablesStyle from "assets/jss/material-dashboard-pro-react/views/extendedTablesStyle.jsx";
import buttonsStyle from "assets/jss/material-dashboard-pro-react/views/buttonsStyle.jsx";
import OhForm from 'components/Oh/OhForm';
import customerService from 'services/CustomerService';
import UserService from 'services/UserService';
import { notifyError } from 'components/Oh/OhUtils';
import Constants from 'variables/Constants/';
import ExtendFunction from "lib/ExtendFunction";
import DiscountForm from './DiscountForm';
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import { Container, Col, Row } from "react-bootstrap";
import TextField from '@material-ui/core/TextField';
import ModalCreateCustomer from "views/Product/components/Product/ModalCreateCustomer";
import OhButton from "components/Oh/OhButton";
import { getUser } from "lib/ExtendFunction";
import { Icon } from "antd";
import _ from "lodash";
class InfoCustomer extends React.Component {
  constructor(props) {
    super(props);

    this.sendData = _.debounce(this.sendData, Constants.UPDATE_TIME_OUT_SEND);
    this.customers = [];

    this.customers = [];
    if (this.props.customers && this.props.customers.length) {
      this.customers = ExtendFunction.sortDefaultCustomer(this.props.customers, Constants.DEFAULT_CUSTOMERS.CUSTOMER);
    }

    this.dataSales = localStorage.getItem("sales-counter") || "";
    this.isJson = JSON.isJson(this.dataSales);
    this.dataCustomers = this.isJson ? JSON.parse(this.dataSales) : {} ;

    this.state = {
      customers: this.customers,
      selectDiscount: false,
      visiblePopTax: false,
      visibleAddCustomer: false,
      totalTemp: 0,
      users: this.props.listUser || [],
      data: this.dataCustomers.dataSalesCounter ?
        (this.dataCustomers.dataSalesCounter[this.props.keyActive] && this.dataCustomers.dataSalesCounter[this.props.keyActive].customerId ? this.dataCustomers.dataSalesCounter 
          : {
            [this.props.keyActive]: {
              ...this.dataCustomers.dataSalesCounter[this.props.keyActive],
              customerId: this.customers[0] && this.customers[0].name === Constants.DEFAULT_CUSTOMERS.CUSTOMER ? this.customers[0].id : undefined,
              customerName: this.customers[0] && this.customers[0].name === Constants.DEFAULT_CUSTOMERS.CUSTOMER ? this.customers[0].name : ''
            }
          })
      :  {
        [this.props.keyActive]: {
          totalAmount: 0,
          discountAmount: 0,
          finalAmount: 0,
          taxAmount: 0,
          payType: 0,
          deliveryAmount: 0,
          paidAmount: 0,
          debtAmount: 0,
          quantityProducts: 0,
          customerId: this.customers[0] && this.customers[0].name === Constants.DEFAULT_CUSTOMERS.CUSTOMER ? this.customers[0].id : undefined,
          customerName: this.customers[0] && this.customers[0].name === Constants.DEFAULT_CUSTOMERS.CUSTOMER ? this.customers[0].name : ''
        }
      },
      prevCustomer: 0,
      moneyCustomerGiven:0,
    };
    this.addressCustomer= '';

    this.props.onRef(this);
    this.filters = {};

  }

  componentDidMount = () => {
    
    this.getCustomer();
    
    this.getDataUser();
    
    document.addEventListener("keydown",(event) =>{
      if(event.code === 'F4') {
        event.preventDefault();
        if (this.selectRef && this.selectRef.ref){
          this.selectRef.ref.focus();
        }
      }

      if(event.code === 'F6') {
          event.preventDefault();
          
          this.setState({
            selectDiscount: !this.state.selectDiscount
        },()=>{
          if (this.discountRef && this.discountRef.ohnumberinputRef && this.discountRef.ohnumberinputRef.numberInputRef){
            this.discountRef.ohnumberinputRef.numberInputRef.focus()
          }
        })
      }

      if(event.code === 'F7') {
        event.preventDefault();
        if (this.inputRef){
          this.inputRef.focus();
          this.inputRef.select();
        }
      }
    }, false);
  }

  getDataUser = async() =>{
    if (!this.props.listUser.length){      
      let dataUser = await UserService.getUserList();
      
      if (dataUser.status){
        this.setState({
          users: dataUser.data
        })
      }
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
    let { dataCustomer } = this.props;
    let {customers} = this.state;
    
    if (prevProps.keyDelete !== this.props.keyDelete) {
      delete this.state.data[this.props.keyDelete];
      
      this.setState({
        data: this.state.data
      }, () => this.sendData())
    }
    
    if ((this.props.listUser.length && this.props.listUser.length !== prevProps.listUser.length)){
      this.setState({
        users: this.props.listUser || [],
      })
    }

    if (prevProps.keyActive !== this.props.keyActive || (dataCustomer && JSON.stringify(prevProps.dataCustomer) !== JSON.stringify(dataCustomer))) {
      let totalAmount = dataCustomer.totalAmount || 0;
      if (dataCustomer.data) {
        if (dataCustomer.data.discountAmount) totalAmount = totalAmount - dataCustomer.data.discountAmount;
        if (dataCustomer.data.taxAmount) totalAmount = totalAmount + dataCustomer.data.taxAmount;
        if (dataCustomer.data.deliveryAmount) totalAmount = totalAmount + dataCustomer.data.deliveryAmount;
         
      }

      this.setState({
        discount: dataCustomer && dataCustomer.data ? dataCustomer.data.discountAmount  : 0,
        isPercentDiscount: false,
        data: {
          ...this.state.data,
          [this.props.keyActive]: {
            ...this.state.data[this.props.keyActive],
            totalAmount: dataCustomer.totalAmount,
            quantityProducts: dataCustomer.quantityProducts,
            finalAmount: Math.round(totalAmount),
            paidAmount:  (dataCustomer.data && dataCustomer.data.payment !== null) ? Math.round(this.state.payment) : Math.round(totalAmount)
          },          
        },
        tax: dataCustomer.data ? dataCustomer.data.taxAmount : 0,
        isPercentTax: false,
        delivery: dataCustomer.data ? dataCustomer.data.deliveryAmount : 0,
        payment: dataCustomer.data && dataCustomer.data.payment ? dataCustomer.data.payment : null,
        totalTemp: dataCustomer.totalAmount
      }, () => {
        this.setState({
          data: {
            ...this.state.data,
            [this.props.keyActive]: {
              ...this.state.data[this.props.keyActive],
              paidAmount:  (dataCustomer.data && dataCustomer.data.payment !== null) ? Math.round(this.state.payment) : Math.round(totalAmount)
            }
          },
          totalTemp: dataCustomer.totalAmount
        },()=>this.sendData())
      })

      if (dataCustomer && dataCustomer.totalAmount === 0){
        this.setState({
            tax:  0,
            isPercentTax: false,
            delivery: 0,
            payment:  null,
            discount:  0,
            isPercentDiscount: false,
            data: {
              ...this.state.data,
              [this.props.keyActive]: {
                ...this.state.data[this.props.keyActive],
                totalAmount: 0,
                discountAmount: 0,
                finalAmount: 0,
                taxAmount: 0,
                payType: 0,
                deliveryAmount: 0,
                paidAmount: 0,
                debtAmount: 0,
                quantityProducts: 0,
                deliveryType: (this.state.data[this.props.keyActive] && this.state.data[this.props.keyActive].deliveryType) || 1,
              }
            },
            totalTemp: 0
          }, () => this.sendData()
        )
      }
      if(dataCustomer && dataCustomer.data && !dataCustomer.data.customerId ){
        this.setState({
          data: {
            ...this.state.data,
            [this.props.keyActive]: {
              ...this.state.data[this.props.keyActive],
              customerId: customers[0] && customers[0].name === Constants.DEFAULT_CUSTOMERS.CUSTOMER ? customers[0].id : undefined,
              customerName: customers[0] && customers[0].name === Constants.DEFAULT_CUSTOMERS.CUSTOMER ? customers[0].name : '',

            }
          }
        }, () => this.sendData())
      }

    }

    if ( prevProps.totalAmount !== this.props.totalAmount && prevProps.keyActive === this.props.keyActive && dataCustomer && dataCustomer.data) {
      this.setState({
        discount: prevProps.totalAmount ? (this.state.discount/prevProps.totalAmount)*this.props.totalAmount : dataCustomer && dataCustomer.data ? dataCustomer.data.discountAmount : 0,
        tax: prevProps.totalAmount ? ((this.state.tax/prevProps.totalAmount)*(this.props.totalAmount)) : dataCustomer && dataCustomer.data ? dataCustomer.data.taxAmount : 0,

      }, () => this.getAmounts())
    }
  }

  getAmounts = (value, isPercent) => {
    let { data, discount, isPercentDiscount, tax, isPercentTax, delivery, payment, totalTemp } = this.state;
    let checkDiscount = isPercent || isPercentDiscount;
    let dataCustomer = data[this.props.keyActive] || {};
    let discountTotal = Math.round(isPercentDiscount ? dataCustomer.totalAmount * discount / 100 : discount);
    let taxTotal =  Math.round(isPercentTax ? ((dataCustomer.totalAmount - discountTotal) * tax / 100 ) :  checkDiscount  ? ((Math.round((tax/totalTemp)*100)/100) * (dataCustomer.totalAmount - discountTotal)) : tax);
    let finalTotal = dataCustomer.totalAmount;

    if (discountTotal)
      finalTotal -= discountTotal;

    if (taxTotal)
      finalTotal += taxTotal;
      
    delivery = Math.round(delivery);
    if (delivery)
      finalTotal += delivery;
          
    this.setState({
      data: {
        ...this.state.data,
        [this.props.keyActive]: {
          ...this.state.data[this.props.keyActive],
          discountAmount: !isNaN(discountTotal) ? discountTotal : 0,
          taxAmount: !isNaN(taxTotal) ? taxTotal : 0,
          deliveryAmount: Math.round(delivery) || 0,
          finalAmount: Math.round(finalTotal),
          paidAmount:    Math.round(payment) || Math.round(finalTotal) ,
          debtAmount: 0,
          totalAmount: dataCustomer.totalAmount,
          payment: payment ? Math.round(payment) : null 
        }
      },
      totalTemp: dataCustomer.totalAmount - discountTotal
    }, () => {
      if (value === '0'){                
        this.props.sendProductsData(this.state.data[this.props.keyActive], this.state.data); 
      } else {
        this.sendData()
      }
    })
  }

  sendData = () => {
    let { data } = this.state;
    this.props.sendProductsData(data[this.props.keyActive], this.state.data)
  }

  getCustomer = async () => {
    
    const query = {
      sort: "name ASC"
    };    

    let getCustomer = await customerService.getCustomers(query);

    if (getCustomer.status) {
      let customers =  ExtendFunction.sortDefaultCustomer(getCustomer.data, Constants.DEFAULT_CUSTOMERS.CUSTOMER)
      this.setState({ 
        customers,
      })

      if(!this.dataCustomers.length || (this.dataCustomers.dataSalesCounter && this.dataCustomers.dataSalesCounter[this.props.keyActive] && !this.dataCustomers.dataSalesCounter[this.props.keyActive].customerId)){
        this.setState({ 
          data: {
            ...this.state.data,
            [this.props.keyActive]: {
              ...this.state.data[this.props.keyActive],
              customerId: this.state.data[this.props.keyActive].customerId ? this.state.data[this.props.keyActive].customerId : (customers[0] && customers[0].name === Constants.DEFAULT_CUSTOMERS.CUSTOMER ? customers[0].id : undefined),
              customerName: this.state.data[this.props.keyActive].customerName ? this.state.data[this.props.keyActive].customerName : (customers[0] && customers[0].name === Constants.DEFAULT_CUSTOMERS.CUSTOMER ? customers[0].name : '')
            }
          },
        }, () => this.sendData())
      }
    }
    else notifyError(getCustomer.message)
  }

  onChangeNotes = (value) => {
    let { data } = this.state; 
    let { keyActive } =  this.props; 

    this.setState({
      data: {
        ...data,
        [keyActive]: {
          ...data[keyActive],
          notes: value
        }
      },
    }, () => this.sendData())
  }

  onChange = (obj) => {    
    let { data, prevCustomer } = this.state;
    let dataCustomer = this.ohFormRef && this.ohFormRef.select && this.ohFormRef.select.name.record.code  ? this.ohFormRef.select.name.record.data : undefined;
    
    if(prevCustomer !== obj.customerId){
      obj.deliveryAddress = this.addressCustomer;
      this.setState({
        prevCustomer: obj.customerId
      })
    }

    if ( dataCustomer ) {
      
      obj["totalDeposit"] = dataCustomer.totalDeposit
      obj["mobile"] = dataCustomer.tel || "";
      obj["customerName"]= dataCustomer.name || "";
      obj["email"] = dataCustomer.email || "";
      obj["deliveryAddress"] = dataCustomer.address || "";
    }
    
    this.setState({
      data: {
        ...data,
        [this.props.keyActive]: {
          ...obj,
        }
      },
    }, () => this.sendData())
  }

  getTextFieldDiscount = (value, readOnly, name, isPercent) => {
    
    return (
      <TextField
        value={value ? ExtendFunction.FormatNumber(Math.round(value)) : 0}
        InputProps={{
          readOnly: readOnly,
          inputProps: {
            style: { textAlign: "right", width: 115, padding: 0 },
            onChange: (e) => {
              let value = e.target.value;
              if (value === ""){
                value = 0;
              }
              else {
                if (isNaN(ExtendFunction.UndoFormatNumber(value)) === false && Math.round(ExtendFunction.UndoFormatNumber(value)) >= 0 ) {
                  value = Math.round(ExtendFunction.UndoFormatNumber(value));
                }
              }
              
              this.setState({ 
                [name]: value, 
                [isPercent]: false
              }, () => this.getAmounts(undefined, isPercent === "isPercentDiscount" ))
            },
            onKeyDown: async (e) => {
              if (e.keyCode === 13) {
                if (name === "discount"){
                  this.handleVisibleChange(true)
                } else {
                  this.visiblePopoverTaxChange(true);
                }
              }
            }
          },
        }}
      />
    )
  }

  handleVisibleChange = visible => {
    this.setState({ selectDiscount : visible },()=>{
      if (this.state.selectDiscount){
        if (this.discountRef && this.discountRef.ohnumberinputRef && this.discountRef.ohnumberinputRef.numberInputRef){
          this.discountRef.ohnumberinputRef.numberInputRef.focus();
        }
        if (this.discountRef_1 && this.discountRef_1.ohnumberinputRef && this.discountRef_1.ohnumberinputRef.numberInputRef){
          this.discountRef_1.ohnumberinputRef.numberInputRef.focus();
        }
        this.props.visiblePopoverDiscountPrice(false);
      }
    });
  };

  visiblePopoverTaxChange = (e) => {
    this.setState({visiblePopTax: e},()=>{
      if (this.state.visiblePopTax){
        if (this.discountRef && this.discountRef.ohnumberinputRef && this.discountRef.ohnumberinputRef.numberInputRef){
          this.discountRef.ohnumberinputRef.numberInputRef.focus();
        }
        if (this.discountRef_1 && this.discountRef_1.ohnumberinputRef && this.discountRef_1.ohnumberinputRef.numberInputRef){
          this.discountRef_1.ohnumberinputRef.numberInputRef.focus();
        }
      }
    });
  }

  render() {
    let { t } = this.props;
    let { customers, data, visibleAddCustomer, users } = this.state;
    let dataCustomer = data[this.props.keyActive] || {};
    let checkEmployees = this.props.Employees_Options && parseInt(this.props.Employees_Options) === Constants.EMPLOYEES_OPTIONS.ON;
    let createdBy = dataCustomer.createdBy === undefined  ? this.props.currentUser.user.id : dataCustomer.createdBy;    
    let fullName = (checkEmployees ? "": (dataCustomer.createdBy !== undefined ? getUser(dataCustomer.createdBy, users) : getUser(this.props.currentUser.user.id, users)));
    
    dataCustomer.createdBy = createdBy;
    dataCustomer.fullName = fullName;

    let addCustomer = 
    <OhButton 
      type="exit" 
      onClick={() => {
        this.setState({ visibleAddCustomer: true })
      }} 
      className="button-add-information" 
      icon={<Icon type="plus" className="icon-add-information" />} 
    />
    const column1 = [
      checkEmployees ?
      {
        name: "createdBy",
        label: t("Người tạo"),
        ohtype: "select",
        options: users.map(item => ({value: item.id, title: item.fullName, data: item})),
        placeholder: t("Chọn một nhân viên"),
        rowClassName: "customer-sals-counter",
        labelClassName: "label-sales-counter",
        className: "product-form",
      }: {
        name: "fullName",
        label: t("Người tạo"),
        ohtype: "label",
        rowClassName: "customer-sals-counter",
        labelClassName: "label-sales-counter",
        className: "product-form",
      },
      {
        name: "customerId",
        label: t("Khách hàng") + " (F4)",
        ohtype: "select",
        onRef: ref => this.selectRef = ref,
        labelClassName: "label-sales-counter",
        options: customers.length ? customers.map((item) => ({ value: item.id, title: item.name, data: item, code: item.code, mobile: item.tel})) : [],
        autoFocus: true,
        button: addCustomer,
        rowClassName: "customer-sals-counter",
        className: "product-form",
        placeholder: t("Chọn một khách hàng"),
        onChange: (value, record) => {
          this.addressCustomer = record.data ? record.data.address : '';
        }
      }
    ];
    let totalProduct = dataCustomer && dataCustomer.quantityProducts ? dataCustomer.quantityProducts : 0;
    return (
      <Fragment>
        <ModalCreateCustomer
          type={"add"}
          visible={visibleAddCustomer}
          customerType = {Constants.CUSTOMER_TYPE.TYPE_CUSTOMER}
          title={t("Tạo khách hàng")}
          onChangeVisible={(visible, customerId) => {
            this.setState({
              visibleAddCustomer: visible
            });

            if(customerId){                            
            this.setState({
              data: {
                ...this.state.data,
                [this.props.keyActive]: {
                  ...this.state.data[this.props.keyActive],
                  customerId: customerId.id,
                  totalDeposit: customerId.totalDeposit,
                  deliveryAddress: customerId.address.trim(),
                  mobile: customerId.tel,
                  customerName: customerId.name,
                  email: customerId.email
                }
              },
              customers:[
                ...this.state.customers,
                customerId
              ]
            }, () =>  this.sendData())
            }
          }}
        />
        <OhForm
          title={""}
          defaultFormData={{...dataCustomer, mobile: dataCustomer.mobile || "", createdBy : createdBy, fullName: fullName }}
          onRef={ref => {
            this.ohFormRef = ref;

          }}
          columns={[column1]}
          labelRow={37}
          onChange={value => { this.onChange(value) }}
          checkValidationMessage={false}
          labelPadding={"label-padding-sale-counter"}
        />
        <GridContainer className={"payment-sale-counter"}>
          <GridItem xs={12} >
            <Container style ={{padding: "4px 2px 0px 0px"}}>
              <Row>
                <Col style={{ textAlign: "left", fontSize: "14px" }}>{t("Tổng tiền")+" "+ totalProduct +" "+ t("sp", {count: totalProduct })}</Col>
                <Col className ="totalCol-sale-counter">
                  {ExtendFunction.FormatNumber(Math.round(dataCustomer && dataCustomer.totalAmount ? dataCustomer.totalAmount : 0))}
                </Col>
              </Row>

              <Row className='row-product-sales'>

                <Col style={{ textAlign: "left", fontSize: "14px" }}>
                  {t("Chiết khấu") + " (F6)"}
                </Col>

                <Col className ="totalCol-sale-counter">
                  <Popover
                    trigger="click"
                    placement="left"
                    visible={this.state.selectDiscount}
                    onVisibleChange={this.handleVisibleChange}
                    getPopupContainer={trigger => trigger.parentNode}
                    content={ 
                      <DiscountForm
                        title={t('Chiết khấu thường')}
                        onChangeDiscount={(isPercent, discount) => {
                          this.setState({
                            discount: Number(discount),
                            isPercentDiscount: isPercent,
                          }, () => this.getAmounts())
                        }}
                        discountAmount={dataCustomer ? dataCustomer.discountAmount : 0}
                        totalAmount={dataCustomer ? dataCustomer.totalAmount : 0}
                        onRef ={ ref => this.discountRef = ref}
                        onChangeVisible={ (e) => this.handleVisibleChange(e) }

                      /> 
                    }
                  >
                    {this.getTextFieldDiscount(dataCustomer ? dataCustomer.discountAmount : 0, false, 'discount', 'isPercentDiscount')}
                  </Popover>
                </Col>
              </Row>

              <Row className='row-product-sales'>
                <Col style={{ textAlign: "left", fontSize: "14px" }}>
                  {t("Thuế")}
                </Col>
                <Col className ="totalCol-sale-counter">
                  <Popover
                    trigger="click"
                    placement="left"
                    visible={this.state.visiblePopTax}
                    onVisibleChange={ (e) => this.visiblePopoverTaxChange(e) }
                    getPopupContainer={trigger => trigger.parentNode}
                    content={
                      <DiscountForm
                        title={t('Thuế GTGT')}
                        onChangeDiscount={(isPercent, tax) => {
                          this.setState({
                            tax: dataCustomer ? tax : 0,
                            isPercentTax: dataCustomer ? isPercent : 0,
                          }, () => this.getAmounts())
                        }}
                        discountAmount={dataCustomer ? dataCustomer.taxAmount : 0}
                        totalAmount={dataCustomer ? (+dataCustomer.totalAmount - +dataCustomer.discountAmount ) : 0}

                        onRef ={ ref => this.discountRef_1 = ref}
                        onChangeVisible={ (e) => this.visiblePopoverTaxChange(e) }

                      />
                    }
                  >
                    {
                    this.getTextFieldDiscount(dataCustomer ? dataCustomer.taxAmount : 0, false, 'tax', 'isPercentTax')
                    }
                  </Popover>
                </Col>
              </Row>

              <Row className='row-product-sales'>
                <Col style={{ textAlign: "left", fontSize: "14px" }}>
                  {t("Phí giao hàng")}
                </Col>
                <Col className ="totalCol-sale-counter">
                  <TextField
                    value={dataCustomer && dataCustomer.deliveryAmount ? ExtendFunction.FormatNumber(parseInt(dataCustomer.deliveryAmount)) : 0}
                    InputProps={{
                      inputProps: {
                        style: { textAlign: "right", width: 115, padding: 0 },
                      },
                      onChange: (e) => {
                        let value = e.target.value.replace(/[^0-9]/g, "");
                        this.setState({
                          delivery: value,
                        }, () => this.getAmounts())
                      },
                      onClick: (e) => {
                        e.target.select()
                      },
                      readOnly: false
                    }}
                  />
                </Col>
              </Row>
              <Row className='row-product-sales'>
                <Col style={{ textAlign: "left", fontWeight: 600 }}>{t("Khách phải trả")}</Col>
                <Col style={{ maxWidth: 200, textAlign: "right", fontWeight: 700 }} className={'total-amount'}>
                  {dataCustomer && dataCustomer.finalAmount ? ExtendFunction.FormatNumber((dataCustomer.finalAmount < 0) ? 0 : Math.round(dataCustomer.finalAmount)) : 0}
                </Col>
              </Row>
              <Row className='row-product-sales'>
                <Col style={{ textAlign: "left" }}>{t("Khách thanh toán") + " (F7)"}</Col>
                <Col className ="totalCol-sale-counter">
                  <TextField
                    value={dataCustomer && dataCustomer.paidAmount ? ExtendFunction.FormatNumber(parseInt((dataCustomer.paidAmount < 0) ? 0 : dataCustomer.paidAmount)) : ''}
                    InputProps={{
                      inputProps: {
                        style: { textAlign: "right", width: 115, maxWidth: 160, padding: 0 },
                      },
                      onChange: (e) => {
                        let value = e.target.value.replace(/[^0-9]/g, "");
                        if (value === '' || Math.round(value) < 0){
                          value = '0';
                        }
                        this.setState({
                          payment: value,
                        }, () => this.getAmounts(value))
                      },
                      onClick: (e) => {
                        e.target.select()
                      },
                      readOnly: false
                    }}
                    inputRef= {ref => this.inputRef = ref}
                  />
                </Col>
              </Row>
              <Row className='row-product-sales'>
                <Col style={{ textAlign: "left" }}>{t("Tiền thừa trả khách")}</Col>
                <Col style={{ maxWidth: 200, textAlign: "right", fontWeight: 700 }} className={'totalCol-sale-counter'}>
                  {dataCustomer && dataCustomer.finalAmount ?  ExtendFunction.FormatNumber(Math.round(dataCustomer.paidAmount - dataCustomer.finalAmount )) : 0}
                </Col>
              </Row>
              <p className="notes-sale-counter">
                <TextField
                    id="standard-multiline-flexible"
                    placeholder={t("Ghi chú")}
                    multiline
                    rowsMax={ window.innerHeight > 770 ? 8 : 4}
                    value={dataCustomer && dataCustomer.notes ? dataCustomer.notes : ""}
                    onChange={(e) => this.onChangeNotes(e.target.value)}
                    style={{ display: 'flex', flexWrap: 'wrap', margin: "8px 0px" }}
                    inputProps={{
                      'aria-label': 'Description',
                      style: { color: "black" }

                  }}
                  />
                </p>
                <p className="call-sale-counter">{t("Hỗ trợ khách hàng 1900 88.86.98 - 08.6666.8168")}</p>
            </Container>
          </GridItem>
        </GridContainer>
      </Fragment>
    );
  }
}

export default (
  connect(function (state) {
    return {
      currentUser: state.userReducer.currentUser,
      customers: state.customerListReducer.customers,
      branchId: state.branchReducer.branchId,
      Employees_Options: state.reducer_user.Employees_Options,
      listUser: state.userListReducer.users
    };
  })
)(withTranslation("translations")

  (withStyles((theme) => ({
    ...extendedTablesStyle,
    ...buttonsStyle
  }))(InfoCustomer)));;
