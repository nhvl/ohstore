import React, { Fragment } from "react";
import { connect } from "react-redux";
import { withTranslation, } from 'react-i18next';
import withStyles from "@material-ui/core/styles/withStyles"
import CardBody from "components/Card/CardBody.jsx";
import extendedTablesStyle from "assets/jss/material-dashboard-pro-react/views/extendedTablesStyle.jsx";
import buttonsStyle from "assets/jss/material-dashboard-pro-react/views/buttonsStyle.jsx";
import Constants from 'variables/Constants/';
import { Tabs, Popover, Tooltip, Icon, Col} from 'antd';
import OhTable from "components/Oh/OhTable";
import { notifySuccess, notifyError } from 'components/Oh/OhUtils';
import AlertQuestion from 'components/Alert/AlertQuestion';
import ExtendFunction, { trans } from "lib/ExtendFunction";
import OhAutoComplete from "components/Oh/OhAutoComplete";
import productService from 'services/ProductService';
import DiscountPriceForm from 'views/Invoice/Create/DiscountUnitPriceForm';
import TextField from '@material-ui/core/TextField';
import OhSelectMaterial from 'components/Oh/OhSelectMaterial.jsx';
import OhNumberInput from "components/Oh/OhNumberInput.jsx";
import IconBarcode from "assets/img/vectorpaint.svg"
import IconBar from "assets/img/IconBarcode.svg"
import _ from "lodash";
import HttpService from "services/HttpService";
import OhButton from "components/Oh/OhButton";
import AddProduct from "views/Product/components/Product/Form";
import OhModal from "components/Oh/OhModal";


const { TabPane } = Tabs;
class TableSales extends React.Component {
  constructor(props) {
    super(props);
    if (this.props.onRef) this.props.onRef(this);
    this.dataSales = localStorage.getItem("sales-counter") || "";
    this.isJson = JSON.isJson(this.dataSales);
    this.dataCustomers = this.isJson ? JSON.parse(this.dataSales) : {};
    if (Object.keys(this.dataCustomers).length > 0) {
      let key = 1;

      this.dataCustomers.panes.forEach(item => {
        item.title = `Hóa đơn ${key}`
        item.key = key.toString();
        key += 1;
      })
      this.dataCustomers.TabIndex = this.dataCustomers.panes.length + 1;
    }
    this.newTabIndex = this.dataCustomers.TabIndex || 2;
    const panes = [
      {
        title: 'Hóa đơn 1',
        content: [],
        key: '1',
      },
    ];
    this.state = {
      ProductsForm: {},
      products: [],
      dataProducts: [],
      activeKey: this.dataCustomers.panes ? this.dataCustomers.panes[0].key : panes[0].key,
      panes: this.dataCustomers.panes || panes,
      alert: null,
      isVisible: false,
      visiblePrice: {},
      checkWidth: true,
      quamtityProduct: 0,
      product_copy: {},
      valueQuantity: 1,
      visibleAddService: false


    };
    this.count = 1;
    this.uniqueId = 0;
    this.discountRef_2 = {}

  }

  componentWillMount = () => {
    HttpService.setBranch(this.props.branchId, false, this.props.nameBranch);
  }

  componentDidMount = async() => {
    let onRef = this.autoRef.ref;
    this.setData();    

    document.addEventListener("keydown", (event) => {
      if (event.code === 'F3') {
        event.preventDefault();
        if (onRef) {
          onRef.focus();
          onRef.props.onSelect();
        }
        
      }
    }, false); 
  }

  setData = async() =>{
    let { panes } = this.state;
    let { stockList } = this.props;
    let stock_List = Object.keys(stockList);

    let objKeys = {};

    _.forEach(panes, item =>{
      let dataSource = item.content || [];

      if (dataSource.length){
        _.forEach(dataSource, ele =>{
          if (objKeys[ele.productId] !== ele.productId){
            objKeys[ele.productId] = ele.productId;
          }
        })
      }

    })

    let getProductList = await productService.getProductList({
      filter: { id:{ in: Object.values(objKeys) }, stoppedAt: 0, deletedAt: 0 }
    });

    if (getProductList.status) {
      let dataProduct = {};
      _.forEach(getProductList.data, item=>{
        dataProduct[item.id] = {...item}
        
      })
      let arrRemoveProduct = [];
      _.forEach(panes, ele =>{
        
        let dataSource = ele.content || [];        
  
        if (dataSource.length){
          
          _.forEach(dataSource, element =>{
            if ( element!== undefined) {

              let item = dataProduct[element.productId];
              element.total = item.saleUnitPrice   ;
              element.unitPrice = item.saleUnitPrice;
              element.sellPrice = element.sellPrice ? Number(element.sellPrice) : 0;
              element.discount = element.unitPrice - element.sellPrice;
              element.maxDiscount= item.maxDiscount
              element.costUnitPrice = item.costUnitPrice
              
              element.finalAmount = element.sellPrice * (Math.round(element.quantity * 100000) / 100000)
              let checkStock =  stockList[element.stockId].deletedAt !== 0;
              if (checkStock) {
                arrRemoveProduct.push({...element, keyPane: ele.key})
              } else {
                element.stockQuantity = stockList[element.stockId] && item[stockList[element.stockId].stockColumnName] ? item[stockList[element.stockId].stockColumnName] : 0;
              }

              stock_List.forEach(stock => {
                let check_stock =  stockList[stock] && stockList[stock].deletedAt === 0;

                if (check_stock){
                  element[stock] = stockList[stock] && item[stockList[stock].stockColumnName] ? item[stockList[stock].stockColumnName] : 0;
                } else {
                  delete  element[stock];
                }
              }) 

              element.key = this.count;
              element.index = this.count;
              this.count += 1;
              if(element.type === Constants.PRODUCT_TYPES.id.merchandise){
                element.totalQuantity = this.checkStockQuantity(element, Number(ele.key));

              }    
            }
          })
        }
      })

      if (arrRemoveProduct.length) this.removeProducts(arrRemoveProduct)
      this.setState({
        panes
      },()=>this.getTotalAmount(this.state.activeKey))
    }
  }

  onChangeTab = activeKey => {
    this.setState({ activeKey }, () => {
      this.setData();     
    });
  };

  onEdit = (targetKey, action) => {
    this[action](targetKey);
  };

  getTotalAmount = (key) => {

    let total = 0;
    let { panes } = this.state;
    let indexPane = panes.findIndex(item => item.key === key);
    let dataProduct = panes[indexPane].content;

    dataProduct.map(item => total += item.finalAmount);
    if (total < 0) total = 0;
    this.setState({
      totalAmount: total
    }, () => this.props.onChangProduct(dataProduct.length, this.state.totalAmount, key, panes, this.newTabIndex))
  }

  visiblePopoverDiscountPrice =(e, name) =>{
    let { visiblePrice } = this.state;
    for(let type in visiblePrice){
      visiblePrice[type] = false;
    }
    if (name) {
      visiblePrice[name] = e;
    }

    this.setState({visiblePrice},()=>{
      if (this.state.visiblePrice[name] &&  this.discountRef_2[name] && this.discountRef_2[name].ohnumberinputRef && this.discountRef_2[name].ohnumberinputRef.numberInputRef){
        this.discountRef_2[name].ohnumberinputRef.numberInputRef.focus();
        this.props.visiblePopoverDiscountTax(false);
      }
    });
  }

  checkMaxDiscount = (sellPrice, maxDiscount, unitPrice) => {
    const {t} = this.props;
    let discount = (unitPrice - sellPrice)*100/unitPrice;
    if(discount > maxDiscount){
      notifyError(t("Chiết khấu tối đa của sản phẩm này là {{maxDiscount}}%", {maxDiscount: t(maxDiscount)} ));
      return false;
    }
    return true;
  }

  checkStockQuantity = (record, key) =>{
    if (Number(this.state.activeKey) === Number(key)){
      let { panes } = this.state;
      let indexPane = panes.findIndex(item => item.key === this.state.activeKey);

      if (indexPane > -1) {

        let products = panes[indexPane].content || [];

        let changedProductQuantity = {};
        products.forEach(item => {
          if(item.type === Constants.PRODUCT_TYPES.id.merchandise){
            let {productId, quantity, stockId} = item;
            changedProductQuantity[stockId] = changedProductQuantity[stockId] || {};
            changedProductQuantity[stockId][productId] = (changedProductQuantity[stockId][productId] || 0) + +quantity;
          }
        })  
        return changedProductQuantity[record.stockId][record.productId];
      } return 0;
    } return 0;
  }

  setTotalQuantity = (record, data, indexPane) =>{
    if (indexPane > -1) {
      let products = data[indexPane].content || [];
      let dataProduct = products.filter(item => item.productId === record.productId && item.stockId === record.stockId) || [];
      let total = dataProduct.reduce((total, item) => total += Math.round(item.quantity * 100000) / 100000, 0);
      
      dataProduct.forEach(item => {
        if(item.type === Constants.PRODUCT_TYPES.id.merchandise){
          item.totalQuantity = total ;       
        }
      })
  
      return data;

    } return 0;
  }


  getTableSales = (key) => {
    let { panes } = this.state;
    let { t, readOnly, stockList } = this.props; 
    let listStock = ExtendFunction.getSelectStockList(stockList, []);


    let indexPane = panes.findIndex(item => item.key === key);

    let dataSource = [];

    if (indexPane !== -1) {
      dataSource = panes[indexPane].content
    }
        
    let columns = [
      {
        title: t("Mã"),
        dataIndex: "productCode",
        key: "productCode",
        width: "14%",
        align: "left",
        sorter: false
      },
      {
        title: t("Tên"),
        dataIndex: "productName",
        key: "productName",
        align: "left",
        sorter: false,
        render: value => trans(value)
      },
      {
        title: t("Kho"),
        dataIndex: "store",
        key: "store",
        width: "14%",
        sorter: false,
        align: "left",
        render: (value, record, index) => {  
          if (record.type === Constants.PRODUCT_TYPES.id.merchandise){
            let data = panes[indexPane].content[index]; 
            let dataProduct_new = [];
            if (data && data.stockDelete && data.stockDelete === true ) {
              return (<Tooltip 
                placement="leftTop" 
                title={ stockList[data.stockId].name || ""} 
                mouseEnterDelay={0.5}
                ><span className="ellipsis-not-span">{stockList[data.stockId].name}</span></Tooltip>) 
            } else {            
            return (
              <OhSelectMaterial 
                options = {listStock}
                onChange = {(value) => {
                  data.stockQuantity = record[value];
                  data.stockId = Number(value);     
                  this.setState({
                    panes
                  },()=> {
                    this.getTotalAmount(key);

                    dataProduct_new = this.setTotalQuantity(data, panes, indexPane);

                    if (dataProduct_new.length){
                      let dataproduct = this.setTotalQuantity(record, dataProduct_new, indexPane)
                      this.setState({
                        panes: dataproduct
                      })
                    }

                  })
                }}
                value={data.stockId}
              />
            );
            }
          } 
        }
      },
      {
        title: t("Số lượng"),
        dataIndex: "quantity",
        key: "quantity",
        align: 'right',
        sorter: false,
        width: "12%",
        render: (value, record, index) => {
          let item = panes[indexPane].content[index];
          let sellPrice = Number(item.unitPrice - (item.discount ? item.discount : 0));
          let stockQuantity = Math.round(record.stockQuantity * 100000) / 100000; 
          let total = Math.round(record.totalQuantity * 100000) / 100000;         
          let datapProduct = [];
          return (
            <Tooltip placement="left"
                title={(total > stockQuantity && record.type === Constants.PRODUCT_TYPES.id.merchandise) ? t("Số lượng tồn kho ( hiện tại {{stockQuantity}}, đặt hàng {{totalQuantity}} ) không đủ để bán", { stockQuantity: stockQuantity, totalQuantity: total  }) : ((total < stockQuantity && record.type === Constants.PRODUCT_TYPES.id.merchandise) ? t("Số lượng tồn kho ( hiện tại {{stockQuantity}}, đặt hàng {{totalQuantity}} )", { stockQuantity: stockQuantity, totalQuantity: total  }) : "" )}            >
              <TextField
                style={{ marginTop: record.discount ? -14 : null }}
                type="number"
                value={((value && value > 0) ? (Math.round(value * 100000) / 100000 ) : 0) || 0}
                InputProps={{
                  readOnly: false,
                  inputProps: {
                    min: "0",
                    className: "number-width",
                    style: { textAlign: "right", cursor: "pointer", color: (total  > stockQuantity  &&  record.type === Constants.PRODUCT_TYPES.id.merchandise) ? "red" : "" }
                  },
                  onChange: e => {
                    let temp = e.target.value;                    
                    let val = Math.round(temp * 100000) / 100000;

                    if (isNaN(ExtendFunction.UndoFormatNumber(val)) === false) {
                      let value = ExtendFunction.UndoFormatNumber(val);

                      item.quantity = value;                      
                      item.finalAmount = ((value * sellPrice < 0) ? 0 : (value * sellPrice));

                      if(item.type === Constants.PRODUCT_TYPES.id.merchandise){
                      datapProduct = this.setTotalQuantity(item, panes, indexPane);

                      if (datapProduct.length){
                        this.setState({
                          panes: datapProduct
                        }, () => this.getTotalAmount(key))
                      }
                      } else {
                        this.setState({
                          panes
                        }, () => this.getTotalAmount(key))
                      }
                      
                    }
                    if (val === "") {
                      item.quantity = '';
                      item.finalAmount = 0;
                    }
                  },
                  onKeyDown: async (e) => {
                    if (e.keyCode === 13 && (e.target.value === "" || e.target.value <= "0" || e.target.value <= 0)) {
                      this.removePane(record);
                    }
                  },
                  onBlur: async (e) => {
                    if (e.target.value === "" || e.target.value <= "0" || e.target.value <= 0) {
                      this.removePane(record);
                    }
                  },
                }}
              />
            </Tooltip>
          );
        },
      },
      {
        title: t("Giá bán"),
        dataIndex: "unitPrice",
        key: "unitPrice",
        width: "13%",
        sorter: false,
        align: "right",
        render: (value, record, index) => {
          let product = panes[indexPane].content[index];
          let oldFinalAmount = product.quantity * product.unitPrice;               
                    
          return (
            <div className="popover-price-sale-counter">
            <Popover
              trigger="click"
              placement= {"left"}
              getPopupContainer={trigger => trigger.parentNode}
              visible={this.state.visiblePrice[`visible_${product.key}`]}
              onVisibleChange={ (e) => this.visiblePopoverDiscountPrice(e, `visible_${product.key}`) }
              key={`key_${product.key}`}
              fitToScreen={true}
              content={
                <DiscountPriceForm
                  onChange={(formData) => {
                    let item = panes[indexPane].content[index];
                    item.unitPrice = formData.unitPrice;
                    item.discount = formData.unitPrice - formData.sellPrice;
                    item.finalAmount = formData.sellPrice * item.quantity;
                    item.sellPrice = formData.sellPrice;
                    
                    this.setState({
                      panes
                    }
                      , () => this.getTotalAmount(key)
                    )
                  }}
                  defaultFormData={{
                    unitPrice: product.unitPrice,
                    discount: record.discount,
                    discountType: record.discountType,
                    finalAmount: product.unitPrice - (product.discount ? product.discount : 0),
                    maxDiscount: product.maxDiscount
                  }}
                  isInvoice
                  onRef ={ ref => this.discountRef_2[`visible_${product.key}`] = ref}
                  onChangeVisible={ (e) => this.visiblePopoverDiscountPrice(e, `visible_${product.key}`) }
                  unitPriceName={t("Đơn giá")}
                  sellPriceName={t("Giá bán")}
                />
              }
            >
              <span>
                <TextField
                  value={ExtendFunction.FormatNumber(Math.round(product.sellPrice || (product.unitPrice - product.discount))) || 0}
                  InputProps={{
                    inputProps: {
                      style: { textAlign: "right", cursor: "pointer" }
                    },
                      style: { textAlign: "right", cursor: "pointer" },
                      onChange: (e) => {
                        let value = e.target.value;
                        if (value === ""){
                          product.sellPrice = '';
                          product.discount = product.unitPrice;
                          product.finalAmount = 0;
                        }
                        else {
                          if (isNaN(ExtendFunction.UndoFormatNumber(value)) === false && parseInt(ExtendFunction.UndoFormatNumber(value)) >= 0 ) {
                            product.sellPrice = Math.round((ExtendFunction.UndoFormatNumber(value)));
                            product.discount = product.unitPrice - product.sellPrice;
                            product.finalAmount = Math.round(product.sellPrice * product.quantity);
                            
                          }
                        }
                        this.setState({
                          panes
                        }
                          , () => this.getTotalAmount(key)
                        )
                      },
                      onBlur: (e) => {
                        if(!this.checkMaxDiscount(parseInt(ExtendFunction.UndoFormatNumber(e.target.value)), product.maxDiscount, product.unitPrice)){
                          product.sellPrice = product.unitPrice;
                          product.discount = 0;
                          product.finalAmount = Math.round(product.sellPrice * product.quantity);
                          this.setState({
                            panes
                          }
                            , () => this.getTotalAmount(key)
                          )
                        }
                      },
                      onKeyDown: async (e) => {
                        if (e.keyCode === 13) {
                          this.visiblePopoverDiscountPrice(true, `visible_${product.key}`);
                        }
                      },
                    }
                  }
                />
              </span><br />
              {product.finalAmount !== oldFinalAmount && (
                <span className={'line-through'}>
                  {ExtendFunction.FormatNumber(Math.round(product.unitPrice)) || 0}
                </span>
              )}
            </Popover>
            </div>
          )
        },
      },
      {
        title: t("Thành tiền"),
        dataIndex: "finalAmount",
        key: "finalAmount",
        width: "15%",
        sorter: false,
        align: 'right',
        render: value => {
          return (
            <span className="finalAmount-sale-counter"> 
              {ExtendFunction.FormatNumber(Math.round(value && value > 0 ? value : 0 || 0))}
            </span>
            
          )
        },
      },
    ];

    if(listStock.length <= 1){
      columns.splice(2,1);
    }

    return (
      <div className = {"table-sale-counter"} >
        <OhTable
          id={`sales-counter-${key}`}
          y={"calc(40vh)"}
          columns={columns}
          dataSource={dataSource}
          emptyDescription={Constants.NO_PRODUCT}
          isNonePagination={true}
          rowClassName={(record, index) => {
            if(index % 2 === 1){
              return 'colorRowOhTable';
            } 
          }}
          hasRemoveColumn={(value, record)=>{
            return !(
                ( readOnly )
              )
          }}
          onClickRemove={(value, record) => {
            this.removePane(record)
          }}
        />
      </div>
    )
  }

  removePane = record => {

    let { panes, activeKey } = this.state;
    let newPanes = panes.slice();

    let index = newPanes.findIndex(item => item.key === activeKey);

    if (index > -1) {
      let indexProduct = newPanes[index].content.findIndex(item => item.index === record.index)

      newPanes[index].content.splice(indexProduct, 1);

      this.setState({
        panes: newPanes
      }, () => this.getTotalAmount(activeKey))
    }

  };

  removeProducts = arr => {

    let { panes, activeKey } = this.state;
    let newPanes = panes.slice();

    _.map(arr, item => {
      let index = newPanes.findIndex(elem => elem.key === item.keyPane);

      if (index > -1) {
        let indexProduct = newPanes[index].content.findIndex(ele => ele.index === item.index);
        newPanes[index].content.splice(indexProduct, 1);
      }

      return;
    })    

    this.setState({
      panes: newPanes
    }, () => this.getTotalAmount(activeKey))
  };

  add = () => {
    let { panes } = this.state;
    let { t } = this.props;
    let activeKey = `${this.newTabIndex++}`;

    if (panes.length === 10) {
      notifyError(t("Chỉ tối đa 10 hóa đơn"));
      this.newTabIndex--;
      return;
    } else {
      panes.push({ title: `Hóa đơn ${activeKey}`, content: [], key: activeKey });
      this.setState({ panes, activeKey }, () => this.getTotalAmount(activeKey));
    }
  };

  hideAlert = () => {
    this.setState({ alert: null })
  }

  remove = (targetKey) => {
    let { t } = this.props;
    let { panes } = this.state;
    let flag = false;

    let index = panes.findIndex(item => item.key === targetKey);

    if (panes[index] && panes[index].content.length === 0) flag = true;

    if (flag) {
      this.deleteSumit(targetKey);

    } else {
      this.setState({
        alert: (
          <AlertQuestion
            messege={`Bạn chắc chắn muốn xóa hóa đơn`}
            name={`${targetKey}`}
            hideAlert={this.hideAlert}
            action={(e) => {
              this.hideAlert()
              this.deleteSumit(targetKey);
            }}
            buttonOk={t("Đồng ý")}
          />
        )
      })
    }
  }

  deleteSumit = (targetKey, keyDeleteInvoice ) => {
    let { activeKey } = this.state;
    let { t } = this.props;
    let lastIndex;

    this.state.panes.forEach((pane, i) => {
      if (pane.key === targetKey) {
        lastIndex = i - 1;
      }
    });

    const panes = this.state.panes.filter(pane => pane.key !== targetKey);

    if (panes.length && activeKey === targetKey) {
      if (lastIndex >= 0) {
        activeKey = panes[lastIndex].key;
      } else {
        activeKey = panes[0].key;
      }
    }

    this.setState({ panes, activeKey }, () => {
      if (this.state.panes.length === 0) {
        this.add();
      } else {
        this.getTotalAmount(activeKey);
      }

      this.props.deleteInvoice(targetKey)
      this.setData();
      notifySuccess( keyDeleteInvoice ? null : t("Xóa thành công hóa đơn {{targetKey}}", { targetKey: targetKey }))
    });
  };

  onKeyInput = async (event) => { 
    const { t } = this.props
    if (event.target.value && event.keyCode === 13) {
      if(this.state.dataProducts.length === 0) {
      let getProductList = await productService.getProductList({
        filter: { stoppedAt: 0, or: [{ code: event.target.value }, { barCode: event.target.value }] }      
      })

      if (getProductList.status) {
        this.setState({
          dataProducts: getProductList.data
        }, () => {
          if (getProductList.data.length === 1) {
            this.autoRef.ref.props.onSelect(getProductList.data[0].id, {props: {label: trans(getProductList.data[0].name, true)}} ) 
          } 
          else if (getProductList.data.length > 1) {
            this.autoRef.ref.props.onSelect()
            this.chooseProduct(getProductList.data)
          }
          else {
            notifyError(t("Sản phẩm không tồn tại hoặc đã ngừng kinh doanh"));
            this.autoRef.ref.props.onSelect()
          }
        }) 
      }
       else notifyError(getProductList.error)
    }     
    }
  }


  onSearchProduct = async value => {
    let getProductList = await productService.getProductList({
      filter: { or: [{ name: { contains: value } }, { code: { contains: value } }], stoppedAt: 0, deletedAt: 0 },
      limit: value === "" ? 0 : Constants.LIMIT_AUTOCOMPLETE_SEARCH
    });

    if (getProductList.status) {
      if (getProductList.data.length > 0)
        this.setState({ 
          dataProducts: getProductList.data,
          valueQuantity: 1
        });
      else
        this.setState({ dataProducts: [] });
    }
  }

  chooseProduct(products) {
    let { panes, activeKey, checkWidth } = this.state;
    let panes_copy = _.cloneDeep(panes);
    let { stockList } = this.props;
    let onRef = this.autoRef.ref;
    let dataProduct_new = [];
    let stockIdFirst;
    let stock_Lists = Object.keys(stockList);
    let stock_List = [];

    if (stock_Lists.length){
      stock_Lists.forEach(stock => {
        let check_Stock =  stockList[stock] && stockList[stock].deletedAt === 0;
        if (check_Stock){
          stock_List.push(stock) ;
        }
      })
  
      stockIdFirst = stock_List[0];
    }
    let newProduct = {};

    products.forEach(item => {
      
      this.uniqueId += 1;

      let newProduct = {
        id: 'new_' + this.uniqueId,
        productId: item.id,
        key: this.count,
        index: this.count,
        productCode: item.code,
        productName: item.name,
        unit: item.unitId.name,
        stockQuantity: item[stockList[stockIdFirst].stockColumnName] || 0,
        stockId: Number(stockIdFirst),
        quantity: 1,
        unitPrice: item.lastImportPrice,
        finalAmount: item.lastImportPrice,
        sellPrice: item.lastImportPrice,
        discount: 0,
        maxDiscount: item.maxDiscount,
        costUnitPrice: item.costUnitPrice,
        total: item.lastImportPrice,

        }
        stock_List.forEach(stock => {
            newProduct[stock] = item[stockList[stock].stockColumnName] || 0 ;
        }) 
      })

      let index = panes_copy.findIndex(item => item.key === activeKey);
      if (index !== -1) {

        panes_copy[index].content = [
          ...panes_copy[index].content,
          newProduct
        ]
        this.count += 1;

        if (checkWidth) {
          this.setState({
            panes: panes_copy,
            dataProducts:[]
          }, () => { 
            this.getTotalAmount(activeKey)
            if (onRef){
              onRef.focus();
              onRef.props.onSelect();
            }
            dataProduct_new = this.setTotalQuantity(newProduct, this.state.panes, index);
            if (dataProduct_new.length){
              this.setState({
                panes: dataProduct_new
              })
            }
          
          })        
        } 
        else {
          this.setState({
              product_copy: newProduct,
              dataProducts:[]
          },()=>{
            if (this.ohnumberinputValueRef && this.ohnumberinputValueRef.numberInputRef){
              this.ohnumberinputValueRef.numberInputRef.focus()
            }
          })
        }
      } 
  }

  onClickProduct = (id, record) => {
    let {stockList} = this.props;
    let stockIdFirst;
    let dataProduct_new = [];
    let stock_Lists = Object.keys(stockList);
    let stock_List = [];
    let onRef = this.autoRef.ref;

    if (stock_Lists.length){
      stock_Lists.forEach(stock => {
        let check_Stock =  stockList[stock] && stockList[stock].deletedAt === 0;
        if (check_Stock){
          stock_List.push(stock) ;
        }
      })
  
      stockIdFirst = stock_List[0];
    }

    id = Number(id);
    let { dataProducts, panes, activeKey, checkWidth } = this.state;
    let panes_copy = _.cloneDeep(panes);

    let productFound = record ? record : dataProducts.find(item => item.id === id);
    
    
      if (productFound) {
        let product = {
          productId: id ? id : productFound.key,
          key: this.count,
          index: this.count,
          productCode: productFound.code,
          productName: productFound.name,
          quantity: 1,
          discount: 0,
          stockQuantity: productFound[stockList[stockIdFirst].stockColumnName] || 0,
          stockId: Number(stockIdFirst),
          finalAmount: productFound.saleUnitPrice,
          unitPrice: productFound.saleUnitPrice,
          total: productFound.saleUnitPrice,
          maxDiscount: productFound.maxDiscount,
          sellPrice: productFound.saleUnitPrice,
          costUnitPrice: productFound.costUnitPrice,
          type: productFound.type,
          unit: productFound.unitId ? productFound.unitId.name : productFound.unit
        
        }
        stock_List.forEach(stock => {
            product[stock] = productFound[stockList[stock].stockColumnName] || 0 ;
        }) 
          let index = panes_copy.findIndex(item => item.key === activeKey);
          if (index !== -1) {

            panes_copy[index].content = [
              ...panes_copy[index].content,
              product
            ]
            
            this.count += 1;

            if (checkWidth || record !== undefined) {
              this.setState({
                panes: panes_copy,
                dataProducts:[]
              }, () => { 
                this.getTotalAmount(activeKey)
                if (onRef){
                  onRef.focus();
                  onRef.props.onSelect();
                }
               dataProduct_new = this.setTotalQuantity(product, this.state.panes, index);
                if (dataProduct_new.length){
                  this.setState({
                    panes: dataProduct_new
                  })
                }
              })        
            } 
           else {
              this.setState({
                  product_copy: product,
                  dataProducts:[]
              },()=>{
                if (this.ohnumberinputValueRef && this.ohnumberinputValueRef.numberInputRef){
                  this.ohnumberinputValueRef.numberInputRef.focus()
                }
              })
            }
      }
    }
  }
  onChangeAutoCompWidth = () =>{
    let { t } = this.props;
    let onRef = this.autoRef.ref;
    this.setState({
      checkWidth: !this.state.checkWidth,
      valueQuantity: 1
    },()=>{
      if (onRef){
        onRef.focus();
        onRef.props.onSelect();
      }
      if (this.state.checkWidth) {
        if (this.autoRef) this.autoRef.onResetValue();
        return notifySuccess(t("Chế độ nhập nhanh"));
      } else {
        if (this.autoRef) this.autoRef.onResetValue();
        return notifySuccess(t("Chế độ nhập thường"));
      }
    })
  }

  onEnter = () => {
    let { panes, activeKey, product_copy } = this.state;
    let { t } = this.props;
    let dataProduct_new = [];
    let checkDataProduct = product_copy.hasOwnProperty("productId");
    let product = _.cloneDeep(product_copy);

    if ( checkDataProduct) {
      let onRef = this.autoRef.ref;

      let index = panes.findIndex(item => item.key === activeKey);
  
      if (index !== -1) {
  
        panes[index].content = [
          ...panes[index].content,
          product_copy
        ]
        this.count += 1;
  
        this.setState({
          panes,
          valueQuantity: 1,
          dataProducts:[],
          product_copy: {}
        }, () => { 
          this.getTotalAmount(activeKey);        
          if (onRef){
            onRef.focus();
            onRef.props.onSelect();
          }
          if (this.ohnumberinputValueRef){
            this.ohnumberinputValueRef.onResetValue();
          }
          dataProduct_new = this.setTotalQuantity(product, this.state.panes, index);
            if (dataProduct_new.length){
              this.setState({
                panes: dataProduct_new
              })
            }
          
        })        
      }
    } else {
      notifyError(t("Không tìm thấy sản phẩm"))
    }
  }
  

  render() {
    let { t } = this.props;
    let { checkWidth, visibleAddService } = this.state;
    
    return (
      <Fragment>
        {this.state.alert}
        <CardBody className={"table-sales"} >
        {visibleAddService ? 
          <OhModal
            width={window.innerWidth > 1370 ? 1300 : 1100}
            title={t("Thêm sản phẩm")}
            content={<AddProduct
              onQuickAdd = {(dataSave) => {
                this.setState({visibleAddService: false})
                if(dataSave){
                  this.setState({
                    dataProducts: [
                      ...this.state.dataProducts,
                      dataSave
                    ]
                  }, () => {

                    if (this.autoRef && this.autoRef.ref && this.autoRef.ref.props){
                      this.autoRef.onSearchData(trans(dataSave.name, true));
                      this.autoRef.ref.props.onChange(trans(dataSave.name, true));
                    }
                    
                    this.onClickProduct(dataSave.id)
                  })
                }
              }}
            />}
            onOpen={visibleAddService}
            onClose={() => this.setState({visibleAddService: false})}
            footer={null}
            style={{top: window.innerHeight >= 800 ? "" : 10}}
          />
          : null }
          <Tabs
            tabPosition={"top"}
            defaultActiveKey={"1"}
            onChange={this.onChangeTab}
            className={""}
            activeKey={this.state.activeKey}
            type="editable-card"
            onEdit={this.onEdit}

          >
            {this.state.panes.map(pane => {
              let title = pane.title.split(" ");

              return (
                <TabPane tab={t(title[0] + ` ${title[1]}`) + ` ${title[2]}`} key={pane.key} closable={pane.closable}>
                  {this.getTableSales(pane.key)}
                </TabPane>
              )
            }
            )}
          </Tabs>
          <span className="ohsearch-table-sales">
            <OhAutoComplete
              autoFocus ={true}
              dataSelects={this.state.dataProducts}
              onSearchData={value => this.onSearchProduct(value)}
              onClickValue={id => this.onClickProduct(id)}
              placeholder={t("Tìm sản phẩm") + " (F3)"}
              onClick={() => this.setState({ isVisible: true })}
              onRef={ref => this.autoRef = ref}
              onKeyPress={e => this.onKeyInput(e)}
              widthInput={checkWidth ? "calc(100% - 16%)" : "calc(100% - 35%)"}
              isViewValue={!checkWidth}
            />
            <span className= {checkWidth ? "button-add-information-product": "button-add-information-product-1"} >
              <Tooltip placement="topLeft" title={t('Thêm sản phẩm')}>
                <Col className={"oh-button-col"}>
                  <OhButton 
                    type="exit" 
                    onClick={() => {
                      this.setState({ visibleAddService: true })
                    }} 
                    className="button-add-information" 
                    icon={<Icon type="plus" className="icon-add-information" />} 
                  />
                </Col>
              </Tooltip>
            </span>
            {
             !checkWidth ?  
            <OhNumberInput 
              placeholder={t("Số lượng")}
              defaultValue={this.state.valueQuantity}
              onChange={e => {
                let item = this.state.product_copy || {};
                
                item.quantity = e;
                item.finalAmount = e * item.sellPrice;

                if (e === "") {
                  item.quantity = 0 ;
                  item.finalAmount = 0;
                }

                this.setState({
                  product_copy: item,
                  valueQuantity: e
                })
              }}
              onKeyDown={e=>{                
                if (e.keyCode === 13 && this.state.product_copy.quantity > 0 ) {
                  this.onEnter();
                  if(this.autoRef) this.autoRef.onResetValue()
                }
              }}
              
              className ="ohsearch-table-sales-input"
              isNegative = {false}
              isDecimal= {true}
              valueDecimal={10000000}
              onRef ={ ref => this.ohnumberinputValueRef = ref}
            /> : null}
            <span className ="ohsearch-table-sales-barcode" onClick={()=>this.onChangeAutoCompWidth()} title={t("Chế độ nhập")}>
             {checkWidth ?
             <img src={IconBarcode} style={{width:"32px"}} alt=""/>:
             <img src={IconBar} style={{width:"32px"}} alt=""/>}
            </span>
          </span>
        </CardBody>
      </Fragment>
    );
  }
}

export default (
  connect(function (state) {
    return {
      currentUser: state.userReducer.currentUser,
      stockList: state.stockListReducer.stockList,
      branchId: state.branchReducer.branchId,
      nameBranch: state.branchReducer.nameBranch,    
    };
  })
)(withTranslation("translations")

  (withStyles((theme) => ({
    ...extendedTablesStyle,
    ...buttonsStyle
  }))(TableSales)));;
