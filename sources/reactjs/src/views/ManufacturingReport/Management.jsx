import React, { Component, Fragment } from 'react';
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import regularFormsStyle from "assets/jss/material-dashboard-pro-react/views/regularFormsStyle";
import { withTranslation } from 'react-i18next';
import { connect } from "react-redux";
import Card from "components/Card/Card.jsx";
import { Typography } from "antd";
import GridContainer from "components/Grid/GridContainer.jsx";
import CardBody from "components/Card/CardBody.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import moment from "moment";
import FormLabel from "@material-ui/core/FormLabel";
import OhTable from 'components/Oh/OhTable';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import { MdViewList, MdVerticalAlignBottom } from "react-icons/md";
import OhToolbar from "components/Oh/OhToolbar";
import { notifyError } from 'components/Oh/OhUtils';
import ManufacturingReportService from 'services/ManufacturingReportService';
import { trans } from "lib/ExtendFunction";
import OhMultiChoice from "components/Oh/OhMultiChoice";
import OhDateTimePicker from 'components/Oh/OhDateTimePicker';
import { ExportCSV } from 'ExportExcel/ExportExcel';
import Constants from 'variables/Constants/';

const { Paragraph } = Typography;

class ManufacturingReport extends Component {
  constructor(props) {
    super(props);
    let { t } = props;
    this.state = {
      dateTime: {
        start: new Date(moment().startOf('month')).getTime(),
        end: new Date(moment().endOf('month')).getTime()
      },
      InputValue: [moment().startOf('month'), moment().endOf('month')],
      dataSourcePType: Constants.OPTIONS_MANUFACTURING_STOCK.map(item => ({ id: item.value, name: t(item.title) })) || [],
      dataSourceProduct: (this.props.productList || []).filter(item => item.type === Constants.PRODUCT_TYPES.id.merchandise).map(item => ({ ...item, name: trans(item.name, true) })),
      dataManufacturingReport: [],
      productType: {},
      selectTypes: [],
      selectProducts: [],
      loading: false,
      totalProduct: 0
    };
  }

  componentDidMount() {
    let start = moment().startOf('month')
    let end = moment().endOf('month')
    this.getData(start, end, 0);
  }

  async componentDidUpdate(prevProps, prevState) {
    if ((this.props.productList.length && this.props.productList.length !== prevProps.productList.length) || (this.props.productTypes.length && this.props.productTypes.length !== prevProps.productTypes.length)) {
      this.setState({
        dataSourceProduct: (this.props.productList || []).filter(item => item.type === Constants.PRODUCT_TYPES.id.merchandise).map(item => ({ ...item, name: trans(item.name, true) })),
      })
    }
  }

  getData = async (start, end) => {
    this.setState({loading: true})
    let query = {
      startDate: new Date(start).getTime(),
      endDate: new Date(end).getTime(),
      categories: this.state.selectTypes,
      selects: this.state.selectProducts,
    }

    let dataManufacturingReport = await ManufacturingReportService.getManufacturingReportData(query)

    if (dataManufacturingReport.status) {
      this.setState({
        dataManufacturingReport: dataManufacturingReport.data,
        totalProduct: dataManufacturingReport.totalProduct,
        loading: false
      })
    } else notifyError(dataManufacturingReport.message);
    
    this.setState({loading: false})
  }

  async export() {
    let { dateTime } = this.state;

    let query = {
      startDate: new Date(dateTime.start).getTime(),
      endDate: new Date(dateTime.end).getTime(),
      categories: this.state.selectTypes,
      selects: this.state.selectProducts,
    }

    let getData = await ManufacturingReportService.getManufacturingReportData(query)
    let dataManufacturingReport = []
    if (getData.status) {
      dataManufacturingReport = getData.data
    }

    let { t } = this.props
    let dataExcel = [[
      '#',
      t('Mã'),
      t('Sản phẩm'),
      t('ĐVT'),
      t('Nhập'),
      t('Sản xuất'),
      t('Xuất'),
      t('Tồn'),
    ]];
    for (let item in dataManufacturingReport) {
      ;
      dataExcel.push(
        [
          parseInt(item) + 1,
          dataManufacturingReport[item].code,
          trans(dataManufacturingReport[item].name, true),
          dataManufacturingReport[item].unit,
          dataManufacturingReport[item].quantityImport !== undefined ? Number(dataManufacturingReport[item].quantityImport) : 0,
          dataManufacturingReport[item].quantityProduced !== undefined ? Number(dataManufacturingReport[item].quantityProduced) : 0,
          dataManufacturingReport[item].quantityExport !== undefined ? Number(dataManufacturingReport[item].quantityExport) : 0,
          dataManufacturingReport[item].quantityManufacturing !== undefined ? Number(dataManufacturingReport[item].quantityManufacturing) : 0,
        ]
      )
    }

    ExportCSV(dataExcel, t("BaoCaoSanXuat"));
  }

  render() {
    let { t } = this.props;
    let { InputValue, dateTime, dataManufacturingReport, loading } = this.state;

    let columns = [
      {
        title: t('Mã'),
        align: 'left',
        dataIndex: 'code',
        width: "10%",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.code ? a.code.localeCompare(b.code) : -1),
        key: "code",
        render: value => {
          return <div title={value}>{value}</div>;
        },
      },
      {
        title: t("Sản phẩm"),
        align: 'left',
        dataIndex: 'name',
        width: "15%",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.name ? a.name.localeCompare(b.name) : -1),
        render: value => {
          return (
            <Paragraph title={trans(value, true)} style={{ wordWrap: "break-word", wordBreak: "break-word", maxWidth: 200 }} ellipsis={{ rows: 4 }}>
              {trans(value)}
            </Paragraph>
          );
        },
      },
      {
        title: t("ĐVT"),
        dataIndex: 'unit',
        key: "unit",
        width: "5%",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.unit ? a.unit.localeCompare(b.unit) : -1),
      },
      {
        title: t("Nhập"),
        dataIndex: 'quantityImport',
        key: "quantityImport",
        align: 'right',
        width: "5%",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.quantityImport ? a.quantityImport.localeCompare(b.quantityImport) : -1),
        render: value => value || 0,
      },
      {
        title: t("Sản xuất"),
        dataIndex: 'quantityProduced',
        key: "quantityProduced",
        align: 'right',
        width: "5%",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.quantityProduced ? a.quantityProduced.localeCompare(b.quantityProduced) : -1),
        render: value => value || 0,
      },
      {
        title: t("Xuất"),
        dataIndex: 'quantityExport',
        key: "quantityExport",
        align: 'right',
        width: "5%",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.quantityExport ? a.quantityExport.localeCompare(b.quantityExport) : -1),
        render: value => value || 0,
      },
      {
        title: t("Tồn"),
        dataIndex: 'quantityManufacturing',
        key: "quantityManufacturing",
        align: 'right',
        width: "5%",
        sortDirections: ["descend", "ascend"],
        sorter: (a, b) => (a.quantityManufacturing ? a.quantityManufacturing.localeCompare(b.quantityManufacturing) : -1),
      },
    ];

    return (
      <div>

        <Fragment>
          <Card>
            <CardBody >
              <GridContainer style={{ padding: '0px 15px' }} alignItems="center">
                <OhToolbar
                  left={[
                    {
                      type: "button",
                      label: t("Xuất báo cáo"),
                      typeButton: "export",
                      onClick: () => this.export(),
                      icon: <MdVerticalAlignBottom />,
                    },
                  ]}
                />
                <GridItem>
                  <span className="TitleInfoForm">{t("Ngày")}</span>
                </GridItem>
                <GridItem >
                  <OhDateTimePicker defaultValue={InputValue} onChange={(start, end) => {
                    let dateTime = { start: start, end: end };
                    this.setState({ dateTime })
                  }}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer style={{ padding: '0px 15px' }} alignItems="center">
                <GridItem>
                  <span className="TitleInfoForm">{t("Theo")}</span>
                </GridItem>

                <GridItem  >
                  <OhMultiChoice
                    dataSourcePType={this.state.dataSourcePType}
                    placeholder={t("Chọn theo loại sản phẩm")}
                    onChange={(selectTypes) => {
                      this.setState({
                        selectTypes: selectTypes,
                      })
                    }}
                    defaultValue={this.state.selectTypes}
                    className='reportSelect'
                  />
                </GridItem>
                <GridItem>
                  <span className="TitleInfoForm">{t("Sản phẩm")}</span>
                </GridItem>
                <GridItem  >
                  <OhMultiChoice
                    dataSourcePType={this.state.dataSourceProduct}
                    placeholder={t("Chọn tên sản phẩm")}
                    onChange={(selectProducts) => {
                      this.setState({
                        selectProducts: selectProducts,
                      })
                    }}
                    defaultValue={this.state.selectProducts}
                    maxValue={10}
                    className='reportSelect'
                  />
                </GridItem>
                <OhToolbar
                  right={[
                    {
                      type: "button",
                      label: t("Xem báo cáo"),
                      onClick: () => this.getData(dateTime.start, dateTime.end, 0),
                      icon: <MdViewList />,
                      simple: true,
                      typeButton: "add",
                    },
                  ]}
                />
              </GridContainer>
              <GridContainer>
                <CardBody>

                  {this.state.totalProduct > 1000 ?
                    <GridItem >
                      <GridContainer >
                        <FormLabel className="ProductFormAddEdit" style={{ margin: 0, marginRight: 15 }}>
                          <b className='HeaderForm'>{t("Hiển thị 1–{{maxLength}} của {{total}} kết quả. Để thu hẹp kết quả, bạn hãy chỉnh sửa phạm vi thời gian hoặc chọn Xuất báo cáo để xem đầy đủ", { maxLength: 1000, total: this.state.totalProduct })}</b>
                        </FormLabel>
                      </GridContainer>
                    </GridItem>
                    :
                    null
                  }
                  <OhTable
                    id="store-import-export-report"
                    columns={columns}
                    dataSource={dataManufacturingReport.length > 1000 ? dataManufacturingReport.slice(0, 1000) : dataManufacturingReport}
                    isNonePagination={true}
                    loading={loading}
                  />
                </CardBody>
              </GridContainer>
            </CardBody>
          </Card>
        </Fragment>

      </div>
    );
  }
}

ManufacturingReport.propTypes = {
  classes: PropTypes.object.isRequired
};

export default (
  connect(function (state) {
    return {
      productList: state.productListReducer.products,
      productTypes: state.productTypeReducer.productTypes,
    };
  })
)(
  withTranslation("translations")(
    withStyles(theme => ({
      ...regularFormsStyle
    }))(ManufacturingReport)
  )
);
