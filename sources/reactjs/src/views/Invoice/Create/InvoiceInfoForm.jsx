import React from "react";
import { connect } from "react-redux";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import regularFormsStyle from "assets/jss/material-dashboard-pro-react/views/regularFormsStyle";

// multilingual
import { withTranslation } from "react-i18next";
import Constants from 'variables/Constants/';
import "date-fns";
import moment from "moment";
import _ from "lodash";
import OhForm from 'components/Oh/OhForm';
import { getUser } from "lib/ExtendFunction";

import {isMobile} from "react-device-detect";

class ProductForm extends React.Component {
  constructor(props) {
    super(props);    
    this.state = {
      invoiceInfo: {
        invoiceAt: moment().format(Constants.DATABASE_DATE_TIME_FORMAT_STRING),
      },
      users: this.props.listUser || []
    };
  }

  componentDidUpdate =(prevProps, prevState) => {
    let { dataEdit, dataUser } = this.props;    

    if (prevProps.dataEdit !== dataEdit && dataEdit){      
      this.setState({
        invoiceInfo: {
          code: dataEdit.code,
          notes: dataEdit.notes,
          invoiceAt: dataEdit.invoiceAt || moment().format(Constants.DATABASE_DATE_TIME_FORMAT_STRING),
          createdBy: dataEdit.createdBy,
          fullName: dataEdit.user_invoice ? dataEdit.user_invoice.fullName : this.props.currentUser.user.fullName
        }
      })
    }

    if (prevProps.dataUser !== dataUser && dataUser && !this.props.listUser.length){
      this.setState({
        users: dataUser
      })
    }
  }

  onChange = (value) => {

    if (value["code"]){      
      value["code"] = value["code"].trim();
    } 
    
    this.setState({
      invoiceInfo: value
    }, () => this.props.sendInvoiceInfo(this.state.invoiceInfo))
  }

  render() {
    let { t, dataEdit, isEdit, isCanceledCard, currentUser } = this.props;    
    let { invoiceInfo, users } = this.state;
    let checkEmployees = this.props.Employees_Options && parseInt(this.props.Employees_Options) === Constants.EMPLOYEES_OPTIONS.ON;
    let createdBy = isEdit ? (invoiceInfo || {}).createdBy : (checkEmployees && invoiceInfo.createdBy ? invoiceInfo.createdBy : currentUser.user.id );
    let fullName = checkEmployees ? "" : (isEdit ? (invoiceInfo || {}).fullName : getUser(currentUser.user.id, users))

    return (
      <GridItem float='right' xs={12} sm={12} md={6} lg={6}>
        <Card className = 'invoice-info-card' style={{height: "100%"}}>
          <CardBody xs={12} style={{ padding: 0 }}>
            <OhForm
              title={t("Thông tin hóa đơn")}
              defaultFormData = { _.extend(invoiceInfo, {createdBy: createdBy, fullName: fullName}) }
              onRef={ref => this.ohFormRef = ref}
              tag={isCanceledCard ? Constants.INVOICE_STATUS.name[dataEdit.status] : null}
              labelPadding={isMobile ? "mobile-oh-form-label" : ""}
              hasHelpText={false}
              columns={[
                [
                  {
                    name: "code",
                    label: t("Mã hóa đơn"),
                    ohtype: "input",
                    disabled: true,
                    placeholder: t(Constants.PLACEHOLDER_AUTO_GENERATE_CODE),
                  },
                  checkEmployees ?
                  {
                    name: "createdBy",
                    label: t("Người tạo"),
                    ohtype: "select",
                    options: users.map(item => ({value: item.id, title: item.fullName, data: item})),
                    placeholder: t("Chọn một nhân viên"),
                  }: {
                    name: "fullName",
                    label: t("Người tạo"),
                    ohtype: "label",
                  },
                  {
                    name: "invoiceAt",
                    label: t("Ngày bán"),
                    isDisabledDate:true,
                    showTime: true,
                    ohtype: "date-picker",
                    formatDateTime: Constants.DATABASE_DATE_TIME_FORMAT_STRING,
                  },
                  {
                    name: "notes",
                    label: t("Ghi chú"),
                    ohtype: isCanceledCard ? "label" : "textarea",
                    minRows: 2,
                    maxRows: 2
                  },
                ],
              ]}
              onChange={value => {                
                this.onChange(value);
              }}
            />
          </CardBody>
        </Card>
      </GridItem>
    );
  }
}

export default connect(function(state) {
  return ({
    currentUser: state.userReducer.currentUser,
    Employees_Options: state.reducer_user.Employees_Options,
    listUser: state.userListReducer.users
    
  });
})(
  withTranslation("translations")(
    withStyles(theme => ({
      ...regularFormsStyle
    }))(ProductForm)
  )
);
