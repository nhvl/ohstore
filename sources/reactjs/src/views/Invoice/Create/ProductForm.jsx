import React from "react";
import { connect } from "react-redux";
import FormLabel from "@material-ui/core/FormLabel";
import { Popover, Tooltip, List } from "antd";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import regularFormsStyle from "assets/jss/material-dashboard-pro-react/views/regularFormsStyle";
// multilingual
import { withTranslation } from "react-i18next";
import ExtendFunction from "lib/ExtendFunction";
import Constants from 'variables/Constants/';
import { Container, Col, Row } from "react-bootstrap";
import "date-fns";
import productService from 'services/ProductService';
import productUnitService from 'services/ProductUnitService';
import DiscountForm from './DiscountForm';
import DiscountUnitPriceForm from './DiscountUnitPriceForm';
import NotificationError from "components/Notification/NotificationError.jsx";
import OhTable from "components/Oh/OhTable";
import TextField from '@material-ui/core/TextField';
import OhAutoComplete from "components/Oh/OhAutoComplete";
import ModalClickGroup from "views/ProductType/components/ModalClickGroup";
import { notifyError } from 'components/Oh/OhUtils';
import { trans, NumberFormatTextField } from "lib/ExtendFunction";
import OhSelectMaterial from 'components/Oh/OhSelectMaterial';
import ManualSortFilter from "MyFunction/ManualSortFilter";
import ProductService from "services/ProductService";
import {BrowserView, MobileView, isBrowser, isMobile} from "react-device-detect";
import OhButton from "components/Oh/OhButton";
import { Empty, Icon } from "antd";
import OhModal from "components/Oh/OhModal";
import AddProduct from "views/Product/components/Product/Form";
import HttpService from "services/HttpService";

class ProductForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      ProductsForm: [],
      discountAmount: 0,
      finalAmount: 0,
      totalAmount: 0,
      taxAmount: 0,
      payType: 0,
      deliveryType: 0,
      deliveryAmount: 0,
      paidAmount: 0,
      debtAmount: 0,
      isVisible: false,
      isChange: false,
      visiblePopup:{},
      discount: 0,
      tax: 0,
      visibleAddService: false
    };

    this.uniqueId = 0;
    this.count = 1;
    this.productStocks = {};
    this.discountRef_2 = {}
  }

  componentWillMount = async () => {
    let productStock = await ProductService.getProductStockList();

    if (productStock.status) {
      productStock.data.forEach(item => this.productStocks[item.productId] = item);
    }
    HttpService.setBranch(this.props.branchId, false, this.props.nameBranch);
  }

  componentDidUpdate = async (prevProps, prevState) => {
    
    const { dataEdit, stockList } = this.props;
    let stock_List = Object.keys(stockList);

    if (prevProps.dataEdit !== dataEdit && dataEdit) {
      let productUnit = null;
      let products = dataEdit.products;
      let getProductUnits = this.props.productUnits && this.props.productUnits.length ? this.props.productUnits : await productUnitService.getProductUnits();

      if (!getProductUnits.length) {
        getProductUnits = getProductUnits.data
      }

      products.forEach(item => {
        let checkStock =  stockList[item.stockId] && stockList[item.stockId].deletedAt === 0;
        
        if (!checkStock) {
          item.stockQuantity = 0;
          item.stockDelete = true;
        } else {
          item.stockQuantity = item.productId[stockList[item.stockId].stockColumnName] || 0;
          item.stockDelete = false;
        }

        item.maxDiscount = item.productId.maxDiscount;
        item.type = item.productId.type;
        getProductUnits.forEach(unit => {
          if (item.productId.unitId === unit.id) {
            productUnit = unit;
            item.unit = productUnit.name;
            return;
          }
        })

        stock_List.forEach(stock => {
          let check_Stock =  stockList[stock] && stockList[stock].deletedAt === 0;
          if (check_Stock){
            item[stock] = item.productId[stockList[stock].stockColumnName] || 0 ;
          }
        }) 

        item.productId = item.productId.id;
        item.key = this.count;
        item.index = this.count;
        this.count += 1;

      })
      this.setState({
        ProductsForm: products,
        discountAmount: dataEdit.discountAmount,
        finalAmount: dataEdit.finalAmount,
        totalAmount: dataEdit.totalAmount,
        taxAmount: dataEdit.taxAmount,
        payType: dataEdit.payType,
        deliveryType: dataEdit.deliveryType,
        deliveryAmount: dataEdit.deliveryAmount,
        delivery: dataEdit.deliveryAmount,
        paidAmount: dataEdit.paidAmount,
        debtAmount: dataEdit.debtAmount,
        discount: dataEdit.discountAmount/dataEdit.totalAmount*100,
        isPercentDiscount: true,
        tax: dataEdit.taxAmount/(dataEdit.totalAmount - dataEdit.discountAmount)*100,
        isPercentTax: true
      }, () => this.sendData())
    }
  }

  onClickProduct = (id) => {
    let {stockList} = this.props;
    let { products, ProductsForm } = this.state;

    id = Number(id);
    let stockIdFirst;
    let stock_Lists = Object.keys(stockList);
    let stock_List = [];
    
    if (stock_Lists.length){
      stock_Lists.forEach(stock => {
        let check_Stock =  stockList[stock] && stockList[stock].deletedAt === 0;
        if (check_Stock){
          stock_List.push(stock) ;
        }
      })
  
      stockIdFirst = stock_List[0];
    }

    let productFound = products.find(item => item.id === id);    

    let product = {};
    if ( productFound ) {
      let stockQuantity = 0;

      if (this.props.productList && this.props.productList.length) {
        stockQuantity = (this.productStocks[id] && this.productStocks[id][stockList[stockIdFirst].stockColumnName]) || productFound[stockList[stockIdFirst].stockColumnName];
      }
      else stockQuantity = productFound[stockList[stockIdFirst].stockColumnName];

      this.uniqueId += 1;
      product = {
        id: 'new_' + this.uniqueId,
        key: this.count,
        index: this.count,
        productId: id,
        productCode: productFound.code,
        productName: productFound.name,
        unit: productFound.unitId.name,
        stockQuantity: stockQuantity || 0,
        stockId: Number(stockIdFirst),
        quantity: 1,
        unitPrice: productFound.saleUnitPrice,
        finalAmount: productFound.saleUnitPrice,
        sellPrice: productFound.saleUnitPrice,
        maxDiscount: productFound.maxDiscount,
        costUnitPrice: productFound.costUnitPrice,
        discount: 0,
        stockDelete: false,
        type: productFound.type
      }
      stock_List.forEach(stock => {
        if (this.props.productList && this.props.productList.length) {
          product[stock] = (this.productStocks[id] && this.productStocks[id][stockList[stock].stockColumnName]) || (productFound[stockList[stock].stockColumnName] || 0);
        }
        else product[stock] = productFound[stockList[stock].stockColumnName] || 0 ;
      }) 
      this.count += 1;
      this.setState({
        ProductsForm: [
          ...ProductsForm,
          product
        ],
        products: [],
        isChange: true,
      }, () => this.getTotalAmount())
    }
  }

  error = (mess) => {
    const { t } = this.props;
    this.setState({
      brerror: <NotificationError closeNoti={() => this.setState({ brerror: null })} message={t(mess)} />
    })
  }

  getTotalAmount = () => {
    let total = 0;
    const { ProductsForm } = this.state;
    ProductsForm.map(item => total += item.finalAmount);
    this.setState({
      totalAmount: total,
    }, () => this.getAmounts())
  }

  getAmounts = () => {
    let { totalAmount, discount, isPercentDiscount, tax, isPercentTax, delivery } = this.state;
    let discountTotal = Math.round(isPercentDiscount ? totalAmount * discount / 100 : discount);
    let taxTotal = Math.round(isPercentTax ? (totalAmount-discountTotal) * tax / 100 : tax);
    let finalTotal = totalAmount;
    if (discountTotal)
      finalTotal -= discountTotal;
    if (taxTotal)
      finalTotal += taxTotal;
    delivery = Number(delivery);
    if(delivery)
      finalTotal += delivery;
    this.setState({
      discountAmount: !isNaN(discountTotal) ? discountTotal : 0,
      taxAmount: !isNaN(taxTotal) ? taxTotal : 0,
      deliveryAmount: delivery || 0,
      finalAmount: finalTotal,
      paidAmount: finalTotal,
      debtAmount: 0
    }, () => this.sendData())
  }

  sendData = () => {
    let { ProductsForm, discountAmount, finalAmount, totalAmount, taxAmount, deliveryAmount, paidAmount, debtAmount, isChange } = this.state;
    this.props.sendProductsData(
      ProductsForm,
      discountAmount,
      Math.round(finalAmount),
      totalAmount,
      taxAmount,
      deliveryAmount,
      paidAmount,
      debtAmount,
      isChange,
    )
  }

  onKeyInput = async (event) => { 
    const { t } = this.props
    if (event.target.value && event.keyCode === 13) {
      if(this.state.products.length === 0) {
        let filter = { stoppedAt: 0, or: [{ code: event.target.value }, { barCode: event.target.value }] }
        
        if (this.props.productList && this.props.productList.length) {
          let dataFilter = ManualSortFilter.ManualSortFilter(this.props.productList, filter, {})
          this.setState({
            products: dataFilter
          }, () => {
            if (dataFilter.length === 1) {
              this.onRef.ref.props.onSelect(dataFilter[0].id)
            }
            else {
              this.onRef.ref.props.onSelect()
              this.chooseProduct(dataFilter)
            }
          })
        } else {
          let getProductList = await productService.getProductList({ filter })
          if (getProductList.status) {
            this.setState({
              products: getProductList.data
            }, () => {
              if (getProductList.data.length === 1) { 
                this.onRef.ref.props.onSelect(getProductList.data[0].id) 
              } 
              else if (getProductList.data.length > 1) {
                this.onRef.ref.props.onSelect()
                this.chooseProduct(getProductList.data)
              }
              else
                notifyError(t("Sản phẩm không tồn tại hoặc đã ngừng kinh doanh"));
                this.onRef.ref.props.onSelect()
            }) 
          }
          else notifyError(getProductList.error)
        }
      }
    }
  }

  onSearchProduct = async value => {
    this.time = new Date().getTime();
    let filter = { stoppedAt: 0, or: [{ name: { contains: value } }, { code: { contains: value } }] }
    if (this.props.productList && this.props.productList.length) {
      if (value === "") {
        this.setState({ products: [] })
      }
      else {
        let dataFilter = ManualSortFilter.ManualSortFilter(this.props.productList, filter, {});

        this.setState({ products: dataFilter })
      }
    }
    else {
      let getProductList = await productService.getProductList({
        filter: { stoppedAt: 0, or: [{ name: { contains: value } }, { code: { contains: value } }] },
        limit: value === "" ? 0 : Constants.LIMIT_AUTOCOMPLETE_SEARCH,
        time: this.time
      });

      if (getProductList.status) {
        if (getProductList.data.length > 0 && this.time === getProductList.time) {
          this.setState({ products: getProductList.data });
        }
        else this.setState({ products: [] });
      }
    }
  };

  removeProduct = (record) => {
    let products = this.state.ProductsForm;
    let index = products.findIndex(item => item.index === record.index);
    if ( index > -1 ) {
      products.splice(index, 1);
    }
    this.setState({ ProductsForm: products }, () => this.getTotalAmount())
  }

  getTextFieldDiscount = (value, readOnly, name, isPercent) => {
    return(
      <TextField
        value={value ? ExtendFunction.FormatNumber(Math.round(value)) : 0}
        InputProps={{
          readOnly: readOnly,
          inputProps: {
            style: { textAlign: "right", width: 80, padding: 0 },
            onChange: (e) => {
              let value = e.target.value;
              if (value === ""){
                value = 0;
              }
              else {
                if (isNaN(ExtendFunction.UndoFormatNumber(value)) === false && parseInt(ExtendFunction.UndoFormatNumber(value)) >= 0 ) {
                  value = parseInt(ExtendFunction.UndoFormatNumber(value));
                }
              }
              if(!readOnly)
                this.setState({ 
                  [name]: value, 
                  [isPercent]: false
                }, () => this.getAmounts())
            },
            onKeyDown: async (e) => {
              if (e.keyCode === 13) {
                this.visiblePopup(true, name);
              }
            },
          },
        }}
      />
    )
  }

  chooseProduct(products) {
    let { ProductsForm } = this.state;
    let { stockList } = this.props;
    let stockIdFirst;
    let stock_Lists = Object.keys(stockList);
    let stock_List = [];

    if (stock_Lists.length){
      stock_Lists.forEach(stock => {
        let check_Stock =  stockList[stock] && stockList[stock].deletedAt === 0;
        if (check_Stock){
          stock_List.push(stock) ;
        }
      })
  
      stockIdFirst = stock_List[0];
    }
    products.forEach(item => {
      this.uniqueId += 1;
      let stockQuantity = 0;
      if (this.props.productList && this.props.productList.length) {
        stockQuantity = this.productStocks[item.id][stockList[stockIdFirst].stockColumnName];
      }
      else stockQuantity = item[stockList[stockIdFirst].stockColumnName]
      let newProduct = {
        id: 'new_' + this.uniqueId,
        key: this.count,
        index: this.count,
        productId: item.id,
        productCode: item.code,
        productName: item.name,
        unit: item.unitId.name,
        stockQuantity: stockQuantity || 0,
        stockId: Number(stockIdFirst),
        quantity: 1,
        unitPrice: item.saleUnitPrice,
        finalAmount: item.saleUnitPrice,
        sellPrice: item.saleUnitPrice,
        discount: 0,
        maxDiscount: item.maxDiscount,
        costUnitPrice: item.costUnitPrice,
        stockDelete: false,
        type: item.type
      }
      stock_List.forEach(stock => {
        if (this.props.productList && this.props.productList.length) {
          newProduct[stock] = this.productStocks[item.id][stockList[stock].stockColumnName] || 0
        }
        else newProduct[stock] = item[stockList[stock].stockColumnName] || 0 ;
      }) 
      this.count += 1;
      ProductsForm.push(newProduct)
    })
    this.setState({
      ProductsForm: ProductsForm,
      products: [],
      isChange: true,
    }, () => this.getTotalAmount())
  }

  onClickGroupProduct = async productTypeId => {    
    const { t } = this.props
    try {
      if (this.props.productList && this.props.productList.length) {
        let dataFilter = ManualSortFilter.ManualSortFilter( this.props.productList, {"productTypeId.id": productTypeId, stoppedAt: 0 }, {})
        if (dataFilter.length)
          this.chooseProduct(dataFilter)
        else notifyError(t("Không có sản phẩm nào ở nhóm này"))
      }
      else {
        let products = await productService.getProductList({filter: { stoppedAt: 0, productTypeId } })

        if (products.status) {
          if ( products.data.length > 0 ) {
            this.chooseProduct(products.data)
          } else notifyError(t("Không có sản phẩm nào ở nhóm này"))      
        } else throw products.error
      }
    }
    catch(error){
      notifyError(t("Lấy sản phẩm theo nhóm sản phẩm bị lỗi"))
    }

  }
  
  visiblePopup = (e, name) => {
    let {visiblePopup} = this.state;
    for(let type in visiblePopup){
      visiblePopup[type] = false;
    }
    visiblePopup[name] = e;
    this.setState({visiblePopup},()=>{
      if (this.state.visiblePopup[name]){
        if (this.discountRef && this.discountRef.ohnumberinputRef && this.discountRef.ohnumberinputRef.numberInputRef){
          this.discountRef.ohnumberinputRef.numberInputRef.focus()
        }
        if (this.discountRef_1 && this.discountRef_1.ohnumberinputRef && this.discountRef_1.ohnumberinputRef.numberInputRef){
          this.discountRef_1.ohnumberinputRef.numberInputRef.focus();          
        }  
        if (this.discountRef_2[name] && this.discountRef_2[name].ohnumberinputRef && this.discountRef_2[name].ohnumberinputRef.numberInputRef){
          this.discountRef_2[name].ohnumberinputRef.numberInputRef.focus();
        }
      }
    });
  }  

  getStoreSelect = (record, index, options) => {
    let { t, stockList } = this.props;
    let { ProductsForm, isCanceledCard, isReturn } = this.state;
    if ((((ProductsForm[index] && ProductsForm[index].stockDelete) && ProductsForm[index].stockDelete === true) || isCanceledCard) || isReturn) {
      return (<Tooltip 
        placement="leftTop" 
        title={ (ProductsForm[index].stockId && stockList[ProductsForm[index].stockId]) ? stockList[ProductsForm[index].stockId].name : ""} 
        mouseEnterDelay={0.5}
        ><span className="ellipsis-not-span">{ (ProductsForm[index].stockId && stockList[ProductsForm[index].stockId]) ? stockList[ProductsForm[index].stockId].name : ""}</span></Tooltip>) 
    } else {      
    return (
      <OhSelectMaterial 
        options = {options}
        onChange = {(value) => {
          ProductsForm[index].stockQuantity = record[value];
          ProductsForm[index].stockId = Number(value);
          this.setState({
            ProductsForm: ProductsForm
          },() => this.getTotalAmount())
        }}
        value={ProductsForm[index].stockId}
        formater={value => t(value)}
        disabled={isCanceledCard || isReturn}

      />
     )
    }
  }

  checkMaxDiscount = (sellPrice, maxDiscount, unitPrice) => {
    const {t} = this.props;
    let discount = (unitPrice - sellPrice)*100/unitPrice;
    if(discount > maxDiscount){
      notifyError(t("Chiết khấu tối đa của sản phẩm này là {{maxDiscount}}%", {maxDiscount: t(maxDiscount)} ));
      return false;
    }
    return true;
  }

  render() {
    let { t, isReturn, isCanceledCard, stockList } = this.props;
    let { products, ProductsForm, discountAmount, finalAmount, totalAmount, taxAmount, deliveryAmount, visibleAddService } = this.state;
    let listStock = ExtendFunction.getSelectStockList(stockList, []);
    
    let columns = [
      {
        title: t("Mã"),
        dataIndex: "productCode",
        key: "productCode",
        width: "11%",
        align: "left",
      },
      {
        title: t("Tên sản phẩm"),
        dataIndex: "productName",
        key: "productName",
        width: "20%",
        align: "left",
        render: value => trans(value)
      },
      {
        title: t("Đơn vị"),
        dataIndex: "unit",
        key: "unit",
        width: "10%",
        align: "left",
      },
      {
        title: t("Kho"),
        dataIndex: "store",
        key: "store",
        width: "10%",
        align: "left",
        render: (value, record, index) => {
          if(record.type === Constants.PRODUCT_TYPES.id.merchandise)
            return this.getStoreSelect(record, index, listStock);
        }
      },
      {
        title: t("Tồn kho"),
        dataIndex: "stockQuantity",
        key: "stockQuantity",
        align: 'right',
        width: "10%",
        render: (value, record, index) => {
          if(record.type === Constants.PRODUCT_TYPES.id.merchandise)
            return ExtendFunction.FormatNumber(value) || 0;
        },
      },
      {
        title: t("Số lượng"),
        dataIndex: "quantity",
        key: "quantity",
        align: 'right',
        width: "10%",
        render: (value, record, index) => {          
          return (
            <TextField
              value={value}
              id={"formatted-numberformat-input" + record.index}
              style = {{marginTop: Math.round(record.discount) !== 0 && !isReturn && !isCanceledCard ? -14 : null}}
              InputProps={{
                inputComponent: NumberFormatTextField,
                readOnly: isReturn || isCanceledCard || (record.stockDelete && record.type === Constants.PRODUCT_TYPES.id.merchandise) ,
                inputProps: {
                  style: { textAlign: "right", width: 80}
                },
                onChange: e => {
                  let item = ProductsForm[index];
                  let sellPrice = Number(item.unitPrice - (item.discount ? item.discount : 0));
                  let val = e.target.value;
                  if (isNaN(ExtendFunction.UndoFormatNumber(val)) === false) {
                    let value = ExtendFunction.UndoFormatNumber(val);
                    
                    item.quantity = +value;
                    item.finalAmount = value * sellPrice;
                  }
                  if (val === "") {
                    item.quantity = '';
                    item.finalAmount = 0;
                  }

                  this.setState({ 
                    ProductsForm: ProductsForm,
                    isChange: true,
                   }, () => this.getTotalAmount())
                },
                onFocus: (e) => {
                  e.target.select()
                },
                onClick: (e) => {
                  e.target.select()
                },
                onKeyDown: async (e) => {
                  if (e.keyCode === 13 && (e.target.value === "" || e.target.value === "0")) {
                    this.removeProduct(record);
                  }
                },
                onBlur: async (e) => {
                  if (e.target.value === "" || e.target.value === "0") {
                    this.removeProduct(record);
                  }
                },
              }}
            />
          );
        },
      },
      {
        title: t("Giá bán"),
        dataIndex: "unitPrice",
        key: "unitPrice",
        width: "10%",
        align: "right",
        render: (value, record, index) => {
          let product = ProductsForm[index];
          
          return (
            (isReturn || isCanceledCard || (record.stockDelete && record.type === Constants.PRODUCT_TYPES.id.merchandise))
            ? <TextField
                value={value ? ExtendFunction.FormatNumber(Math.round(product.unitPrice - product.discount)) : 0}
                InputProps={{
                  readOnly: true,
                  inputProps: {
                    style: { textAlign: "right", width: 80 }
                  },
                }}
              />
            :
              <Popover
                trigger="click"
                placement={isMobile ? "bottom" : "left"}
                getPopupContainer={trigger => trigger.parentNode}
                visible={this.state.visiblePopup[`visible_${record.key}`]}
                onVisibleChange={ (e) => this.visiblePopup(e, `visible_${record.key}`) }
                content={
                  <DiscountUnitPriceForm
                    onChange={(formData) => {
                      let item = ProductsForm[index];

                      item.unitPrice = formData.unitPrice;
                      item.discount = formData.unitPrice - formData.sellPrice;
                      item.finalAmount = Math.round(formData.sellPrice * item.quantity);
                      item.sellPrice = formData.sellPrice;
                      this.setState({ 
                        ProductsForm: ProductsForm, 
                      }, () => this.getTotalAmount())
                    }}
                    defaultFormData={{
                      unitPrice: product.unitPrice,
                      discount: record.discount,
                      discountType: record.discountType,
                      finalAmount: product.unitPrice - (product.discount ? product.discount : 0),
                      maxDiscount: product.maxDiscount
                    }}
                    isInvoice
                    onRef ={ ref => this.discountRef_2[`visible_${record.key}`] = ref}
                    onChangeVisible={ (e) => this.visiblePopup(e, `visible_${record.key}`) }
                    unitPriceName={t("Đơn giá")}
                    sellPriceName={t("Giá bán")}
                  />
                }
              >
                <span>
                  <TextField
                    value={ExtendFunction.FormatNumber(Math.round( product.sellPrice || (product.unitPrice - product.discount))) || 0}
                    InputProps={{
                      inputProps: {
                        style: { textAlign: "right", width: 80},
                        onChange: (e) => {
                          let value = e.target.value;
                          if (value === ""){
                            product.sellPrice = '';
                            product.discount = product.unitPrice;
                            product.finalAmount = 0;
                          }
                          else {
                            if (isNaN(ExtendFunction.UndoFormatNumber(value)) === false && parseInt(ExtendFunction.UndoFormatNumber(value)) >= 0 ) {
                              product.sellPrice = parseInt(ExtendFunction.UndoFormatNumber(value));
                              product.discount = product.unitPrice - product.sellPrice;
                              product.finalAmount = Math.round(product.sellPrice * product.quantity);
                            }
                          }
                          this.setState({ 
                            ProductsForm: ProductsForm, 
                          }, () => this.getTotalAmount())
                        },
                        onBlur: (e) => {
                          if(!this.checkMaxDiscount(parseInt(ExtendFunction.UndoFormatNumber(e.target.value)), product.maxDiscount, product.unitPrice)){
                            product.sellPrice = product.unitPrice;
                            product.discount = 0;
                            product.finalAmount = Math.round(product.sellPrice * product.quantity);
                            this.setState({ 
                              ProductsForm: ProductsForm, 
                            }, () => this.getTotalAmount())
                          }
                        },
                        onKeyDown: async (e) => {
                          if (e.keyCode === 13) {
                            this.visiblePopup(true, `visible_${record.key}`);
                          }
                        },
                      }
                    }}
                  />
                </span><br />
                {Math.round(product.discount) ? (
                  <span className={'line-through'}>
                    {ExtendFunction.FormatNumber(Math.round(product.unitPrice)) || 0}
                  </span>
                ) : null}
              </Popover>
          )
        },
      },
      {
        title: t("Thành tiền"),
        dataIndex: "finalAmount",
        key: "finalAmount",
        align: 'right',
        render: value => {
          return ExtendFunction.FormatNumber(Math.round(value)) || 0;
        },
        width: "15%"
      },
    ];

    if(listStock.length <= 1){
      columns.splice(3,1);
    }

    return (
      <>
        {this.state.br}
        {this.state.brerror}
        <GridContainer style={{ width: "100%" }}>
          {this.state.isVisible ? 
          <ModalClickGroup
            visible={this.state.isVisible}
            transferData={(isVisible, data) => {
              this.setState({ isVisible });
              this.onClickGroupProduct(data.productTypeId)
            }}
            handleCloseModal={isVisible => this.setState({ isVisible })}
          /> : null }
          {visibleAddService ? 
          <OhModal
            width={window.innerWidth > 1370 ? 1300 : 1100}
            title={t("Thêm sản phẩm")}
            content={<AddProduct
              onQuickAdd = {(dataSave) => {
                this.setState({visibleAddService: false})
                if(dataSave){
                  this.setState({
                    products: [
                      ...this.state.products,
                      dataSave
                    ]
                  }, () => this.onClickProduct(dataSave.id))
                }
              }}
            />}
            onOpen={visibleAddService}
            onClose={() => this.setState({visibleAddService: false})}
            footer={null}
            style={{top: window.innerHeight >= 800 ? "" : 10}}
          />
          : null }
          <GridItem xs={12} >
            <FormLabel className="ProductFormAddEdit" style={{ margin: 0 }}>
              <b className='HeaderForm'>{t("Thông tin sản phẩm")}</b>
            </FormLabel>
          </GridItem>
        </GridContainer>
        {isReturn || isCanceledCard ? null :
          <GridContainer style={{ width: '100%', marginLeft: 0 }}>
            <GridItem className = {"products_auto_complete"} xs={12} style={{ width: '100%' }} id='AutoComplete' >
            <Row className={"oh-row"} key={`row_auto_complele`}>
            <Col style={{paddingLeft: 0}}>
              <OhAutoComplete
                dataSelects={products}
                onSearchData={value => this.onSearchProduct(value)}
                placeholder={t(Constants.PLACEHOLDER_SEARCH_PRODUCTS)}
                onClickValue={id => this.onClickProduct(id)}
                isButton
                onClick={() => this.setState({isVisible: true})}
                onKeyPress={e => this.onKeyInput(e)}
                onRef={ref => this.onRef = ref}
              />
              </Col>
              <Tooltip placement="topLeft" title={t('Thêm sản phẩm')}>
                <Col className={"oh-button-col"}>
                  <OhButton 
                    type="exit" 
                    onClick={() => {
                      this.setState({ visibleAddService: true })
                    }} 
                    className="button-add-information" 
                    icon={<Icon type="plus" className="icon-add-information" />} 
                  />
                </Col>
              </Tooltip>
              </Row>
            </GridItem>
          </GridContainer>            
          }
        <GridContainer style={{ width: "100%", margin: 0 }}>
          <GridItem xs={12} >
            <BrowserView>
            <OhTable
              onRef={ref => (this.tableRef = ref)}
              columns={columns}
              dataSource={ProductsForm}
              hasRemoveColumn={(value, record)=>{
                return !(
                    (record.stockDelete || isCanceledCard || isReturn)
                  )
              }}
              onClickRemove={(value, record) => {
                this.removeProduct(record)
              }}
              isNonePagination={true}
              id={"product-form-table"}
              emptyDescription={Constants.NO_PRODUCT}
            />
            </BrowserView>
            <MobileView>
              <List dataSource={ProductsForm} width="100%"
              locale={{ emptyText: <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t("Không có dữ liệu")} /> }}
                  renderItem={item => (
              <List.Item key={item.id}>
                <List.Item.Meta
                  title={<>{trans(item.productName)}<br/>{item.productCode}<br/>{item.unitPrice} x {item.quantity}</>}
                  />
                  <div style={{alignSelf:"flex-start"}}>{ExtendFunction.FormatNumber(Math.round(item.finalAmount)) || 0}</div>
                  {/*<div>{<>
                    <span className="AmountReport">{ExtendFunction.FormatNumber(item.finalAmount)}</span>
                    <br/>
                    <span style={{color: item.status === Constants.INVOICE_STATUS.id.CANCELLED ? "red": null}}>{t(Constants.INVOICE_STATUS.name[item.status])}</span>
                    </>}</div>*/}
              </List.Item>)}
            ></List>



            </MobileView>
          </GridItem>
        </GridContainer>

        <GridContainer style={{ width: "100%" }} justify='flex-end'>
          <GridItem xs={isMobile ? 12 : 6}>
            <Container>
              <Row>
                <Col style={{ textAlign: "right" }}>{t("Tổng tiền ({{ count }} sản phẩm)", {count: ProductsForm.length})}</Col>
                <Col style={{ maxWidth: 150, textAlign: "right", fontWeight: 700, fontSize:15 }}>
                  {ExtendFunction.FormatNumber(Math.round(totalAmount))}
                </Col>
              </Row>

              <Row className = 'row-invoice'>

                <Col style={{ textAlign: "right" }}>
                  {t("Chiết khấu")}
                </Col>

                <Col style={{ maxWidth: 150, textAlign: "right", fontWeight: 700 }}>
                  {isReturn || isCanceledCard ? this.getTextFieldDiscount(discountAmount, true) :
                    <Popover
                      trigger="click"
                      placement={isMobile ? "bottom" : "left"}
                      getPopupContainer={trigger => trigger.parentNode}
                      visible={this.state.visiblePopup.discount}
                      onVisibleChange={ (e) => this.visiblePopup(e, 'discount')}
                      content={
                        <DiscountForm
                          title={t('Chiết khấu thường')}
                          onChangeDiscount={(isPercent, discount) => {
                            this.setState({
                              discount: Number(discount),
                              isPercentDiscount: isPercent,
                              isChange: true,
                            }, () => this.getAmounts())
                          }}
                          discountAmount={discountAmount}
                          totalAmount={totalAmount}
                          onRef ={ ref => this.discountRef = ref}
                          onChangeVisible={ (e) => this.visiblePopup(e, 'discount') }
                        />
                      }
                    >
                      {this.getTextFieldDiscount(discountAmount, false, 'discount', 'isPercentDiscount')}
                    </Popover>
                  }
                </Col>
              </Row>

              <Row className = 'row-invoice'>
                <Col style={{ textAlign: "right" }}>
                  {t("Thuế")}
                </Col>
                <Col style={{ maxWidth: 150, textAlign: "right", fontWeight: 700 }}>
                {isReturn || isCanceledCard ? this.getTextFieldDiscount(taxAmount, true) :
                  <Popover
                    trigger="click"
                    placement={isMobile ? "bottom" : "left"}
                    getPopupContainer={trigger => trigger.parentNode}
                    visible={this.state.visiblePopup.tax}
                    onVisibleChange={ (e) => this.visiblePopup(e, 'tax') }
                    content={
                      <DiscountForm
                        title={t('Thuế GTGT')}
                        onChangeDiscount={(isPercent, tax) => {
                          this.setState({
                            tax: Number(tax),
                            isPercentTax: isPercent,
                            isChange: true,
                          }, () => this.getAmounts())
                        }}
                        discountAmount = {taxAmount}
                        onRef ={ ref => this.discountRef_1 = ref}
                        totalAmount = {+totalAmount - +discountAmount}
                        onChangeVisible={ (e) => this.visiblePopup(e, 'tax') }
                      />
                    }
                  >
                    {this.getTextFieldDiscount(taxAmount, false, 'tax', 'isPercentTax')}
                    </Popover>
                  }
                </Col>
              </Row>

              <Row className = 'row-invoice'>
                <Col style={{ textAlign: "right" }}>
                  {t("Phí giao hàng")}
                </Col>
                <Col style={{ maxWidth: 150, textAlign: "right", fontWeight: 700 }}>
                  <TextField
                    value={deliveryAmount ? ExtendFunction.FormatNumber(parseInt(deliveryAmount)) : 0}
                    InputProps={{
                      inputProps: {
                        style: { textAlign: "right", width: 80, padding: 0 },
                      },
                      onChange: (e) => {
                        let value = e.target.value.replace(/[^0-9]/g, "");
                        this.setState({
                          delivery: value,
                          isChange: true,
                        }, () => this.getAmounts())
                      },
                      onClick: (e) => {
                        e.target.select()
                      },
                      readOnly: isReturn || isCanceledCard ? true : false
                    }}
                  />
                </Col>
              </Row>

              <Row className = 'row-invoice'>
                <Col style={{ textAlign: "right" }}>{t("Khách phải trả")}</Col>
                <Col style={{ maxWidth: 150, textAlign: "right", fontWeight: 700 }} className={'total-amount'}>
                  {finalAmount ? ExtendFunction.FormatNumber(Math.round(finalAmount)) : 0}
                </Col>
              </Row>
            </Container>
          </GridItem>
        </GridContainer>
      </>
    );
  }
}


export default connect(function(state) {
  return ({
    stockList: state.stockListReducer.stockList,
    productUnits: state.productUnitReducer.productUnits,
    productList: state.productListReducer.products,
    branchId: state.branchReducer.branchId,
    nameBranch: state.branchReducer.nameBranch,    
  });
})(
  withTranslation("translations")(
    withStyles(theme => ({
      ...regularFormsStyle
    }))(ProductForm)
  )
);
