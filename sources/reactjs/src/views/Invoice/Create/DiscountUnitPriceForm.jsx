import React from "react";
import { connect } from "react-redux";
import Card from "components/Card/Card.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import regularFormsStyle from "assets/jss/material-dashboard-pro-react/views/regularFormsStyle";
// multilingual
import { withTranslation } from "react-i18next";
import ExtendFunction from "lib/ExtendFunction";
import "date-fns";
import Constants from "variables/Constants/";
import FormLabel from "@material-ui/core/FormLabel";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem";
import { ButtonGroup } from "@material-ui/core";
import ButtonTheme from "components/CustomButtons/Button.jsx";
import OhCheckBox from "components/Oh/OhCheckbox";
import { notifyError } from 'components/Oh/OhUtils';
import OhNumberInput from "components/Oh/OhNumberInput";
import _ from 'lodash';

class ProductForm extends React.Component {
  constructor(props) {
    super(props);
    if (this.props.onRef) this.props.onRef(this);    

    let { defaultFormData } = this.props;

    this.state = {
      formData: {
        discountType: defaultFormData.discountType !== Constants.DISCOUNT_TYPES.id.percent ? Constants.DISCOUNT_TYPES.id.VND : defaultFormData.discountType,
        unitPrice: defaultFormData.unitPrice === undefined ? 0 : defaultFormData.unitPrice,
        discount: defaultFormData.discount === undefined ? 0 : defaultFormData.discount,
        sellPrice: defaultFormData.finalAmount === undefined ? 0 : defaultFormData.finalAmount,
      },
      isCheckSellPrice: false
    };
  }

  checkMaxDiscount = (discount, discountType, maxDiscount, unitPrice) => {
    const {t} = this.props;
    
    if(discountType === Constants.DISCOUNT_TYPES.id.percent && maxDiscount !== undefined){
      if(discount > maxDiscount){
        notifyError(t("Chiết khấu tối đa của sản phẩm này là {{maxDiscount}}%", {maxDiscount: t(maxDiscount)} ));
        return false;
      }
    }
    else {
      if(discount > maxDiscount * unitPrice / 100){
        notifyError(t("Chiết khấu tối đa của sản phẩm này là {{maxDiscount}}%", {maxDiscount: t(maxDiscount)}));
        return false;
      }
    }
    return true;
  }

  componentDidUpdate = (prevProps, prevState) => {
    let discount;
    const { formData } = this.state;
    const { discountType } = formData;
    const { defaultFormData } = this.props;
    if (prevState.formData.discountType !== discountType && prevState.formData.sellPrice === formData.sellPrice && !formData.isPromoted) {
      if (defaultFormData.unitPrice && defaultFormData.discount) {
        if (discountType === Constants.DISCOUNT_TYPES.id.percent) {
          discount = Number(defaultFormData.discount / defaultFormData.unitPrice * 100);
        }
        if (discountType === Constants.DISCOUNT_TYPES.id.VND) {
          discount = Number(defaultFormData.discount);
        }
        let sellPrice = ExtendFunction.getSellPrice(defaultFormData.unitPrice, discount, discountType, formData.isPromoted)
        this.setState({
          formData: {
            ...this.state.formData, 
            discount: discount > 0 ? discount : 0,
            sellPrice: sellPrice > 0 ? Math.round(sellPrice) : 0
          }
        }, () => this.sendChange());
      }
    }
    if(!_.isEqual(prevProps.defaultFormData, defaultFormData)){
      this.setState({
        formData: {
          discountType: defaultFormData.discountType !== Constants.DISCOUNT_TYPES.id.percent ? Constants.DISCOUNT_TYPES.id.VND : defaultFormData.discountType,
          unitPrice: defaultFormData.unitPrice === undefined ? 0 : defaultFormData.unitPrice,
          discount: defaultFormData.discount === undefined ? 0 : defaultFormData.discount,
          sellPrice: defaultFormData.finalAmount === undefined ? 0 : defaultFormData.finalAmount,
        },
      })
    }
  }

  sendChange = () => {
    if (this.props.onChange) this.props.onChange(this.state.formData)
  }

  onChangeType = (discountType) => {
    this.setState({ formData: {
      ...this.state.formData,
      discountType: discountType,
    }});
  }

  onChangeDiscount = (discount) => {
    const {formData, isCheckSellPrice} = this.state;
    if(isCheckSellPrice){
      let unitPrice = ExtendFunction.getUnitPrice(formData.sellPrice, discount, formData.discountType);
      this.setState({ formData: {
        ...formData,
        discount: discount > 0 ? discount : 0,
        unitPrice: unitPrice > 0 ? Math.round(unitPrice) : 0
      }}, () => this.sendChange());
    }
    else{
      let sellPrice = ExtendFunction.getSellPrice(formData.unitPrice, discount, formData.discountType, formData.isPromoted);
      this.setState({ formData: {
        ...formData,
        discount: discount > 0 ? discount : 0,
        sellPrice: sellPrice > 0 ? Math.round(sellPrice) : 0
      }}, () => this.sendChange());
    }
  }

  onChangeSellPrice = (sellPrice) => {
    const {formData, isCheckSellPrice} = this.state;
    sellPrice = Math.round(sellPrice);
    if(isCheckSellPrice){
      let unitPrice = ExtendFunction.getUnitPrice(sellPrice, formData.discount, formData.discountType);
      this.setState({ formData: {
        ...formData,
        unitPrice: unitPrice > 0 ? Math.round(unitPrice) : 0,
        sellPrice
      }}, () => this.sendChange());
    }
    else{
      let discount = this.state.formData.unitPrice - sellPrice;
      this.setState({ formData: {
        ...this.state.formData,
        discount: discount > 0 ? discount : 0,
        discountType: Constants.DISCOUNT_TYPES.id.VND,
        sellPrice
      }}, () => this.sendChange());
    }
  }

  onChangePromoted = (isPromoted) => {
    this.setState({
      formData: {
        ...this.state.formData,
        discount: isPromoted ? 100 : this.state.formData.setDiscount,
        discountType: isPromoted ? Constants.DISCOUNT_TYPES.id.percent : this.state.formData.setDiscountType,
        sellPrice: isPromoted ? 0 : this.state.formData.setPrice,
        setPrice: this.state.formData.sellPrice,
        setDiscount: this.state.formData.discount,
        setDiscountType: this.state.formData.discountType,
        isPromoted: isPromoted,
    }}, () => this.sendChange())
  }

  onChangeUnitPrice = (unitPrice) => {
    unitPrice = Math.round(unitPrice); 
    let {formData} = this.state;
    let sellPrice = 0;
    if(formData.discountType === Constants.DISCOUNT_TYPES.id.percent){
      sellPrice = unitPrice - (unitPrice * formData.discount/100);
    }
    else{
      sellPrice = unitPrice - formData.discount;
    }
    this.setState({ formData: {
      ...this.state.formData,
      sellPrice: sellPrice > 0 ? Math.round(sellPrice) : 0,
      unitPrice
    }}, () => this.sendChange());
  }

  render() {
    const { t, isInvoice, defaultFormData, sellPriceName } = this.props;
    const {maxDiscount} = defaultFormData;
    const { formData, isCheckSellPrice } = this.state;
    let perCent =  formData.discountType === Constants.DISCOUNT_TYPES.id.percent;
    
    return (
      <Card className='CardPopover'>
          <GridContainer justify='center' style={{marginTop: 10}}>
            <FormLabel>
              <b className='HeaderForm'>{t("Chiết khấu")}</b>
            </FormLabel>
          </GridContainer>

          <from onKeyDown={(e) => {if(e.keyCode === 13) this.props.onChangeVisible(false)}}>

          <GridContainer xs={12} className='GridContentPopover'>
            <GridItem xs={4}>
              <FormLabel className="LabelPopover">
                <b className='ContentForm'>{t("Đơn giá")}</b>
              </FormLabel>
            </GridItem>
            <GridItem xs={8}>
              <OhNumberInput
                autoFocus
                style={{marginTop: 5, height: 29, textAlign: 'right' }}
                onBlur={(e) => {
                  let value = 0;
                  
                  if (isNaN(ExtendFunction.UndoFormatNumber(e.target.value)) === false) {
                    value = Math.round(ExtendFunction.UndoFormatNumber(e.target.value));
                  }
                  if (e.target.value === "")
                    value = 0;
                  if(value !== Math.round(formData.unitPrice))
                  this.onChangeUnitPrice(value)
                }}
                defaultValue={Math.round(formData.unitPrice) || 0}
                isDecimal={false}
                isNegative={false}
                permission={{
                  name: Constants.PERMISSION_NAME.EDIT_PRICE,
                  type: Constants.PERMISSION_TYPE.TYPE_ALL
                }}
                onRef = { ref => this.ohnumberinputRef = ref}
              />
            </GridItem>
          </GridContainer>

          {isInvoice ?
          <GridContainer xs={12} className='GridContentPopover' justify={'flex-end'}>
            <GridItem xs={8} style={{marginTop: 5}}>
              <OhCheckBox
                options={ [{label: t('Tính theo giá bán'), value: 1}]}
                onChange={value => this.setState({ isCheckSellPrice: value[0]})}
                permission={{
                  name: Constants.PERMISSION_NAME.EDIT_PRICE,
                  type: Constants.PERMISSION_TYPE.TYPE_ALL
                }}
                disabled={formData.isPromoted}
              />
            </GridItem>
          </GridContainer>
          : null }

          <GridContainer xs={12} className='GridContentPopover'>
            <GridItem xs={4}>
              <FormLabel className="LabelPopover">
                <b className='ContentForm'>{t("Chiết khấu")}</b>
              </FormLabel>
            </GridItem>
            <GridItem xs={8}>
              <ButtonGroup>
                <OhNumberInput
                  style={{marginTop: 5, height: 29, textAlign: 'right' }}
                  onBlur={(e) => {
                    let value = 0;
                    
                    if (isNaN(ExtendFunction.UndoFormatNumber(e.target.value)) === false) {
                      value = parseFloat(ExtendFunction.UndoFormatNumber(e.target.value));
                    }
                    if (e.target.value === "")
                      value = 0;
                    let unitPrice = formData.unitPrice;
                    if(isCheckSellPrice){
                      unitPrice = ExtendFunction.getUnitPrice(formData.sellPrice, value, formData.discountType);
                    }
                    if((this.checkMaxDiscount(value, formData.discountType, maxDiscount, unitPrice) && isInvoice) || !isInvoice)
                      this.onChangeDiscount(value)
                  }}
                  onFocus= {(event) => event.target.select()}
                  max={perCent ? Constants.NUMBER_LENGTH.PERCENT_VALUE : Constants.NUMBER_LENGTH.VALUE}
                  defaultValue={ perCent ? formData.discount : Math.round(formData.discount) || 0}
                  isDecimal={true}
                  isNegative={false}
                  disabled= {formData.isPromoted}
                />
                <ButtonTheme size="sm" className={formData.discountType === Constants.DISCOUNT_TYPES.id.VND ? 'buttonGreen' : 'buttonGray'} id="vnd" onClick={() => this.onChangeType(Constants.DISCOUNT_TYPES.id.VND)}>
                  VND
              </ButtonTheme>
                <ButtonTheme size="sm" className={perCent ? 'buttonGreen' : 'buttonGray'} id="%" onClick={() => this.onChangeType(Constants.DISCOUNT_TYPES.id.percent)}>
                  %
              </ButtonTheme>
              </ButtonGroup>
            </GridItem>
          </GridContainer>

          <GridContainer xs={12} className='GridContentPopover'>
            <GridItem xs={4}>
              <FormLabel className="LabelPopover">
                <b className='ContentForm'>{sellPriceName}</b>
              </FormLabel>
            </GridItem>
            <GridItem xs={8}>
              <OhNumberInput
                style={{marginTop: 5, height: 29, textAlign: 'right' }}
                defaultValue={ Math.round(formData.sellPrice) || 0}
                isDecimal={ false}
                isNegative={false}
                disabled = {formData.isPromoted}
                onBlur={(e) => {
                  let value = 0;
                  
                  if (isNaN(ExtendFunction.UndoFormatNumber(e.target.value)) === false) {
                    value = parseFloat(ExtendFunction.UndoFormatNumber(e.target.value));
                  }
                  if (e.target.value === "")
                    value = 0;
                  let unitPrice = formData.unitPrice;
                  if(isCheckSellPrice){
                    unitPrice = ExtendFunction.getUnitPrice(value, formData.discount, formData.discountType);
                  }
                  if((this.checkMaxDiscount(unitPrice - value, Constants.DISCOUNT_TYPES.id.VND, maxDiscount, unitPrice)&& isInvoice) || !isInvoice)
                    this.onChangeSellPrice(value);
                }}

              />
            </GridItem>
          </GridContainer>
          </from>
          <GridContainer xs={12} className='GridContentPopover' justify={'flex-end'}>
            <GridItem xs={8} style={{marginTop: 5}}>
              <OhCheckBox
                options={ [{label: t('Khuyến mãi'), value: 1}]}
                onChange={value => this.onChangePromoted(value[0])}
                disabled={maxDiscount < 100}
              />
            </GridItem>
          </GridContainer>
      </Card>
    );
  }
}

export default connect()(
  withTranslation("translations")(
    withStyles(theme => ({
      ...regularFormsStyle
    }))(ProductForm)
  )
);
