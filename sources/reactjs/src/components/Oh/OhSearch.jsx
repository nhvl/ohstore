import React, { PureComponent } from 'react';
import { AutoComplete, Select } from "antd";
import Constants from "variables/Constants/";
import _ from "lodash";

const { Option } = Select;

class OhAutoComplete extends PureComponent {
  constructor(props){
    super(props);
    this.state={
      value: ""
    };
    if (this.props.onRef) this.props.onRef(this)
    this.sendChange = _.debounce(this.sendChange, Constants.UPDATE_TIME_OUT);
  }
  
  componentDidUpdate = (prevProps) => {
    if(prevProps.defaultValue !== this.props.defaultValue) {
      this.setState({value: this.props.defaultValue})
    }
  }
  
  onChange = (value) => {
    this.setState({value}, () => this.sendChange())
  }
  
  sendChange = () => {
    if(this.props.onChange) this.props.onChange(this.state.value)
  }

  render() {
    let { disabled, placeholder, options, className, render } = this.props;
    let {value} = this.state;
    return (
      <AutoComplete
        dataSource={options}
        value={value}
        children={value}
        disabled={disabled}
        className={className}
        onChange={this.onChange}
        placeholder={placeholder}
        optionLabelProp="value"
        getPopupContainer={(triggerNode) => triggerNode}
      >
        {options.map(render || (item => <Option key={item.value}>{item.name || item.value}</Option>))}
      </AutoComplete>
    );
  }
}

export default OhAutoComplete;