import React, { Component } from "react";
import { Modal } from 'antd';
import {Modal as ModalMobile} from 'antd-mobile';
import {BrowserView, MobileView} from "react-device-detect";

class OhModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      open: this.handleOpen,
      close: this.handleClose,
    }
  }

  handleOpen=()=>{
    this.setState({visible:true})
  }

  handleClose=()=>{
    this.setState({visible:false})
  }

  render() {
    const { onClose, onOk, title, key, content, onOpen, okText, cancelText, ClassName, footer,style,width } = this.props;
    return (<>
      <BrowserView>
        <Modal
        footer={footer}
        className={ClassName}
        title={title}
        key={key}
        visible={onOpen}
        onOk={onOk}
        onCancel={onClose}
        onClose={onClose}
        maskClosable={false}
        okText={okText}
        cancelText={cancelText}
        width={width}
        style={style}
      >
        {content}
      </Modal>
      </BrowserView>
      <MobileView>
        <ModalMobile
          footer={footer ? footer : []}
          className={ClassName}
          title={title}
          key={key}
          visible={onOpen}
          onOk={onOk}
          onCancel={onClose}
          onClose={onClose}
          maskClosable={false}
          okText={okText}
          cancelText={cancelText}
          width={width}
          style={style}
        >
          {content}
        </ModalMobile>
      </MobileView>
      </>
    );
  }
}

export default OhModal
