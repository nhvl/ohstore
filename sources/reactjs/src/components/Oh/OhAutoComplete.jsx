import React, { PureComponent } from 'react';
import { AutoComplete, Input, Select, Button, Icon } from "antd";
import ExtendFunction from "lib/ExtendFunction";
import { Container } from "react-grid-system";
import { trans } from "lib/ExtendFunction";
import { MdSearch } from 'react-icons/md';

const { Option } = Select;

class OhAutoComplete extends PureComponent {
  constructor(props){
    super(props);
    this.state={
      value: ""
    };
    if (this.props.onRef) this.props.onRef(this)
  }
  renderOptions = item => {
    return (
      <Option key={item.id} text={trans(item.name, true)} label={trans(item.name, true)}>
        <div className="global-search-item">
          <span className="global-search-item-desc" title={ item.name.length > 95 ? trans(item.name,true):""}>{trans(item.name)}</span>
          <span className="global-search-item-count">{item.code}</span>
        </div>
      </Option>
    );
  };

  onSearchData = ExtendFunction.debounce(async value => {
    this.props.onSearchData(value)
  }, 500);

  onResetValue = () => {
    this.setState({ value: "" })
  }

  render() {
    let { onClickValue, disabled, placeholder, dataSelects, onClick, isButton, onKeyPress, autoFocus, classNameAutoCompleted, widthInput, isViewValue } = this.props;
    
    return (
      <Container className = {["AutoCompleted-container", classNameAutoCompleted ].join(' ')} style={{width: widthInput ? widthInput : "100%"}}>
        <AutoComplete
          className="global-search"
          size="default"
          dataSource={dataSelects.map(this.renderOptions)}
          value={this.state.value}
          onSelect={(value, record) => {  
            let name = record ?  record.props.label : ''; 
                     
            onClickValue(value);
            this.setState({ value: isViewValue ? name : "" })
          }}
          onSearch={this.onSearchData}
          onChange={value=>this.setState({value})}
          disabled={disabled}
          placeholder={placeholder}
          optionLabelProp="label"
          getPopupContainer={trigger => trigger.parentNode}
          onInputKeyDown={onKeyPress}
          ref={ref => this.ref = ref }
          autoFocus={autoFocus || autoFocus === undefined ? true : false}
        >
          <Input prefix={<MdSearch/>} className = { classNameAutoCompleted ? classNameAutoCompleted : ''} style={{width: "100%"}} />
        </AutoComplete>
        { isButton ?
          <Button
            className="search-btn"
            style={{ position: "absolute", border: "unset", backgroundColor: "unset", zIndex: 1, right: 0, marginTop: -6}}
            size="large"
            onClick={onClick}
          >
            <Icon type="unordered-list" />
          </Button>
          : null }
      </Container>
    );
  }
}

export default OhAutoComplete;