import React, { Component } from "react";
import { Select, Tooltip, Empty, ConfigProvider } from "antd";
import { withTranslation } from 'react-i18next';
import ExtendFunction from "lib/ExtendFunction";
import _ from "lodash"
import Constants from "variables/Constants";

const { Option } = Select;

class OhMultiChoice extends Component {
  constructor(props) {
    super(props);
    let { defaultValue } = this.props;
    
    this.state = {
      selects: defaultValue,
      selectTypes: defaultValue,
      selectNameType: [],
      dataOptions: []
    };
    this.counter_one = 0;
    this.counter_two = 0;
    this.onSearchValue = _.debounce(this.onSearchValue, Constants.UPDATE_TIME_OUT)
  }

  componentDidMount = () => {
    this.getDataSource();
  }

  onChange = async (value, obj) => {
    let dataSourcePType = this.props.dataSourcePType.slice()
    let valueAll, allTypes = [], selectNameType = [];

    let findAllType = value.find(item => item === "all");
    
    if (findAllType === "all") {
      dataSourcePType.forEach((item) => allTypes.push(item.id))
      valueAll = ["all"]
    } else obj.forEach((item,i) => selectNameType.push(item.props.children + (obj.length > (i + 1) ? ", " : "")) )
    
    let selectTypes = findAllType === "all" ? allTypes : value;
    let selects = findAllType === "all" ? valueAll : value;

    if(value[0] === "all" && value.length > 1)
    {
      selectTypes = value.slice(1);
      selects = value.slice(1);
    }
    
    this.setState({
      selectTypes,
      selects,
      selectNameType,
    },() => {
      console.log(selectNameType)
      this.props.onChange(selectTypes)
      this.onSearchValue("")
    })
  }

  componentDidUpdate(prevProps, prevState) {   
    if (JSON.stringify(this.props.dataSourcePType) !== JSON.stringify(prevProps.dataSourcePType)){         
      this.getDataSource();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (JSON.stringify(this.props.dataSourcePType) !== JSON.stringify(nextProps.dataSourcePType) || JSON.stringify(nextProps.defaultValue) !== JSON.stringify(this.props.defaultValue) 
    || JSON.stringify(this.state.dataOptions) !== JSON.stringify(nextState.dataOptions) || JSON.stringify(this.state.selectTypes) !== JSON.stringify(nextState.selectTypes)
    ){         
      return true;
    }
    else return false
  }

  onSearchValue(value) {
    const { dataSourcePType } = this.props;
    let dataSearch = [];
            
    dataSourcePType.forEach(item => {
      if(ExtendFunction.removeSign(item.name.toLowerCase()).includes(ExtendFunction.removeSign(value.toLowerCase())) || ( item.code 
      && ExtendFunction.removeSign(item.code.toString().toLowerCase()).includes(ExtendFunction.removeSign(value.toLowerCase()))) ||
      ( item.mobile && ExtendFunction.removeSign(item.mobile.toString().toLowerCase()).includes(ExtendFunction.removeSign(value.toLowerCase())))){
        dataSearch.push(item);
      }
    })

    this.getDataSource(dataSearch)
  }

  getDataSource(dataSearch) {
    const { dataSourcePType, t } = this.props;
    let dataSource = [];

    if (dataSourcePType.length > 0 || (dataSearch && dataSearch.length > 0)) {
      (dataSearch || dataSourcePType).sort((a, b) =>a.name ? a.name.toString().localeCompare(b.name) : "").map(data => dataSource.push(<Option value={data.id} key={data.id} title={data.name} record={data}>{data.name}</Option>));
    }

    if (dataSource.length === dataSourcePType.length) {
      dataSource = [<Option value={"all"} key={"all"} >{t("Tất cả")}</Option>].concat(dataSource)
    }
    
    this.setState({
      dataOptions: dataSource.length > 10 ? dataSource.slice(0,10) : dataSource
    })
  }
  customizeRenderEmpty = () => {
    const {t} = this.props;
    return (<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t("Không có dữ liệu")} />)
  };

 render() {
    const { dataSourcePType, options, onChange, placeholder, maxValue, disabled, ...rest } = this.props;
    let { dataOptions } = this.state;
    
    this.counter_one = 0;
    this.counter_two = 0;

    return (
      <ConfigProvider renderEmpty={this.customizeRenderEmpty}>
        <Tooltip 
          placement="rightTop" 
          title={this.state.selectNameType.length > 1 ? this.state.selectNameType.join("") : "" } 
          mouseEnterDelay={0.5}
        >        
          <Select
            placeholder={placeholder}
            mode="multiple"
            onChange={(value, obj) => this.onChange(value, obj)}
            value={this.state.selects}
            filterOption={false}
            onSearch={value => {
              this.onSearchValue(value)
            }}
            maxTagTextLength={6}
            maxTagCount={1}
            getPopupContainer={trigger => trigger.parentNode}
            {...rest}
          >
            {dataOptions}
          </Select>        
        </Tooltip>
      </ConfigProvider> 
    );
  }
}

export default withTranslation("translations")(OhMultiChoice);
