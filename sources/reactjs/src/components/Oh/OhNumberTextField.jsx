import React, { Component } from "react";
import NumberFormat from 'react-number-format';


class OhNumberTextField extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  
  render() {
    const { thousandSeparator, decimalSeparator, thousandsGroupStyle, decimalScale, fixedDecimalScale, allowNegative, allowEmptyFormatting, allowLeadingZeros, 
        mask, format, getInputRef, allowedDecimalSeparators, isAllowed, onValueChange, customInput, removeFormatting, isNumericString, ...other} = this.props;    
    return (
        <NumberFormat
        {...other}
        getInputRef={getInputRef}
        onValueChange={onValueChange}
        thousandSeparator={thousandSeparator}
        decimalSeparator={decimalSeparator}
        thousandsGroupStyle={thousandsGroupStyle}
        decimalScale={decimalScale}
        fixedDecimalScale={fixedDecimalScale}
        allowNegative={allowNegative}
        allowEmptyFormatting={allowEmptyFormatting}
        allowLeadingZeros={allowLeadingZeros}
        mask={mask}
        format={format}
        allowedDecimalSeparators={allowedDecimalSeparators}
        isAllowed={isAllowed}
        customInput={customInput}
        removeFormatting={removeFormatting}
        isNumericString={isNumericString}
      />
    );
  }
}

export default OhNumberTextField
