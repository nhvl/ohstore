import React,{ Component } from "react";
import { connect } from "react-redux";
import {  DatePicker } from "antd";
import {  DatePicker as DatePickerMobile, List, LocaleProvider } from "antd-mobile";
import moment from 'moment';
import VN from 'antd/es/date-picker/locale/vi_VN';
import KR from 'antd/es/date-picker/locale/ko_KR';
import EN from 'antd/es/date-picker/locale/en_US';
import enUS from 'antd-mobile/lib/locale-provider/en_US';
import _ from 'lodash';
import 'moment/locale/vi';
import 'moment/locale/ko';
import {isMobile} from "react-device-detect";
import { trans } from "lib/ExtendFunction";

class OhDatePicker extends Component {
  constructor(props) {
    super(props);
    let { defaultValue } = this.props;
    this.state = {
      formatDate: defaultValue || "",
      locale: VN,
    }
  }

  componentDidMount(){
    this.changeLanguage()
  }

  changeLanguage(){
    switch(this.props.languageCurrent){
      case 'vn':
        moment.locale('vi')
        this.setState({locale: VN})
        break;
      case 'kr':
        moment.locale('ko')
        this.setState({locale: KR})
        break;
      default:
        moment.locale('en')
        this.setState({locale: EN})
        break;
    }
  }

  sendChange = () => {
    if(this.props.onChange) {
      this.props.onChange(this.state.formatDate)
    }
  }

  handleChangeTime = e => {
    let time = e ? moment(e) : 0
    
    this.setState({ formatDate: time },
      () => this.sendChange()
    );
  };

  componentDidUpdate = (prevProps) => {
    if(prevProps.languageCurrent !== this.props.languageCurrent){
      this.changeLanguage()
    }
    
    if( this.props.defaultValue && !_.isEqual(this.applyDefaultValue(prevProps.defaultValue), this.applyDefaultValue(this.props.defaultValue)) ) {
      this.setState({
        ...this.applyDefaultValue(this.props.defaultValue)
      }, this.sendChange);
    }
  }

  applyDefaultValue = (defaultValue) => {
    return (
      Array.isArray(defaultValue) ? {
        formatDate: defaultValue[0] === 0 ? "" : defaultValue[0]
      } : {
        formatDate: defaultValue === 0 ? "" : defaultValue || moment()
            .startOf("day")
        }
    )
  }

  disabledDate = (current )=> {
    // Can not select days before today and today
    if(this.props.timeDisableStart) 
      return current && current <  moment(this.props.timeDisableStart).subtract(1, "days").endOf("day");
    
    return current && current > moment().endOf("day");
  }

 
render(){
  const { placeholder, disabled, showTime, isDisabledDate, label } = this.props;
  const { formatDate, locale } = this.state;

  return (<>
    {isMobile ? (<LocaleProvider locale={enUS}><DatePickerMobile
      mode={showTime ? "datetime" : "date"}
      style={{ width: '100%' }} 
      okText={trans("Đồng ý")}
      dismissText={trans("Đóng")}
      year = {trans("Năm")}
      disabled = {disabled}
      onOk={this.handleChangeTime} 
      onChange={this.handleChangeTime} 
      value={formatDate && Number(formatDate._i) === 0 ? "" : new Date(formatDate)} 
      format={showTime ? "DD/MM/YYYY HH:mm:ss" : "DD/MM/YYYY"}
      ><List.Item arrow="horizontal"></List.Item></DatePickerMobile></LocaleProvider>) : (<DatePicker
      showTime={showTime}
      style={{ width: '100%' }} 
      locale={locale} 
      disabled = {disabled}
      disabledDate = {isDisabledDate ? this.disabledDate :  null}
      placeholder={placeholder}
      getCalendarContainer={trigger => trigger.parentNode}
      onOk={this.handleChangeTime} 
      onChange={this.handleChangeTime} 
      value={formatDate && Number(formatDate._i) === 0 ? "" : formatDate} 
      format={showTime ? "DD/MM/YYYY HH:mm:ss" : "DD/MM/YYYY"} />)}
    </>
    );
  }
};

export default connect(
  function (state) {
    return {
      languageCurrent: state.languageReducer.language
    };
  }
)(OhDatePicker);
