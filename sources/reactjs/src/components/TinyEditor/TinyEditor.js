import React, { Component } from 'react';
import tinymce from 'tinymce/tinymce.js';
import { Editor } from '@tinymce/tinymce-react';
import 'tinymce/themes/silver';
import 'tinymce/plugins/paste';
import 'tinymce/plugins/autolink';
import 'tinymce/plugins/link';
import 'tinymce/plugins/lists';
import 'tinymce/plugins/advlist';
import 'tinymce/plugins/image';
import 'tinymce/plugins/charmap';
import 'tinymce/plugins/print';
import 'tinymce/plugins/preview';
import 'tinymce/plugins/anchor';
import 'tinymce/plugins/hr';
import 'tinymce/plugins/searchreplace';
import 'tinymce/plugins/visualblocks';
import 'tinymce/plugins/code';
import 'tinymce/plugins/fullscreen';
import 'tinymce/plugins/directionality';
import 'tinymce/plugins/quickbars';
import 'tinymce/plugins/insertdatetime';
import 'tinymce/plugins/media';
import 'tinymce/plugins/table';
import 'tinymce/plugins/imagetools';
import 'tinymce/plugins/wordcount';
import 'tinymce/plugins/codesample';
import 'tinymce/plugins/template';
import Resizer from 'react-image-file-resizer';
import {isMobile} from "react-device-detect";

class TinyEditor extends Component {
  constructor(props) {
    super(props);
    this.tempFiles = [];
    this.id = "";
    this.state={
      editor: null
    }
  }

  render() {
    return (
      <Editor
        value={this.props.content}
        selector={"#" + this.props.id}
        disabled={this.props.isDisabled}
        onEditorChange={(content, editor) => this.props.onEditorChange(editor.getContent())}
        init={{
          setup: editor => {
            this.setState({ editor });
          },
          theme: "silver",
          readonly: 1,
          height: this.props.height || 500,
          image_title: true,
          automatic_uploads: true,
          file_picker_types: 'image',
          file_picker_callback: (cb, value, meta) => {
            let input = document.createElement('input');
            let id = 'image_' + (new Date()).getTime();
            this.id = id;
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            input.onchange = (e) => {
              let file = e.path[0].files[0];
              let reader = new FileReader();

              reader.onload = function (e) {
                let blobCache = tinymce.activeEditor.editorUpload.blobCache;
                Resizer.imageFileResizer(
                  file,
                  file.width,
                  file.height,
                  'JPEG',
                  30,
                  0,
                  uri => {
                    let base64 = uri.split(',')[1];
                    let blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
                    cb(blobInfo.blobUri(), { title: file.name });
                  }
                );
              };
              reader.readAsDataURL(file);
            };
            input.click();
          },
          menubar: "custom",
          menu: {
            custom: { title: "Custom menu", items: "basicitem nesteditem toggleitem" }
          },
          plugins: [
            "advlist autolink lists link image charmap print preview anchor hr",
            "searchreplace visualblocks code fullscreen directionality",
            "insertdatetime media table paste imagetools wordcount codesample template"
          ],
          toolbar1: `undo redo | fontselect | fontsizeselect | styleselect | 
                  bold italic underline strikethrough | forecolor backcolor | hr | blockquote`,          
          toolbar2: `alignleft aligncenter alignright alignjustify | numlist bullist outdent indent | charmap | quicktable table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol`,
          toolbar3: `fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl | removeformat | code | searchreplace`,       
        }} 
      />
    );
  }
}

export default TinyEditor;