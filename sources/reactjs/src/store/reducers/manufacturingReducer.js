const INITIAL_STATE = {
  manufacturings: {},
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case "CHANGE_MANUFACTURING_LIST":
      return {
        ...state,
        manufacturings: action.manufacturings,
      };

    default:
      return state;
  }
}