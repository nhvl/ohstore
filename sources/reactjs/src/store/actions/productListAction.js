function changeProductList(products) {
  return { type: 'CHANGE_PRODUCT_LIST', products}

}

export default {
  changeProductList
};