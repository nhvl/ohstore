function changeStockCheck(stockChecks) {
  return { type: 'CHANGE_STOCK_CHECK', stockChecks }

}

export default {
  changeStockCheck
};