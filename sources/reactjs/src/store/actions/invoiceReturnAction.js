function changeInvoiceReturn(invoiceReturns) {
  return { type: 'CHANGE_INVOICE_RETURN', invoiceReturns }

}

export default {
  changeInvoiceReturn
};