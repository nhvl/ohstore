function changeIncomeExpenseType(incomeExpenseTypes) {
  return { type: 'CHANGE_INCOME_EXPENSE_TYPE', incomeExpenseTypes }

}

export default {
  changeIncomeExpenseType
};