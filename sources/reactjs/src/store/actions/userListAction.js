function changeUserList(users) {
  return { type: 'CHANGE_USER_LIST', users }

}

export default {
  changeUserList
};