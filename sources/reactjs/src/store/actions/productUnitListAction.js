function changeProductUnitList(productUnits) {
  return { type: 'CHANGE_PRODUCT_UNIT_LIST', productUnits}

}

export default {
  changeProductUnitList
};