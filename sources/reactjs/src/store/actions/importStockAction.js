function changeImportStockList(importStocks) {
  return { type: 'CHANGE_IMPORT_STOCK_LIST', importStocks }

}

export default {
  changeImportStockList
};