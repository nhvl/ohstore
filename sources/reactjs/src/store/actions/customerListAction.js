function changeCustomerList(customers) {
  return { type: 'CHANGE_CUSTOMER_LIST', customers }

}

export default {
  changeCustomerList
};