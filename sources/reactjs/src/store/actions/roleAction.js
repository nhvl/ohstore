function changeRoleList(roles) {
  return { type: 'CHANGE_ROLE_LIST', roles }

}

export default {
  changeRoleList
};