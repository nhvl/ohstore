function changeSupplierList(suppliers) {
  return { type: 'CHANGE_SUPPLIER_LIST', suppliers }

}

export default {
  changeSupplierList
};