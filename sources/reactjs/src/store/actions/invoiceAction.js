function changeInvoice(invoices) {
  return { type: 'CHANGE_INVOICE', invoices }

}

export default {
  changeInvoice
};