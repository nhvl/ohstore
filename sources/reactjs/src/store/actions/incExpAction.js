function changeIncomeExpense(incomeExpenses) {
  return { type: 'CHANGE_INCOME_EXPENSE', incomeExpenses }

}

export default {
  changeIncomeExpense
};