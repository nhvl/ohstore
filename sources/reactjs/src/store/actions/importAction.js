function changeImport(imports) {
  return { type: 'CHANGE_IMPORT', imports }

}

export default {
  changeImport
};