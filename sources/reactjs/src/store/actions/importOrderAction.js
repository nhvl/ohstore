function changeImportOrder(importOrders) {
  return { type: 'CHANGE_IMPORT_ORDER', importOrders }

}

export default {
  changeImportOrder
};