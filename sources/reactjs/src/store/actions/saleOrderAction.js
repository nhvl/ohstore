function changeSaleOrder(saleOrders) {
  return { type: 'CHANGE_SALE_ORDER', saleOrders }

}

export default {
  changeSaleOrder
};