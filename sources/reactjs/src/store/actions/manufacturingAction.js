function changeManufacturingList(manufacturings) {
  return { type: 'CHANGE_MANUFACTURING_LIST', manufacturings }

}

export default {
  changeManufacturingList
};