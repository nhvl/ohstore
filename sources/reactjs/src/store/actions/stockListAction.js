function changeStockList(stockList) {
    return { type: 'CHANGE_STOCK_LIST', stockList }
    
  }
  
  export default {
    changeStockList
  };