function changeExportStockList(exportStocks) {
  return { type: 'CHANGE_EXPORT_STOCK_LIST', exportStocks }

}

export default {
  changeExportStockList
};