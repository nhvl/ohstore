function changeImportReturn(importReturns) {
  return { type: 'CHANGE_IMPORT_RETURN', importReturns }

}

export default {
  changeImportReturn
};