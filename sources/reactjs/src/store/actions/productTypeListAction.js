function changeProductTypeList(productTypes) {
  return { type: 'CHANGE_PRODUCT_TYPE_LIST', productTypes}

}

export default {
  changeProductTypeList
};