function changeDepositList(deposits) {
  return { type: 'CHANGE_DEPOSIT_LIST', deposits }

}

export default {
  changeDepositList
};