import _ from "lodash";
import ExtendFunction from "lib/ExtendFunction";

function ManualSortFilter(data, filter, sort) {
  let _data = data.slice();
  if (filter && Object.keys(filter).length) {
    _data = _data.filter(item => ManualFilter(item, filter ));
  }

  if (sort && sort.sortOrder) {
    _data = _.orderBy(
      _data,
      [(obj) => {
        let arrSortField = sort.sortField.split('.');
        let value = obj;
        for(let key of arrSortField) {
          if(typeof value !== 'object' || value === null) break
          value = value[key];
        }
        
        return value;
      }],
      [sort.sortOrder.toLowerCase()]
    )
  }

  return _data;
}

function ManualFilter(data, filter) {
  let isValid = true;

  for (let key in filter) {
    if (key === "or") {
      let check = false;
      for (let subKey in filter[key]) {
        let field = Object.keys(filter[key][subKey])[0];
        let condition = filter[key][subKey][field];
        let manualFilterBy = ManualFilterBy( data, field, condition );
        check = check || manualFilterBy;
      }
      isValid = isValid && check;
    } else {
      let manualFilterBy = ManualFilterBy(data, key, filter[key]);
      isValid = isValid && manualFilterBy;
    }
  }
  return isValid;
}

function ManualFilterBy(data, field, condition) {
  if (typeof condition === "object") {
    let arrKey = Object.keys(condition);
    if (arrKey.length > 1) {
      let isCheck = true;
      for(let item of arrKey) {
        if (typeof condition[item] === "object") {
          let arrKeyCondition = Object.keys(condition[item]);
          let isCheckCondition = true;

          if (arrKeyCondition.length) {
            for (let elem of arrKeyCondition) {
              let check = ManualFilterBy(data, elem, { [elem]: condition[item][elem] });
              isCheckCondition = isCheckCondition && check;
            }
            isCheck = isCheckCondition && isCheck
          }          
        }
        else {
          let check = ManualFilterBy(data, field, { [item]: condition[item] });
          isCheck = isCheck && check;
        }
      }

      return isCheck
    }

    let key = arrKey[0];
    let _value = condition[key];
    let arrFields = field.split(".");
    let temp = data;
    
    for(let item of arrFields) {
      if(typeof temp !== 'object' || temp === null) return false
      temp = temp[item];
    }

    switch(key) {
      case "<":
        return temp < _value;
      case "<=":
        return temp <= _value;
      case ">":
        return temp > _value;
      case ">=":
        return temp >= _value;
      case "!=":
        return temp !== _value;
      case "in":
        return _value.includes(temp);
      case "nin":
        return !_value.includes(temp);
      case "contains":
        return temp && (ExtendFunction.removeSign(temp)).toLowerCase().includes((ExtendFunction.removeSign(_value)).toLowerCase());
      case "and": {
        let isValid = true;

        for ( let i in _value ) {
          if (typeof _value[i] === "object") {
            let arrKeyAnd = Object.keys(_value[i]);
            if (arrKeyAnd.length) {
              let checkArrKeyAnd = true
              for (let y of arrKeyAnd) {
                let manualFilterBy = ManualFilterBy(data, y, _value[i][y]);
                checkArrKeyAnd = checkArrKeyAnd && manualFilterBy
              }
              isValid = isValid && checkArrKeyAnd
            }
            let manualFilterBy = ManualFilterBy(data, i, _value[i]);
            isValid = isValid && manualFilterBy
          }
          else {
            let manualFilterBy = ManualFilterBy( data, field, _value[i] );
            isValid = isValid && manualFilterBy
          }
        }
        return isValid
      }
      case "or": {
        let isCheckOr = false;

        for (let i in _value) {
          if (typeof _value[i] === "object") {
            let arrKeyOr = Object.keys(_value[i]);
            if(arrKeyOr.length) {
              let checkArrKeyOr = true
              for(let y of arrKeyOr) {
                let manualFilterBy = ManualFilterBy(data, y, _value[i][y]);
                checkArrKeyOr = checkArrKeyOr && manualFilterBy
              }
              isCheckOr = isCheckOr || checkArrKeyOr
            }            
          }
          else {
            let manualFilterBy = ManualFilterBy(data, field, _value[i]);
            isCheckOr = isCheckOr || manualFilterBy
          }
        }
        return isCheckOr
      }
      default:
        return true;
    }
  
  } else {
    let arrFields = field.split(".");
    let temp = data;
    
    for(let item of arrFields) {
      temp = temp && temp[item];
    }

    return temp === condition;
  }
}

function sortArrayObject(data, field, type) {
  switch(type){
    case "asc":
      data = data.sort((a, b) => {
        if (typeof a[field] === "number") {
          return (a[field] - b[field])
        } else if (typeof a[field] === "string") {
          let x = ExtendFunction.removeSign(a[field]).toLowerCase();
          let y = ExtendFunction.removeSign(b[field]).toLowerCase();
          if (x < y) return -1;
          if (x > y) return 1;
          return 0;
        }
        return 0;
      })
      break;
    case "desc":
      data = data.sort((a, b) => {
        if (typeof a[field] === "number") {
          return (b[field] - a[field])
        } else if (typeof a[field] === "string") {
          let x = ExtendFunction.removeSign(a[field]).toLowerCase();
          let y = ExtendFunction.removeSign(b[field]).toLowerCase();
          if (x > y) return -1;
          if (x < y) return 1;
          return 0;
        }
        return 0;
      })
      break;
    default: break;
  }
  return data;
}

export default {
  ManualSortFilter,
  sortArrayObject
}