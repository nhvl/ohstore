import http from "./HttpService";

const apiEndpoint = "/get-return-report";

export function getReturnReportData(datafilter) {
  return http.post(`${apiEndpoint}/`, datafilter);
}

export function getExpandReturnReportData(datafilter) {
  return http.post(`${apiEndpoint}/expand`, datafilter);
}

export function getExpand2ReturnReportData(id, datafilter) {
  return http.post(`${apiEndpoint}/expand/${id}`, datafilter);
}

export default {
  getReturnReportData,
  getExpandReturnReportData,
  getExpand2ReturnReportData
};
