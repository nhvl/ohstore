import http from "./HttpService";

const apiEndpoint = "/get-manufacturing-product-report";

export function getManufacturingReportData(datafilter) {
  return http.post(`${apiEndpoint}/`, datafilter);
}

export default {
  getManufacturingReportData
};