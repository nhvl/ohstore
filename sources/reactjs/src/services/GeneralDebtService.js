import http from "./HttpService";

const apiEndpoint = "/get-general-debt-report";

export function getGeneralDebtReportData(datafilter) {
  return http.post(`${apiEndpoint}/`, datafilter);
}

export function getGeneralDebtExpand(datafilter) {
  return http.post(`${apiEndpoint}/expand`, datafilter);
}

export default {
  getGeneralDebtReportData,
  getGeneralDebtExpand,
};