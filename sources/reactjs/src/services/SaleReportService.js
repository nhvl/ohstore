import http from "./HttpService";

const apiEndpoint = "/get-sale-report";

export function getSaleReportData(datafilter) {
  return http.post(`${apiEndpoint}/`, datafilter);
}

export function getExpandSaleReportData(datafilter) {
  return http.post(`${apiEndpoint}/expand`, datafilter);
}

export function getExpand2SaleReportData(id, datafilter) {
  return http.post(`${apiEndpoint}/expand/${id}`, datafilter);
}

export default {
  getSaleReportData,
  getExpandSaleReportData,
  getExpand2SaleReportData
};
