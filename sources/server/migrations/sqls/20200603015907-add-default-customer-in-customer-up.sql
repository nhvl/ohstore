/* Replace with your SQL commands */
DELETE FROM `customer`
WHERE `name` IN ('Khách lẻ', 'NCC mặc định');

insert into `customer` 
-- các cột
(`createdAt`, `updatedAt`, `name`, `code`, `type`, `branchId`, `address`, `tel`, `fax`, `mobile`, `email`, `gender`, `birthday`, `notes`, `group`, `totalIn`, `totalOut`, `totalOutstanding`, `maxDeptAmount`, `maxDeptDays`, `taxCode`, `totalDeposit`, `initialDeptAmount`, `fix`, `deletedAt`, `province`, `district`, `commune`, `createdBy`, `updatedBy` ) 

values 
-- khách lẻ
((SELECT CAST( 1000*UNIX_TIMESTAMP(current_timestamp(3)) AS UNSIGNED INTEGER)), 
(SELECT CAST( 1000*UNIX_TIMESTAMP(current_timestamp(3)) AS UNSIGNED INTEGER)),
'Khách lẻ',
'KHACH-LE',
'1',
'0',
'','','','','','','0','','','0','0','0','0','0','','0','0','0','0','','','','1','1'
),
-- nhà cung cấp
((SELECT CAST( 1000*UNIX_TIMESTAMP(current_timestamp(3)) AS UNSIGNED INTEGER)), 
(SELECT CAST( 1000*UNIX_TIMESTAMP(current_timestamp(3)) AS UNSIGNED INTEGER)),
'NCC mặc định',
'NCC-MAC-DINH',
'2',
'0',
'','','','','','','0','','','0','0','0','0','0','','0','0','0','0','','','','1','1'
) ;