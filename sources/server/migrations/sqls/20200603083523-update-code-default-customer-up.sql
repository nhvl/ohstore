/* Replace with your SQL commands */

UPDATE `debt`
SET `customerId` = '1'
WHERE `customerId` = (select `id` from `customer` where `code` = 'KHACH-LE');

UPDATE `deposit`
SET `customerId` = '1'
WHERE `customerId` = (select `id` from `customer` where `code` = 'KHACH-LE');

UPDATE `depositcard`
SET `customerId` = '1'
WHERE `customerId` = (select `id` from `customer` where `code` = 'KHACH-LE');

UPDATE `exportcard`
SET `recipientId` = '1'
WHERE `recipientId` = (select `id` from `customer` where `code` = 'KHACH-LE');

UPDATE `importcard`
SET `recipientId` = '1'
WHERE `recipientId` = (select `id` from `customer` where `code` = 'KHACH-LE');

UPDATE `importreturncard`
SET `customerId` = '1'
WHERE `customerId` = (select `id` from `customer` where `code` = 'KHACH-LE');

UPDATE `incomeexpensecard`
SET `customerId` = '1'
WHERE `customerId` = (select `id` from `customer` where `code` = 'KHACH-LE') and `customerType` = '1';

UPDATE `invoice`
SET `customerId` = '1'
WHERE `customerId` = (select `id` from `customer` where `code` = 'KHACH-LE');

UPDATE `ordercard`
SET `customerId` = '1'
WHERE `customerId` = (select `id` from `customer` where `code` = 'KHACH-LE');

UPDATE `customer`
SET `id` = '1'
WHERE `code` = 'KHACH-LE';

ALTER TABLE `customer` DROP COLUMN `customerId`;