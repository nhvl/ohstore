/* Replace with your SQL commands */
INSERT INTO `permission` (`createdAt`,`updatedAt`, `name`, `deletedAt`, `description`, `createdBy`,`updatedBy`) 
  VALUES (
    (SELECT CAST( 1000*UNIX_TIMESTAMP(current_timestamp(3)) AS UNSIGNED INTEGER)),
    (SELECT CAST( 1000*UNIX_TIMESTAMP(current_timestamp(3)) AS UNSIGNED INTEGER)),
    'report_return',
    '0',
    '',
    '1',
    '1'
  );

INSERT INTO `rolepermission` (`createdAt`,`updatedAt`, `type`, `roleId`, `permissionId`, `createdBy`,`updatedBy`) 
  VALUES (
    (SELECT CAST( 1000*UNIX_TIMESTAMP(current_timestamp(3)) AS UNSIGNED INTEGER)),
    (SELECT CAST( 1000*UNIX_TIMESTAMP(current_timestamp(3)) AS UNSIGNED INTEGER)),
    '1', 
    (SELECT `id` FROM `role` where `name` = 'admin'),
    (SELECT `id` FROM `permission` where `name` = 'report_return'),
    '1',
    '1'
  );