/* Replace with your SQL commands */

ALTER TABLE `actionlog` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `archive` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `branch` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `customer` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `debt` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `deposit` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `depositcard` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `exportcard` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `exportcardproduct` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `filestorage` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `importcard` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `importcardproduct` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `importreturncard` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `importreturncardproduct` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `incomeexpensecard` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `incomeexpensecarddetail` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `incomeexpensecardtype` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `invoice` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `invoiceproduct` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `manufacturingcard` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `manufacturingcardfinishedproduct` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `manufacturingcardmaterial` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `manufacturingformula` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `migrations` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `movestockcard` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `movestockcardproduct` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `ordercard` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `ordercardproduct` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `permission` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `product` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `productprice` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `productstock` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `producttype` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `productunit` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `role` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `rolepermission` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `stock` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `stockcheckcard` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `stockcheckcardproduct` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `storeconfig` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `tenants` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;
ALTER TABLE `user` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin;