DROP TABLE `archive`;

DROP TABLE `cashbook`;

DROP TABLE `customer`;

DROP TABLE `debt`;

DROP TABLE `deposit`;

DROP TABLE `depositcard`;

DROP TABLE `exportcard`;

DROP TABLE `exportcardproduct`;

DROP TABLE `filestorage`;

DROP TABLE `importcard`;

DROP TABLE `importcardproduct`;

DROP TABLE `importreturncard`;

DROP TABLE `importreturncardproduct`;

DROP TABLE `incomeexpensecard`;

DROP TABLE `incomeexpensecarddetail`;

DROP TABLE `incomeexpensecardtype`;

DROP TABLE `invoice`;

DROP TABLE `invoiceproduct`;

DROP TABLE `invoicereturn`;

DROP TABLE `invoicereturnproduct`;

DROP TABLE `manufacturingcard`;

DROP TABLE `manufacturingcardfinishedproduct`;

DROP TABLE `manufacturingcardmaterial`;

DROP TABLE `manufacturingformula`;

DROP TABLE `permission`;

DROP TABLE `product`;

DROP TABLE `producttype`;

DROP TABLE `productunit`;

DROP TABLE `role`;

DROP TABLE `rolepermission`;

DROP TABLE `stockcheckcard`;

DROP TABLE `stockcheckcardproduct`;

DROP TABLE `storeconfig`;

DROP TABLE `user`;