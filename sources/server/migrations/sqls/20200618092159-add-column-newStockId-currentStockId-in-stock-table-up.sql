/* Replace with your SQL commands */

ALTER TABLE `stock` ADD (
  `newStockId` double DEFAULT NULL,
  `currentStockId` double DEFAULT NULL
);

set @newStockId = (select id from `stock` where `deletedAt` = 0 limit 1);
update `stock` set newStockId = @newStockId where newStockId is null and deletedAt <> 0;
update `stock` set currentStockId = @newStockId where currentStockId is null and deletedAt <> 0;