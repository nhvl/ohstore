const Datasource = require('mutilTenant/datasource');
const path = require('path');
const fs = require('fs-extra')
const directoryPath = path.join("scripts/ohstore-data-annhienphat");

const csv = require('csvtojson');

const createdBy = 1;
const updatedBy = 1;
const createdAt = new Date().getTime();
const updatedAt = new Date().getTime();
const branchId = 1;
const stockId = 1;
const roleId = 1; // role admin
const defaultProductType = 1; // 1 - sản phẩm, 2 - dịch vụ
const defaultProductCategory = 1; // 1 - nguyên vật liệu, 2 - thành phẩm
const defaultProductQuantity = 0;
let defaultCustomerName = "Khác";

let incomeExpenseType_old = {
  income: 1,
  expense: 0,
  1: "income",
  0: "expense"
}
let incomeExpenseType_new = {
  income: 1,
  expense: 2,
  1: "income",
  2: "expense"
}

var oldFixCostTypes = {
  INVOICE: 1,
  IMPORT: 2,
  IMPORT_RETURN: 3,
  INVOICE_RETURN: 4,
  OTHER_INCOME: 5,
  OTHER_EXPENSE: 6
}
var newFixCostTypes = {
  INVOICE: 1, // type: 1 - thu, 2 - chi
  IMPORT_RETURN: 2,
  OTHER_INCOME: 3,
  IMPORT: 4,
  INVOICE_RETURN: 5,
  OTHER_EXPENSE: 6,
}

module.exports = {
  friendlyName: 'convert old ohstore to new one',
  description: '',

  fn: async function (inputs, exits) {
    
    let datasource;
    try {
      datasource = await sails.config.multitenancy(process.env.DB_IDENTITY);
    } catch(err) {
      return exits.success(err);
    }
    
    const files = await fs.readdir(directoryPath);
    let currentTime = new Date().getTime();
    let data = {};
    (await Promise.all(files.map(item => csv().fromFile(directoryPath + '/' + item)))).map((item, index) => {
      data[files[index].split('.').slice(0, -1).join('.')] = item.map(i => _.pickBy(i, value => value !== 'NULL'));
    })
    
    // update note import export card
    await Promise.all(data.ExportCard.map(item => {
      let {ID,Code,CustomerID,SaleID,Time,Amount,Notes,QtyIn,QtyOutstanding,BillID,IsReturn} = item;
      return sails.sendNativeQuery(datasource, `update invoice set notes = "${Notes}" where code = "${Code}"`);
    }));
    
    return exits.success({
      status: true
    });
  }
};

