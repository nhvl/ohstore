

module.exports.newTrialUser = function (data) {
  let {
    mobile,
  } = data;
  
  let html = '<p>Có khách hàng mới dùng thử Demo Ohstore. Thông tin chi tiết:</p>' +
    '<p>Số điện thoại:<b> &nbsp;' + mobile + '<b></p>'
  return { subject: 'Ohstore - Khách hàng mới dùng thử demo', html: html };
}
