/**
 * Kiểm tra có mã bảo mật
 */

module.exports = function () {
  return async function (req, res, proceed) {
    
    if (req.header("private-key") === process.env.private_key) {
      return proceed()
    }
    
    return res.send({ status: false, message: sails.__("Kết nối thất bại") });
  }
}