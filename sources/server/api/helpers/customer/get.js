module.exports = {
  description: 'Get customer info',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: 'ref',
      required: true,
    },
  },

  fn: async function (inputs, exits) {
    let { id, type, branchId } = inputs.data;
    let { req } = inputs;    

    let foundCustomer = await Customer.findOne(req, {
      where: {id, type}
    }).intercept({ name: 'UsageError' }, ()=>{
      return exits.success({status: false, message: sails.__(sails.config.constant.INTERCEPT.UsageError)});
    });
    
    if (!foundCustomer) {
      return exits.success({status: false, message: sails.__('Khách hàng không tồn tại trong hệ thống')});
    }
    let checkBanch = await sails.helpers.checkBranch(foundCustomer.branchId, req);

    if(!checkBanch){
      return exits.success({status: false, message: sails.__('Không có quyền thực hiện thao tác này')});
    }

    return exits.success({ status: true, data: foundCustomer });
  }

}