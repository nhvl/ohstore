module.exports = {
  description: 'Get list customer',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: 'ref',
      required: true,
    },
  },

  fn: async function (inputs, exits) {
    let { filter, sort, limit, skip, manualFilter, manualSort, select, isBranch, branchId, type, isNotGetDefault } = inputs.data;
    let { req } = inputs;

    filter = _.extend(filter || {}, { deletedAt: 0, type: type, branchId: isBranch ? undefined : {in: isNotGetDefault ? [branchId] : [branchId, 0] } });

    let options = {
      select: select || [
        '*'
      ],
      model: Customer, 
      filter: {...filter, ...manualFilter },
      limit,
      skip,
      sort: sort || 'createdAt DESC',
      count: true
    };

    let {foundData, count} = await sails.helpers.customSendNativeQuery(req, options);
    
    return exits.success({ data: foundData, status: true, count: count});
  }

}