const moment = require("moment");
module.exports = {

  friendlyName: 'Import file',

  description: 'Importfile',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    },
  },

  fn: async function (inputs, exits) {
    let {
      card,
      product,
      payment,
      stopOnCodeDuplicateError,
      branchId,
      createdBy
    } = inputs.data;

    let {req} = inputs;
    let arrayError = [];

    card = JSON.parse(card);
    product = JSON.parse(product);
    payment = JSON.parse(payment);

    // Kiểm tra mã phiếu có bị trùng
    if(_.uniqBy(card, 'code').length !== card.length)
      return exits.success({ status: false, message: sails.__("Mã phiếu nhập không được trùng nhau") });

    // Lọc sản phẩm có số lượng > 0
    product = _.filter(product, item => +item.quantity > 0);

    // Lọc phiếu thanh toán có giá trị > 0
    payment = _.filter(payment, item => +item.paid > 0);

    // Lọc phiếu có chứa sản phẩm
    let paymentCard = [];
    for(let i = 0; i<card.length; i++){
      if(!(_.some(product, {code: card[i].code}))){
        card.splice(i, 1);
        i--;
      }
      else{
        paymentCard = paymentCard.concat(_.filter(payment, item => item.code === card[i].code))
      }
    }

    // Lọc sản phẩm có trong phiếu
    for(let i = 0; i<product.length; i++){
      if(!(_.some(card, {code: product[i].code}))){
        product.splice(i, 1);
        i--;
      }
    }

    // Kiểm tra sản phẩm giống nhau có đơn vị tính và nhóm giống nhau không, nếu không thì báo lỗi
    let groupProductCode = _.groupBy(product, 'productCode');
    for(let productCode in groupProductCode){
      if(groupProductCode[productCode].length > 1){
        for(let index in groupProductCode[productCode]){
          if(index + 1 < groupProductCode[productCode].length){
            if(groupProductCode[productCode][index].productUnit !== groupProductCode[productCode][+index +1].productUnit || groupProductCode[productCode][index].productType !== groupProductCode[productCode][+index +1].productType){
              return exits.success({ status: false, message: sails.__("Thông tin sản phẩm giống nhau phải trùng khớp với nhau") });
            }
          }
          else
            break;
        }
      }
      else
       break;
    }
    
    // Kiểm tra kho, nhà cung cấp, đơn vị tính, nhóm sản phẩm có tồn tại không, nếu không thì thêm mới, và số lượng kho không được quá 6 kho
    // Tìm các kho mới, nhà cung cấp mới
    let groupStock = _.keys(_.groupBy(product, 'stockName'));
    let groupSupplier = _.keys(_.groupBy(card, 'customerCode'));
    let groupProduct = _.keys(groupProductCode);
    let groupProductUnit = _.keys(_.groupBy(product, 'productUnit'));
    let groupProductType = _.keys(_.groupBy(product, 'productType'));

    if(groupStock.length > 6){
      return exits.success({ status: false, message: sails.__("Số kho tạo mới quá lớn, không thể vượt quá tổng số kho tối đa là 6")});
    }
    let [stocks, foundImportCard, suppliers, products, productUnits, productTypes] = await Promise.all([
      Promise.all(_.map(groupStock, item => {
        return Stock.findOne(req, {name: item, branchId})
      })),
      Promise.all(_.map(card, item => {
        return ImportCard.findOne(req, {code: item.code}).populate('recipientId');
      })),
      Promise.all(_.map(groupSupplier, item => {
        return Customer.findOne(req, {code: item, branchId, type: sails.config.constant.CUSTOMER_TYPE.TYPE_SUPPLIER});
      })),
      Promise.all(_.map(groupProduct, item => {
        return Product.findOne(req, {code: item});
      })),
      Promise.all(_.map(groupProductUnit, item => {
        return ProductUnit.findOne(req, {name: item});
      })),
      Promise.all(_.map(groupProductType, item => {
        return ProductType.findOne(req, {name: item});
      })),
    ]) 
    
    let emptyStock = _.keys(_.pickBy(stocks, _.isUndefined));
    let emptySupplier = _.keys(_.pickBy(suppliers, _.isUndefined));
    let emptyProduct = _.keys(_.pickBy(products, _.isUndefined));
    let emptyProductUnit = _.keys(_.pickBy(productUnits, _.isUndefined));
    let emptyProductType = _.keys(_.pickBy(productTypes, _.isUndefined));

    // Kiểm tra tiền tố của mã sản phẩm mới
    let error = false;
    for(let item of emptyProduct){
      let checkExistPrefix = sails.helpers.common.checkPrefixCode(sails.config.cardcode.product, groupProduct[item]);
      if(!checkExistPrefix.status){
        return exits.success({ status: false, message: sails.__('Mã sản phẩm mới không được có tiền tố SP') });
      }
    }

    // Kiểm tra tiền tố của nhà cung cấp mới
    for(let item of emptySupplier){
      let checkExistPrefix = sails.helpers.common.checkPrefixCode(sails.config.cardcode.providerFirstCode, groupSupplier[item]);
      if(!checkExistPrefix.status){
        return exits.success({ status: false, message: sails.__('Mã nhà cung cấp mới không được có tiền tố NCC') });
      }
      if(groupSupplier[item] === sails.config.constant.DEFAULT_CUSTOMERS.SUPPLIER.code)
        return exits.success({ status: false, message: sails.__('Không được tạo NCC mặc định') });
    }

    let listStocks = await sails.helpers.stockList.list(req, {filter: {branchId: branchId}})
    
    if(listStocks.count + emptyStock.length > 6){
      return exits.success({ status: false, message: sails.__(`Số kho hiện tại là ${listStocks.count}, không thể tạo mới vượt quá 6 kho`)});
    }

    // Lọc các phiếu nhập trùng
    let indexesDupplicateCard = _.keys(_.pickBy(foundImportCard, _.isObject));
    let indexesNewCard = _.keys(_.pickBy(foundImportCard, _.isUndefined));

    // dừng import nếu bị trùng mã phiếu nhập
    if(stopOnCodeDuplicateError && indexesDupplicateCard.length){
      _.forEach(indexesDupplicateCard, item => {
        arrayError.push(card[item])
      })
      return exits.success({ status: false, message: sails.__("Trùng mã phiếu nhập"), arrayError });
    }

    // Kiểm tra nhà cung cấp của các mã phiếu trùng có trùng khớp không, nếu không thì báo lỗi
    for(let index in indexesDupplicateCard){
      if(foundImportCard[index].recipientId.code !== card[index].customerCode){
        return exits.success({ status: false, message: sails.__(`Không được thay đổi mã nhà cung cấp của phiếu đã tồn tại`)});
      }
    }
    

    // Tạo mới các kho, nhà cung cấp, sản phẩm, đơn vị tính, nhóm sản phẩm
    let [createStock, createSupplier, createProduct, createProductUnit, createProductType] = await Promise.all([
      Promise.all(_.map(emptyStock, item => {
        return sails.helpers.stockList.create(req, {
          name: groupStock[item],
          branchId,
          createdBy,
        })
      })),
      Promise.all(_.map(emptySupplier, item => {
        return sails.helpers.customer.create(req, {
          name: groupSupplier[item],
          code: groupSupplier[item],
          type: sails.config.constant.CUSTOMER_TYPE.TYPE_SUPPLIER,
          branchId: branchId,
          createdBy,
          updatedBy: createdBy
        })
      })),
      Promise.all(_.map(emptyProduct, item => {
        return sails.helpers.product.create(req, {
          name: groupProduct[item],
          code: groupProduct[item],
          createdBy,
          branchId,
          type: sails.config.constant.PRODUCT_TYPES.merchandise   //Hàng hóa
        })
      })),
      Promise.all(_.map(emptyProductUnit, item => {
        return ProductUnit.create(req, {
          name: groupProductUnit[item],
          createdBy,
          updatedBy: createdBy,
        })
      })),
      Promise.all(_.map(emptyProductType, item => {
        return ProductType.create(req, {
          name: groupProductType[item],
          createdBy,
          updatedBy: createdBy,
        })
      })),
    ])

    // Lấy danh sách nhà cung cấp, kho, sản phẩm, đơn vị tính, nhóm sản phẩm
    let [foundSupplier, foundStock, foundProduct, foundProductUnit, foundProductType] = await Promise.all([
      Promise.all(_.map(card, item => {
        return Customer.findOne(req, {code: item.customerCode});
      })),
      Promise.all(_.map(product, item => {
        return Stock.findOne(req, {name: item.stockName, branchId});
      })),
      Promise.all(_.map(product, item => {
        return Product.findOne(req, {code: item.productCode});
      })),
      Promise.all(_.map(product, item => {
        return ProductUnit.findOne(req, {name: item.productUnit});
      })),
      Promise.all(_.map(product, item => {
        return ProductType.findOne(req, {name: item.productType});
      })),
    ])

    // Cập nhật đơn vị tính, nhóm cho sản phẩm
    let updateProduct = await Promise.all(_.map(foundProduct, async(item, index) =>{
      return Product.update(req, {id: item.id}).set({
        unitId: foundProductUnit[index].id,
        productTypeId: foundProductType[index].id,
        updatedBy: createdBy,
      })
    }))

    // Lấy thông tin sản phẩm
    let getProduct = await Promise.all(_.map(foundProduct, item => {
      return sails.helpers.product.get(req, {productId: item.id, branchId});
    }))

    // Gán sản phẩm vào phiếu
    _.forEach(card, item => {
      item.totalAmount = 0;
      item.products = [];
      _.forEach(product, (elm, index) => {
        if(item.code === elm.code){
          item.products.push({
            productCode: getProduct[index].data.code,
            productName: getProduct[index].data.name,
            quantity: +elm.quantity || 0,
            discount: getProduct[index].data.lastImportPrice - (+elm.importPrice || 0),
            finalAmount: +elm.importPrice || 0,
            unitPrice: getProduct[index].data.lastImportPrice,
            productId: getProduct[index].data.id,
            createdBy: createdBy,
            updatedBy: createdBy,
            stockId: foundStock[index].id
          })
          item.totalAmount += ((+elm.importPrice * +elm.quantity) || 0);
        }
      })
    })

    // Kiểm tra thanh toán có lớn hơn giá trị phiếu nhập
    let errorExpense = [];
    let payCard = _.groupBy(payment, 'code');
    _.forEach(payCard, item => {
      let getCard = _.find(card, value => value.code === item[0].code);
      let totalExpense = 0;
      _.forEach(item, elm => {
        totalExpense += +elm.paid;
      })
      if(totalExpense > getCard.totalAmount){
        errorExpense.push({code: getCard.code})
      }
    })

    if(errorExpense.length){
      return exits.success({ status: false, message: sails.__("Số tiền thanh toán lớn hơn tổng giá trị của phiếu nhập"), arrayError : errorExpense});
    }

    //cập nhật lại phiếu nếu bị trùng mã nhập
    if(indexesDupplicateCard.length && !stopOnCodeDuplicateError){
      // hủy các phiếu chi liên quan đến các phiếu nhập
      let expenseCard = await Promise.all(_.map(indexesDupplicateCard, item => {
        return sails.helpers.income.getCardDetail(req, {
          cardId: foundImportCard[item].id,
          incomeExpenseCardTypeCode: sails.config.constant.DEFAULT_INCOME_EXPENSE_CARD_TYPES.IMPORT.code
        })
      }))

      let arrayCancel = [];
      _.forEach(expenseCard, item => {
        _.forEach(item.data.incomeExpenseCardDetail, elm => {
          arrayCancel.push(elm.incomeExpenseCardId)
        }) 
      })

      for(let item of arrayCancel){
        await sails.helpers.expense.cancel(req, {
          id: item.id,
          updatedBy: createdBy,
          branchId
        })
      }

      // cập nhật lại phiếu nhập
      await Promise.all(_.map(indexesDupplicateCard, item => {
        return sails.helpers.import.update(req, {
          id: foundImportCard[item].id,
          code: card[item].code,
          products: card[item].products,
          finalAmount: card[item].totalAmount - (+card[item].discount) + (+card[item].tax) + (+card[item].delivery),
          totalAmount: card[item].totalAmount,
          importedAt: moment(card[item].importedAt, "DD/MM/YYYY").startOf('day').valueOf() || new Date().getTime(),
          reason: sails.config.constant.IMPORT_CARD_REASON.IMPORT_PROVIDER,
          recipientId: foundSupplier[item].id,
          updatedBy: createdBy,
          paidAmount: 0,
          discountAmount: +card[item].discount,
          taxAmount: +card[item].tax,
          deliveryAmount: +card[item].delivery,
          branchId,
        })
      }))
    }
    
    // Tạo phiếu nhập
    let createImport = await Promise.all(_.map(indexesNewCard, (item) => {
      return sails.helpers.import.create(req, {
        code: card[item].code,
        products: card[item].products,
        finalAmount: card[item].totalAmount - (+card[item].discount) + (+card[item].tax) + (+card[item].delivery),
        totalAmount: card[item].totalAmount,
        importedAt: moment(card[item].importedAt, "DD/MM/YYYY").startOf('day').valueOf() || new Date().getTime(),
        reason: sails.config.constant.IMPORT_CARD_REASON.IMPORT_PROVIDER,
        recipientId: foundSupplier[item].id,
        createdBy,
        discountAmount: +card[item].discount,
        taxAmount: +card[item].tax,
        deliveryAmount: +card[item].delivery,
        branchId,
      })
    }));

    // Lấy danh sách phiếu cần chi, loại chi
    let [foundCard, incomeExpenseCardType] = await Promise.all([
      Promise.all(_.map(payment, item => { 
        return ImportCard.findOne(req, {code: item.code})
      })),
      IncomeExpenseCardType.findOne(req, {code: sails.config.constant.DEFAULT_INCOME_EXPENSE_CARD_TYPES.IMPORT.code})
    ]);

    // Tạo phiếu chi
    for (let index in payment) {
      let createExpense = await sails.helpers.expense.create(req, {
        customerId: foundCard[index].recipientId, 
        customerType: sails.config.constant.CUSTOMER_TYPE.TYPE_SUPPLIER,
        incomeExpenseAt: moment(payment[index].expenseAt, "DD/MM/YYYY").startOf('day').valueOf() || new Date().getTime(),
        incomeExpenseCardTypeId: incomeExpenseCardType.id,
        incomeExpenseCardTypeCode: incomeExpenseCardType.code,
        paymentDetail: [{
          cardId: foundCard[index].id,
          payAmount: +payment[index].paid
        }],
        amount: +payment[index].paid,
        createdBy,
        branchId,
      })
    }

    return exits.success({status: true});
  }
}