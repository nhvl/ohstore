module.exports = {
  description: 'get import card',
  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    },
  },
  fn: async function (inputs, exits) {
    let { id , branchId} = inputs.data;

    let {req} = inputs;

    let foundImportCard = await ImportCard.findOne(req, {
      where: { id: id }
    }).populate("recipientId")
      .intercept({ name: 'UsageError' }, () => {
        return exits.success({ status: false, message: sails.__(sails.config.constant.INTERCEPT.UsageError) });
      })
    if (!foundImportCard) {
      return exits.success({ status: false, message: sails.__(sails.config.constant.INTERCEPT.NOT_FOUND_IMPORT_CARD) });
    }

    let checkBanch = await sails.helpers.checkBranch(foundImportCard.branchId, req);

    if(!checkBanch){
      return exits.success({status: false, message: sails.__('Không có quyền thực hiện thao tác này')});
    }

    let [importProductArray, createdBy, foundUnit, foundExpenseDetail, importReturns, productStock] = await Promise.all([
      ImportCardProduct.find(req, { importCardId: foundImportCard.id }).populate("productId"),
      User.findOne(req, { where: { id: foundImportCard.createdBy }, select: ["id", "fullName"] }),
      ProductUnit.find(req, { deletedAt: 0 }),
      sails.helpers.income.getCardDetail(req, { cardId: foundImportCard.id, incomeExpenseCardTypeCode: sails.config.constant.DEFAULT_INCOME_EXPENSE_CARD_TYPES.IMPORT.code }),
      sails.helpers.exportCard.list(req, { filter: { reference: foundImportCard.code, status: sails.config.constant.EXPORT_CARD_STATUS.FINISHED }, type: sails.config.constant.CUSTOMER_TYPE.TYPE_SUPPLIER }),
      ProductStock.find(req, {branchId})
    ])

    if (createdBy) foundImportCard.createdBy = createdBy;

    let productStocks = {};

    _.forEach(productStock, item => productStocks[item.productId] = item)

    const stocks = Object.values(sails.config.constant.STOCK_QUANTITY_LIST);
    for(let elem of importProductArray){
      stocks.map(stock => {
        if (productStocks[elem.productId.id]) {
          elem.productId[stock] = productStocks[elem.productId.id][stock];
        } else {
          elem.productId[stock] = 0;
        }
      });
      for(let item of foundUnit) {
        if (item.productId && item.id === elem.productId.unitId) {
          elem.unit = item.name;
          break;
        }
      }
    }

    let expenseCards = [];
    if (foundExpenseDetail.status) {
      expenseCards = foundExpenseDetail.data.incomeExpenseCardDetail
    }
    else {
      return exits.success({ status: false, message: sails.__(sails.config.constant.INTERCEPT.UsageError) });
    }

    if(!importReturns.status) {
      return exits.success({status: false, message: sails.__(sails.config.constant.INTERCEPT.UsageError)});
    }
    importReturns = importReturns.data;    

    return exits.success({ status: true, data: foundImportCard, importProductArray, expenseCards, importReturns });
  }

}