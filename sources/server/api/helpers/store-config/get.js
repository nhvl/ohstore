module.exports = {
  description: 'Lấy thông tin cửa hàng',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    },
  },

  fn: async function (inputs, exits) {
    let {req, data} = inputs;
    let {types} = data;
    
    let foundConfig = await StoreConfig.find(req, {
      where: { type: { in: types } }
    }).intercept({ name: 'UsageError' }, () => {
      return exits.success({
        status: false,
        error: sails.__('Thông tin không tồn tại trong hệ thống')
      });
    });
    if (!foundConfig) {
      return exits.success({
        status: false,
        error: sails.__('Thông tin không tồn tại trong hệ thống')
      });
    }
    
    let _foundConfig = {}
    
    foundConfig.forEach(item => {
      _foundConfig[item.type] = item.value;
    })
    
    return exits.success({status: true, data: _foundConfig})
  }

}