module.exports = {
  description: 'Get user info',

  inputs: {
    data: {
      type: 'ref',
      required: true,
    },
  },

  fn: async function (inputs, exits) {
    let { 
      phoneNumber
    } = inputs.data;
    
    // kiểm tra số điện thoại có hợp lệ
    if(!_.isPhoneNumber(phoneNumber)) {
      return exits.success({
        status: false,
        message: sails.__('Số điện thoại không hợp lệ')
      });
    }
    
    let foundUser = await TrialUser.find({phoneNumber});
    foundUser = foundUser[0];
    if(!foundUser) {
      foundUser = await TrialUser.create({phoneNumber});

      // Gửi email cho bộ phận SALE
      let contentSale = sails.config.newTrialUser({
        mobile: phoneNumber,
      });

      let sendMailSale = await sails.helpers.common.sendMail({ from: process.env.SUPPORT_EMAIL, to: process.env.REGISTRATION_RECEIVE, subject: contentSale.subject, html: contentSale.html })
      if(!sendMailSale.status) {
        return exits.success({
          status: false,
          message: sails.__('Gửi email về CSKH thất bại.'),
          data: sendMailSale.data
        })
      }
    }
    
    return exits.success({
      status: true,
      data: foundUser,
    });
  }

}