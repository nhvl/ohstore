module.exports = {
  description: 'get import export report',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    },    
  },

  fn: async function (inputs, exits) {
    let { startDate, endDate, customerId, branchId } = inputs.data;
    let { req } = inputs;

    let foundCustomer = await Customer.findOne(req, {id: customerId, branchId});
    let typeCustomer = foundCustomer.type;
    let SQL_CARD = ``;
    let SQL_RETURN_CARD = ``;
    let SQL_INC_EXP = ``;
    // Nếu là khách hàng thì lấy đơn hàng, trả hàng
    if(typeCustomer === sails.config.constant.CUSTOMER_TYPE.TYPE_CUSTOMER){
      // Tìm đơn hàng
      SQL_CARD = `SELECT "Mua hàng" as name, id, code, invoiceAt as time, finalAmount, createdBy
                              FROM invoice
                              WHERE customerId = ${customerId} 
                                AND invoiceAt BETWEEN ${startDate} AND ${endDate} 
                                AND status = ${sails.config.constant.INVOICE_CARD_STATUS.FINISHED} 
                                AND branchId = ${branchId}`;
      // Tìm trả hàng
      SQL_RETURN_CARD = `SELECT "Trả hàng" as name, id, code, importedAt as time, (finalAmount * (-1)) as finalAmount, createdBy
                              FROM importcard
                              WHERE recipientId = ${customerId} 
                                AND importedAt BETWEEN ${startDate} AND ${endDate} 
                                AND status = ${sails.config.constant.IMPORT_CARD_STATUS.FINISHED} 
                                AND reason = ${sails.config.constant.IMPORT_CARD_REASON.INVOICE_RETURN}
                                AND branchId = ${branchId}`;
      // Tìm phiếu thu chi
      SQL_INC_EXP = `SELECT IF(i.type=1, "Phiếu thu", "Phiếu chi") as name, i.id, i.code, incomeExpenseAt as time, IF(i.type=2, amount * (-1), amount) as paidAmount, i.createdBy
                        FROM incomeexpensecard i
                        LEFT JOIN incomeexpensecardtype t ON t.id = i.incomeExpenseCardTypeId
                        WHERE i.customerId = ${customerId} 
                          AND i.incomeExpenseAt BETWEEN ${startDate} AND ${endDate} 
                          AND i.customerType = ${sails.config.constant.INCOME_EXPENSE_CUSTOMER_TYPES.CUSTOMER} 
                          AND i.status = ${sails.config.constant.INCOME_EXPENSE_CARD_STATUS.FINISHED}
                          AND i.branchId = ${branchId} 
                          AND t.shouldUpdateDebt = ${sails.config.constant.IS_UPDATE_DEBT.YES}`;
    }

    // Nếu là nhà cung cấp thì lấy phiếu nhập hàng, trả hàng nhập
    else{
      // Tìm đơn hàng
      SQL_CARD = `SELECT "Nhập hàng" as name, id, code, importedAt as time, finalAmount, createdBy
                              FROM importcard
                              WHERE recipientId = ${customerId} 
                                AND importedAt BETWEEN ${startDate} AND ${endDate} 
                                AND status = ${sails.config.constant.INVOICE_CARD_STATUS.FINISHED} 
                                AND reason = ${sails.config.constant.IMPORT_CARD_REASON.IMPORT_PROVIDER}
                                AND branchId = ${branchId}`;
      // Tìm trả hàng
      SQL_RETURN_CARD = `SELECT "Trả hàng nhập" as name, id, code, exportedAt as time, (finalAmount * (-1)) as finalAmount, createdBy
                              FROM exportcard
                              WHERE recipientId = ${customerId} 
                                AND exportedAt BETWEEN ${startDate} AND ${endDate} 
                                AND status = ${sails.config.constant.EXPORT_CARD_STATUS.FINISHED} 
                                AND reason = ${sails.config.constant.EXPORT_CARD_REASON.RETURN_PROVIDER}
                                AND branchId = ${branchId}`;
      // Tìm phiếu thu chi
      SQL_INC_EXP = `SELECT IF(i.type=1, "Phiếu thu", "Phiếu chi") as name, i.id, i.code, i.incomeExpenseAt as time, IF(i.type=1, amount * (-1), amount) as paidAmount, i.createdBy
                        FROM incomeexpensecard i
                        LEFT JOIN incomeexpensecardtype t ON t.id = i.incomeExpenseCardTypeId
                        WHERE i.customerId = ${customerId} 
                          AND i.incomeExpenseAt BETWEEN ${startDate} AND ${endDate} 
                          AND i.customerType = ${sails.config.constant.INCOME_EXPENSE_CUSTOMER_TYPES.SUPPLIER} 
                          AND i.status = ${sails.config.constant.INCOME_EXPENSE_CARD_STATUS.FINISHED}
                          AND i.branchId = ${branchId} 
                          AND t.shouldUpdateDebt = ${sails.config.constant.IS_UPDATE_DEBT.YES}`;
    }

    // Tìm công nợ do người dùng điều chỉnh
    let SQL_CHANGE_DEBT = `SELECT "Người dùng điều chỉnh" as name, changeValue as finalAmount, createdBy
                            FROM debt
                            WHERE customerId = ${customerId} AND createdAt BETWEEN ${startDate} AND ${endDate} AND type = ${sails.config.constant.DEBT_TYPES.USER_CREATE}`;

    let [card, returnCard, changeDebt, incomeExpense] = await Promise.all([
      sails.sendNativeQuery(req, SQL_CARD),
      sails.sendNativeQuery(req, SQL_RETURN_CARD),
      sails.sendNativeQuery(req, SQL_CHANGE_DEBT),
      sails.sendNativeQuery(req, SQL_INC_EXP),
    ])

    let arrData = [
      ...card.rows, 
      ...returnCard.rows, 
      ...changeDebt.rows,
      ...incomeExpense.rows,
    ];

    let foundUser = await Promise.all(_.map(arrData, item => {
      return User.findOne(req, { where: { id: item.createdBy }, select: ["fullName"] })
    }));

    _.forEach(foundUser, (item, index) => {
      arrData[index].createdName = item.fullName;
    })
    
    return exits.success({ status: true, data: arrData })
  }
}