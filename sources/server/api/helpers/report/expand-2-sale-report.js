module.exports = {
  description: 'list product on report',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    }
  },

  fn: async function (inputs, exits) {
    let { isReturn, branchId, id } = inputs.data;
    let { req } = inputs;
    let foundProduct = [];

    if (isReturn) {
      let foundInvoiceReturn = await ImportCard.findOne(req, {id, branchId}).intercept({ name: 'UsageError' }, () => {
        return exits.success({status: false, message: sails.__(sails.config.constant.INTERCEPT.UsageError)});
      })

      if (!foundInvoiceReturn) {
        return exits.success({message: sails.__("Không tìm thấy phiếu trả hàng"), status: false})
      }

      let foundProductInvoiceReturn = await ImportCardProduct.find(req, {importCardId: id});

      _.forEach(foundProductInvoiceReturn, item => {
        if(foundInvoiceReturn.totalAmount !== 0) {
          item.discount = (item.discount + foundInvoiceReturn.discountAmount/foundInvoiceReturn.totalAmount*item.finalAmount)*item.quantity;
          item.finalAmount = (item.finalAmount - foundInvoiceReturn.discountAmount/foundInvoiceReturn.totalAmount*item.finalAmount)*item.quantity;
        }
        else {
          item.discount = item.discount*item.quantity;
          item.finalAmount = item.finalAmount*item.quantity;
        }      
      });

      foundProduct = foundProductInvoiceReturn.filter(item => item.quantity !== 0);
    }
    else {
      let foundInvoice = await Invoice.findOne(req, {id, branchId}).intercept({ name: 'UsageError' }, () => {
        return exits.success({status: false, message: sails.__(sails.config.constant.INTERCEPT.UsageError)});
      })

      if (!foundInvoice) {
        return exits.success({message: sails.__("Không tìm thấy phiếu hóa đơn"), status: false})
      }

      let foundProductInvoice = await InvoiceProduct.find(req, {invoiceId: id});

      _.forEach(foundProductInvoice, item => {
        if(foundInvoice.totalAmount !== 0) {
          item.discount = item.discount*item.quantity + foundInvoice.discountAmount/foundInvoice.totalAmount*item.finalAmount;
          item.taxAmount = item.taxAmount*item.quantity + foundInvoice.taxAmount/foundInvoice.totalAmount*item.finalAmount;
          item.finalAmount = item.finalAmount + (-foundInvoice.discountAmount/foundInvoice.totalAmount + foundInvoice.taxAmount/foundInvoice.totalAmount)*item.finalAmount;
        }
        else {
          item.discount = item.discount*item.quantity;
        }      
      });

      foundProduct = foundProductInvoice;
    }

    return exits.success({status: true, data: foundProduct})
  }
}