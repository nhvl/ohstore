module.exports = {
  description: 'list invoice cards',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    }
  },

  fn: async function (inputs, exits) {
    let { filterInvoice, filterInvoiceReturn, options, branchId} = inputs.data;

    let { req } = inputs;

    let foundData = [];
    let count = 0;

    switch(options) {
      case sails.config.constant.OPTIONS_SALE_REPORT.HOUR:
      case sails.config.constant.OPTIONS_SALE_REPORT.DAY:
      case sails.config.constant.OPTIONS_SALE_REPORT.MONTH:
      case sails.config.constant.OPTIONS_SALE_REPORT.YEAR:      
      case sails.config.constant.OPTIONS_SALE_REPORT.CUSTOMER:
        filterInvoice = _.extend(filterInvoice || {}, { deletedAt: 0, branchId, status: sails.config.constant.INVOICE_CARD_STATUS.FINISHED });

        let optionInvoices = {
          select: ['*'],
          model: Invoice, 
          filter: {...filterInvoice },
          sort: 'invoiceAt DESC',
        };

        filterInvoiceReturn = _.extend(filterInvoiceReturn || {}, { deletedAt: 0, branchId, reason: sails.config.constant.IMPORT_CARD_REASON.INVOICE_RETURN, status: sails.config.constant.IMPORT_CARD_STATUS.FINISHED });

        let optionInvoiceReturns = {
          select: ['*'],
          model: ImportCard, 
          filter: {...filterInvoiceReturn },
          sort: 'importedAt DESC',
        };

        let [foundDataInvoice, foundDataInvoiceReturn] = 
          await Promise.all([sails.helpers.customSendNativeQuery(req, optionInvoices), sails.helpers.customSendNativeQuery(req, optionInvoiceReturns)]);

        let foundInvoiceReturn = _.map(foundDataInvoiceReturn.foundData, item => ({ ...item, isReturn: true, time: item.importedAt }));
        let foundInvoice = _.map(foundDataInvoice.foundData, item => ({ ...item, isReturn: false, time: item.invoiceAt }))
        
        foundData = foundInvoice.concat(foundInvoiceReturn).sort((a,b) => b.time - a.time);

        count = foundData.length;

        break;
      case sails.config.constant.OPTIONS_SALE_REPORT.USER:
        filterInvoice = _.extend(filterInvoice || {}, { deletedAt: 0, branchId, status: sails.config.constant.INVOICE_CARD_STATUS.FINISHED });

        let optionInvoiceUsers = {
          select: ['*'],
          model: Invoice, 
          filter: {...filterInvoice },
          sort: 'invoiceAt DESC',
        };

        let filterInvoiceReturnNoCard = _.extend(filterInvoiceReturn || {}, { deletedAt: 0, branchId, reason: sails.config.constant.IMPORT_CARD_REASON.INVOICE_RETURN, status: sails.config.constant.IMPORT_CARD_STATUS.FINISHED, reference: "" });
        let filterInvoiceReturnByCard = _.extend(filterInvoiceReturn || {}, { deletedAt: 0, branchId, reason: sails.config.constant.IMPORT_CARD_REASON.INVOICE_RETURN, status: sails.config.constant.IMPORT_CARD_STATUS.FINISHED, ["invoice.createBy"]: filterInvoiceReturn.createBy });
        delete filterInvoiceReturnByCard.createBy;
        
        let optionInvoiceReturnUsers = {
          select: ['*'],
          model: ImportCard, 
          filter: { ...filterInvoiceReturn },
          customPopulates: `left join invoice on invoice.code = m.reference`,
          sort: 'importedAt DESC',
        };

        let optionInvoiceReturnUserNoCards = {
          select: ['*'],
          model: ImportCard, 
          filter: { ...filterInvoiceReturnNoCard },
          sort: 'importedAt DESC',
        };

        let [dataInvoice, dataInvoiceReturn, dataInvoiceReturnNoCard] = await Promise.all([
          sails.helpers.customSendNativeQuery(req, optionInvoiceUsers), 
          sails.helpers.customSendNativeQuery(req, optionInvoiceReturnUsers),
          sails.helpers.customSendNativeQuery(req, optionInvoiceReturnUserNoCards)
        ]);

        let foundInvoiceUser = _.map(dataInvoice.foundData, item => ({ ...item, isReturn: false, time: item.invoiceAt }));
        let foundInvoiceReturnUser = _.map(dataInvoiceReturn.foundData, item => ({ ...item, isReturn: true, time: item.importedAt }))
        let foundInvoiceReturnUserNoCard = _.map(dataInvoiceReturn.foundData, item => ({ ...item, isReturn: true, time: item.importedAt }))

        foundData = foundInvoiceUser.concat(foundInvoiceReturnUser).concat(foundInvoiceReturnUserNoCard).sort((a,b) => b.time - a.time);
        count = foundData.length;
        
        break;
      case sails.config.constant.OPTIONS_SALE_REPORT.PRODUCT:
        filterInvoice = _.extend(filterInvoice || {}, { deletedAt: 0, branchId, status: sails.config.constant.INVOICE_CARD_STATUS.FINISHED });

        let optionInvoiceProducts = {
          select: [
            'SUM(invoiceproduct.finalAmount) as finalAmountProduct',
            'SUM(invoiceproduct.quantity) as quantity',
            'code',
            'id',
            'finalAmount',
            'paidAmount',
            'invoiceAt as time'
          ],
          model: Invoice,
          filter: {...filterInvoice },
          customPopulates: `left join invoiceproduct on invoiceproduct.invoiceId = m.id`,
          groupBy: "id",
          sort: 'invoiceAt DESC',
        };

        filterInvoiceReturn = _.extend(filterInvoiceReturn || {}, { deletedAt: 0, branchId, reason: sails.config.constant.IMPORT_CARD_REASON.INVOICE_RETURN, status: sails.config.constant.IMPORT_CARD_STATUS.FINISHED });

        let optionInvoiceReturnProducts = {
          select: [
            '*',
            'SUM(importcardproduct.finalAmount * importcardproduct.quantity) as finalAmountProduct',
            'SUM(importcardproduct.quantity) as quantity',
            'code',
            'id',
            'finalAmount',
            'paidAmount',
            'importedAt as time'
          ],
          model: ImportCard, 
          filter: {...filterInvoiceReturn },
          customPopulates: `left join importcardproduct on importcardproduct.importCardId = m.id`,
          groupBy: "id",
          sort: 'importedAt DESC',
        };

        let [foundDataInvoiceProduct, foundDataInvoiceReturnProduct] = 
          await Promise.all([sails.helpers.customSendNativeQuery(req, optionInvoiceProducts), sails.helpers.customSendNativeQuery(req, optionInvoiceReturnProducts)]);
        
        let foundInvoiceReturnProduct = _.map(foundDataInvoiceReturnProduct.foundData, item => ({ ...item, isReturn: true }));
        let foundInvoiceProduct = _.map(foundDataInvoiceProduct.foundData, item => ({ ...item, isReturn: false }))
        
        foundData = foundInvoiceProduct.concat(foundInvoiceReturnProduct).sort((a,b) => b.time - a.time);

        count = foundData.length;

        break;
      case sails.config.constant.OPTIONS_SALE_REPORT.PRODUCT_GROUP:
        filterInvoice = _.extend(filterInvoice || {}, { ["invoice.deletedAt"]: 0, ["invoice.branchId"]: branchId, ["invoice.status"]: sails.config.constant.INVOICE_CARD_STATUS.FINISHED });

        let optionInvoiceProductTypeNoTotal = {
          select: [
            'SUM(m.unitPrice * m.quantity) as totalAmount',
            'SUM(m.discount * m.quantity) as discountAmount',
            'SUM(m.finalAmount) as finalAmount',            
            'SUM(m.quantity) as quantity',
            'product.code as code',
            'product.name as name',
            'productId',
          ],
          model: InvoiceProduct,
          filter: {...filterInvoice, ["invoice.totalAmount"]: 0 },
          customPopulates: `left join invoice on invoice.id = m.invoiceId left join product on product.id = m.productId`,
          groupBy: "productId",
          sort: 'productId DESC',
        };

        let optionInvoiceProductType = {
          select: [
            'SUM(m.unitPrice * m.quantity) as totalAmount',
            'SUM(m.discount * m.quantity + invoice.discountAmount/invoice.totalAmount*m.finalAmount) as discountAmount',
            'SUM(m.finalAmount - invoice.discountAmount/invoice.totalAmount*m.finalAmount + invoice.taxAmount/invoice.totalAmount*m.finalAmount) as finalAmount',            
            'SUM(m.quantity) as quantity',
            'product.code as code',
            'product.name as name',
            'productId',
          ],
          model: InvoiceProduct,
          filter: {...filterInvoice, ["invoice.totalAmount"]: { "!=": 0 } },
          customPopulates: `left join invoice on invoice.id = m.invoiceId left join product on product.id = m.productId`,
          groupBy: "productId",
          sort: 'productId DESC',
        };

        filterInvoiceReturn = _.extend(filterInvoiceReturn || {}, { ["importcard.deletedAt"]: 0, ["importcard.branchId"]: branchId, ["importcard.reason"]: sails.config.constant.IMPORT_CARD_REASON.INVOICE_RETURN, ["importcard.status"]: sails.config.constant.IMPORT_CARD_STATUS.FINISHED });

        let optionInvoiceReturnProductTypeNoTotal = {
          select: [
            'SUM(m.unitPrice * m.quantity) as totalAmountReturn',
            'SUM(m.discount * m.quantity) as discountAmountReturn',
            'SUM(m.finalAmount * m.quantity) as finalAmountReturn',
            'SUM(m.quantity) as quantityReturn',
            'product.code as code',
            'product.name as name',
            'productId',
          ],
          model: ImportCardProduct, 
          filter: {...filterInvoiceReturn, ["importcard.totalAmount"]: 0 },
          customPopulates: `left join importcard on m.importCardId = importcard.id left join product on product.id = m.productId`,
          groupBy: "productId",
          sort: 'productId DESC',
        };

        let optionInvoiceReturnProductType = {
          select: [
            'SUM(m.unitPrice * m.quantity) as totalAmountReturn',
            'SUM((m.discount + importcard.discountAmount/importcard.totalAmount*m.finalAmount) * m.quantity) as discountAmountReturn',
            'SUM((m.finalAmount - importcard.discountAmount/importcard.totalAmount*m.finalAmount) * m.quantity) as finalAmountReturn',
            'SUM(m.quantity) as quantityReturn',
            'product.code as code',
            'product.name as name',
            'productId',
          ],
          model: ImportCardProduct, 
          filter: {...filterInvoiceReturn, ["importcard.totalAmount"]: { "!=": 0 } },
          customPopulates: `left join importcard on m.importCardId = importcard.id left join product on product.id = m.productId`,
          groupBy: "productId",
          sort: 'productId DESC',
        };

        let [foundDataInvoiceProductType, foundDataInvoiceProductTypeNoTotal, foundDataInvoiceReturnProductType, foundDataInvoiceReturnProductTypeNoTotal] = 
          await Promise.all([
            sails.helpers.customSendNativeQuery(req, optionInvoiceProductType),
            sails.helpers.customSendNativeQuery(req, optionInvoiceProductTypeNoTotal),
            sails.helpers.customSendNativeQuery(req, optionInvoiceReturnProductType),
            sails.helpers.customSendNativeQuery(req, optionInvoiceReturnProductTypeNoTotal),
          ]);
        
        let foundProductType = {}
        _.forEach(foundDataInvoiceProductType.foundData, item => foundProductType[item.productId] = {...item, totalAmountReturn: 0, discountAmountReturn: 0, finalAmountReturn: 0, quantityReturn: 0});
        _.forEach(foundDataInvoiceProductTypeNoTotal.foundData, item => {
          if (foundProductType[item.productId]) {
            foundProductType[item.productId] = { 
              ...foundProductType[item.productId], 
              totalAmount: foundProductType[item.productId].totalAmount + item.totalAmount, 
              discountAmount: foundProductType[item.productId].discountAmount + item.discountAmount, 
              finalAmount: foundProductType[item.productId].finalAmount + item.finalAmount, 
              quantity: foundProductType[item.productId].quantity + item.quantity 
            }
          }
          else foundProductType[item.productId] = { ...item, totalAmountReturn: 0, discountAmountReturn: 0, finalAmountReturn: 0, quantityReturn: 0 }
        })
        _.forEach(foundDataInvoiceReturnProductType.foundData, item => {
          if (foundProductType[item.productId]) {
            foundProductType[item.productId] = { ...foundProductType[item.productId], ...item }
          }
          else foundProductType[item.productId] = { ...item, totalAmount: 0, discountAmount: 0, finalAmount: 0, quantity: 0 }
        })

        _.forEach(foundDataInvoiceReturnProductTypeNoTotal.foundData, item => {
          if (foundProductType[item.productId]) {
            foundProductType[item.productId] = { 
              ...foundProductType[item.productId], 
              totalAmountReturn: foundProductType[item.productId].totalAmountReturn + item.totalAmountReturn, 
              discountAmountReturn: foundProductType[item.productId].discountAmountReturn + item.discountAmountReturn, 
              finalAmountReturn: foundProductType[item.productId].finalAmountReturn + item.finalAmountReturn, 
              quantityReturn: foundProductType[item.productId].quantityReturn + item.quantityReturn 
            }
          }
          else foundProductType[item.productId] = { ...item, totalAmount: 0, discountAmount: 0, finalAmount: 0, quantity: 0 }
        })
        
        foundData = Object.values(foundProductType);

        count = foundData.length;

        break;
      default:
        break;
    }    

    return exits.success({ status: true, data: foundData, count: count });
  }

}