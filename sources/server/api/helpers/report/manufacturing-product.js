module.exports = {
  description: 'get manufacturing report',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    },
  },

  fn: async function (inputs, exits) {
    let { startDate, endDate, branchId, categories, selects } = inputs.data;
    let { req } = inputs;
    let arrData = [];
    let filterSelect = '';
    let productSelect = '';
    let timerCurrent = new Date().getTime();

    if (categories.length) {
      filterSelect = ` AND p.category IN (${categories.join(",")})`
    }

    if (selects.length) {
      productSelect = ` AND p.id IN (${selects.join(",")})`
    }

    // câu query sql cho nhập sản phẩm
    let SQL_IMPORT_PRODUCT = `SELECT mp.productId id, mp.productCode as code, mp.productName as name, mp.productUnit as unit, sum(mp.quantity) quantityImport
                              FROM movestockcard m
                              LEFT JOIN movestockcardproduct mp ON m.id = mp.moveStockCardId
                              LEFT JOIN product p ON mp.productId = p.id 
                              WHERE m.reason = 1 AND m.movedAt BETWEEN $1 AND $2 AND m.branchId = ${branchId} ${filterSelect} ${productSelect}
                              GROUP BY mp.productId`

    // câu query sql cho xuất sản phẩm
    let SQL_EXPORT_PRODUCT = `SELECT mp.productId id, mp.productCode as code, mp.productName as name, mp.productUnit as unit, sum(mp.quantity) quantityExport
                              FROM movestockcard m
                              LEFT JOIN movestockcardproduct mp ON m.id = mp.moveStockCardId
                              LEFT JOIN product p ON mp.productId = p.id 
                              WHERE (m.reason = 2 OR m.reason = 3) AND m.movedAt BETWEEN $1 AND $2 AND m.branchId = ${branchId} ${filterSelect} ${productSelect}
                              GROUP BY mp.productId`

    // câu query sql cho sản xuất thành phẩm
    let SQL_FINISHED_PRODUCT = `SELECT mp.productId id, mp.code, mp.name, pu.name as unit, sum(mp.quantity) quantityFinished
                              FROM manufacturingcard m
                              LEFT JOIN manufacturingcardfinishedproduct mp ON m.id = mp.manufacturingCardId
                              LEFT JOIN product p ON mp.productId = p.id 
                              LEFT JOIN productunit pu ON p.unitId = pu.id 
                              WHERE m.status = 1 AND m.createdAt BETWEEN $1 AND $2 AND m.branchId = ${branchId} ${filterSelect} ${productSelect}
                              GROUP BY mp.productId`

    // câu query sql cho đã được sản xuất của sản phẩm
    let SQL_MATERIAL_PRODUCT = `SELECT mp.productId id, mp.code, mp.name, pu.name as unit, sum(mp.quantity) quantityProduced
                                FROM manufacturingcard m
                                LEFT JOIN manufacturingcardmaterial mp ON m.id = mp.manufacturingCardId
                                LEFT JOIN product p ON mp.productId = p.id 
                                LEFT JOIN productunit pu ON p.unitId = pu.id 
                                WHERE m.status = 1 AND m.createdAt BETWEEN $1 AND $2 AND m.branchId = ${branchId} ${filterSelect} ${productSelect}
                                GROUP BY mp.productId`

    let [ productStock, productImportMid, productExportMid, productFinishedMid, productProducedMid, 
          productImportLast, productExportLast, productFinishedLast, productProducedLast ] = 
          await Promise.all([
            ProductStock.find(req, {where:{ branchId }}),
            sails.sendNativeQuery(req, SQL_IMPORT_PRODUCT, [startDate, endDate]),
            sails.sendNativeQuery(req, SQL_EXPORT_PRODUCT, [startDate, endDate]),
            sails.sendNativeQuery(req, SQL_FINISHED_PRODUCT, [startDate, endDate]),
            sails.sendNativeQuery(req, SQL_MATERIAL_PRODUCT, [startDate, endDate])
          ].concat(timerCurrent > Number(endDate) ? [
            sails.sendNativeQuery(req, SQL_IMPORT_PRODUCT, [endDate, timerCurrent]),
            sails.sendNativeQuery(req, SQL_EXPORT_PRODUCT, [endDate, timerCurrent]),
            sails.sendNativeQuery(req, SQL_FINISHED_PRODUCT, [endDate, timerCurrent]),
            sails.sendNativeQuery(req, SQL_MATERIAL_PRODUCT, [endDate, timerCurrent])
          ] : []))
    
    // Trong kỳ
    _.forEach(productImportMid.rows, item => {
      arrData[item.id] = {
        ...arrData[item.id],
        ...item,
        quantityImport: item.quantityImport,
      }
    })
    _.forEach(productExportMid.rows, item => {
      arrData[item.id] = {
        ...arrData[item.id],
        ...item,
        quantityExport: item.quantityExport,
      }
    })

    _.forEach(productFinishedMid.rows, item => {
      arrData[item.id] = {
        ...arrData[item.id],
        ...item,
        quantityImport: ((arrData[item.id] && arrData[item.id].quantityImport) || 0) + item.quantityFinished, //sản phẩm đã nhập = sp nhập + thành phẩm (sản phẩm đã sản xuất)
      }
    })

    _.forEach(productProducedMid.rows, item => {
      arrData[item.id] = {
        ...arrData[item.id],
        ...item,
        quantityProduced: item.quantityProduced,
      }
    })

    //tính tồn
    _.forEach(productStock, item => {
      if(arrData[item.productId])
        arrData[item.productId] = {
          ...arrData[item.productId],
          quantityManufacturing: item.manufacturingQuantity,
        }
    })
    
    if(timerCurrent > Number(endDate)) {

      _.forEach(productImportLast.rows, item => {
        if(arrData[item.id]){
          arrData[item.id] = {
            ...arrData[item.id],
            quantityManufacturing: (arrData[item.id].quantityManufacturing || 0) - item.quantityImport,
          }
        }
      })

      _.forEach(productExportLast.rows, item => {
        if(arrData[item.id]){
          arrData[item.id] = {
            ...arrData[item.id],
            quantityManufacturing: (arrData[item.id].quantityManufacturing || 0) + item.quantityExport,
          }
        }
      })

      _.forEach(productFinishedLast.rows, item => {
        if(arrData[item.id]){
          arrData[item.id] = {
            ...arrData[item.id],
            quantityManufacturing: (arrData[item.id].quantityManufacturing || 0) - item.quantityFinished,
          }
        }
      })

      _.forEach(productProducedLast.rows, item => {
        if(arrData[item.id]){
          arrData[item.id] = {
            ...arrData[item.id],
            quantityManufacturing: (arrData[item.id].quantityManufacturing || 0) + item.quantityProduced,
          }
        }
      })
    }
    //sắp xếp theo thứ tự ngày tạo tăng dần
    arrData.sort(function (a, b) {
      return a.name - b.name;
    });

    return exits.success({ status: true, data: Object.values(arrData), totalProduct: Object.values(arrData).length })
  }
}