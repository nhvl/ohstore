module.exports = {
  description: 'get return report',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    }
  },

  fn: async function (inputs, exits) {
    let { startDate, endDate, options, branchId, selects } = inputs.data;
    let { req } = inputs;

    switch (options) {
      case sails.config.constant.OPTION_RETURN_REPORT.CUSTOMER:
        let querySelectCustomers = "";
        
        if (selects && selects.length) {
          querySelectCustomers = ` AND i.recipientId IN (${selects.join(",")})`
        }

        let QUERY_SQL_CUSTOMER = `Select c.id as id, c.name as customerName, c.code as customerCode, COUNT(i.id) as count, SUM(i.finalAmount) as finalAmount, SUM(i.discountAmount) as discountAmount, SUM(i.totalAmount) as totalAmount
                                  from importcard i
                                  LEFT JOIN customer c ON c.id = i.recipientId
                                  WHERE i.recipientId IN (SELECT id FROM customer where type = ${sails.config.constant.CUSTOMER_TYPE.TYPE_CUSTOMER}) AND i.branchId = ${branchId}
                                  AND i.status = ${sails.config.constant.IMPORT_CARD_STATUS.FINISHED} AND i.deletedAt = 0 AND i.importedAt BETWEEN $1 AND $2 AND i.reason = ${sails.config.constant.IMPORT_CARD_REASON.INVOICE_RETURN} ${querySelectCustomers}
                                  GROUP BY i.recipientId
                                  ORDER BY i.recipientId ASC`;

        let queryCustomer = await sails.sendNativeQuery(req, QUERY_SQL_CUSTOMER, [startDate, endDate]);

        return exits.success({ status: true, data: queryCustomer.rows })
      case sails.config.constant.OPTION_RETURN_REPORT.PRODUCT:
        let querySelectProducts = "";
        
        if (selects && selects.length) {
          querySelectProducts = ` AND i.productId IN (${selects.join(",")})`
        }

        let PRODUCT_IN_INVOICERETURNPRODUCT_SQL =
          `SELECT i.productId, p.name as productName, p.code as productCode, u.name as unitName, SUM(i.quantity) as quantity, SUM((i.finalAmount * i.quantity) - ((iv.discountAmount / iv.totalAmount)*(i.finalAmount * i.quantity))) as finalAmount, SUM(i.discount * i.quantity) as discountAmount, SUM(i.unitPrice * i.quantity) as totalAmount
          FROM importcardproduct i
          LEFT JOIN product p ON p.id = i.productId
          LEFT JOIN productunit u ON u.id = p.unitId
          LEFT JOIN importcard iv ON iv.id = i.importCardId
          WHERE i.importCardId IN (SELECT id FROM importcard WHERE status = ${sails.config.constant.IMPORT_CARD_STATUS.FINISHED} AND deletedAt = 0 AND reason = ${sails.config.constant.IMPORT_CARD_REASON.INVOICE_RETURN} AND importedAt BETWEEN $1 AND $2 AND totalAmount > 0 AND branchId = ${branchId})
          AND i.productId IN (SELECT id FROM product) ${querySelectProducts}
          GROUP BY i.productId
          ORDER BY i.productId ASC`;

        let PRODUCT_IN_INVOICERETURNPRODUCT_NOTOTAL_SQL =
          `SELECT i.productId, p.name as productName, p.code as productCode, u.name as unitName, SUM(i.quantity) as quantity, SUM(i.finalAmount * i.quantity) as finalAmount, SUM(i.discount * i.quantity) as discountAmount, SUM(i.unitPrice * i.quantity) as totalAmount
          FROM importcardproduct i
          LEFT JOIN product p ON p.id = i.productId
          LEFT JOIN importcard iv ON iv.id = i.importCardId
          LEFT JOIN productunit u ON u.id = p.unitId
          WHERE i.importCardId IN (SELECT id FROM importcard WHERE status = ${sails.config.constant.IMPORT_CARD_STATUS.FINISHED} AND deletedAt = 0 AND reason = ${sails.config.constant.IMPORT_CARD_REASON.INVOICE_RETURN} AND importedAt BETWEEN $1 AND $2 AND totalAmount = 0 AND branchId = ${branchId})
          AND i.productId IN (SELECT id FROM product) ${querySelectProducts}
          GROUP BY i.productId
          ORDER BY i.productId ASC`;

        let [queryProductTotal, queryProductNoTotal] = await Promise.all([
          sails.sendNativeQuery(req, PRODUCT_IN_INVOICERETURNPRODUCT_SQL, [startDate, endDate]),
          sails.sendNativeQuery(req, PRODUCT_IN_INVOICERETURNPRODUCT_NOTOTAL_SQL, [startDate, endDate]),
        ]);

        let arrProducts = {};

        _.forEach(queryProductTotal.rows, item => {
          arrProducts[item.productId] = item
        })

        _.forEach(queryProductNoTotal.rows, item => {
          if (arrProducts[item.productId]) {
            arrProducts[item.productId] = { ...arrProducts[item.productId], finalAmount: arrProducts[item.productId].finalAmount - item.finalAmount, quantity: arrProducts[item.productId].quantity + item.quantity, totalAmount: arrProducts[item.productId].totalAmount + item.totalAmount, discountAmount: arrProducts[item.productId].discountAmount + item.discountAmount }
          } else
            arrProducts[item.productId] = item
        })

        
        return exits.success({ status: true, data: Object.values(arrProducts).filter(item => (item && item.quantity !== 0)) })
      default:
        return exits.success({ status: true, data: [] })
    }
  }
}