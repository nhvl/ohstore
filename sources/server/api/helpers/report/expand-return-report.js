module.exports = {
  description: 'list invoice cards',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    }
  },

  fn: async function (inputs, exits) {
    let { filter, options, branchId } = inputs.data;

    let { req } = inputs;

    let foundData = [];
    let count = 0;
    
    switch (options) {
      case sails.config.constant.OPTION_RETURN_REPORT.CUSTOMER:
        filter = _.extend(filter || {}, { deletedAt: 0, branchId, reason: sails.config.constant.IMPORT_CARD_REASON.INVOICE_RETURN, status: sails.config.constant.IMPORT_CARD_STATUS.FINISHED });
        
        let optionInvoiceReturns = {
          select: ['*'],
          model: ImportCard,
          filter: { ...filter },
          sort: 'importedAt DESC',
        };

        let [foundDataInvoiceReturn] =
          await Promise.all([sails.helpers.customSendNativeQuery(req, optionInvoiceReturns)]);

        let foundInvoiceReturn = _.map(foundDataInvoiceReturn.foundData, item => ({ ...item, isReturn: true, time: item.importedAt }));

        foundData = foundInvoiceReturn.sort((a, b) => b.time - a.time);

        count = foundData.length;

        break;
      case sails.config.constant.OPTION_RETURN_REPORT.PRODUCT:
        filter = _.extend(filter || {}, { deletedAt: 0, branchId, reason: sails.config.constant.IMPORT_CARD_REASON.INVOICE_RETURN, status: sails.config.constant.IMPORT_CARD_STATUS.FINISHED });

        let optionInvoiceReturnProductNoTotals = {
          select: [
            '*',
            'SUM(importcardproduct.finalAmount * importcardproduct.quantity ) as finalAmountProduct',
            'SUM(importcardproduct.quantity) as quantity',
            'code',
            'id',
            'finalAmount',
            'paidAmount',
            'importedAt as time'
          ],
          model: ImportCard,
          filter: { ...filter, ["m.totalAmount"]: 0 },
          customPopulates: `left join importcardproduct on importcardproduct.importCardId = m.id`,
          groupBy: "id",
          sort: 'importedAt DESC',
        };

        let optionInvoiceReturnProductTotals = {
          select: [
            '*',
            'SUM((importcardproduct.finalAmount - m.discountAmount/m.totalAmount*importcardproduct.finalAmount) * importcardproduct.quantity) as finalAmountProduct',
            'SUM(importcardproduct.quantity) as quantity',
            'code',
            'id',
            'finalAmount',
            'paidAmount',
            'importedAt as time'
          ],
          model: ImportCard,
          filter: { ...filter, ["m.totalAmount"]: { "!=": 0 } },
          customPopulates: `left join importcardproduct on importcardproduct.importCardId = m.id`,
          groupBy: "id",
          sort: 'importedAt DESC',
        };

        let [foundDataInvoiceReturnProductNoTotal, foundDataInvoiceReturnProductTotal] =
          await Promise.all([sails.helpers.customSendNativeQuery(req, optionInvoiceReturnProductNoTotals), sails.helpers.customSendNativeQuery(req, optionInvoiceReturnProductTotals)]);
        
        let foundDataInvoiceReturnNoTotal = _.map(foundDataInvoiceReturnProductNoTotal.foundData, item => ({ ...item }));
        let foundDataInvoiceReturnTotal = _.map(foundDataInvoiceReturnProductTotal.foundData, item => ({ ...item }));

        foundData = foundDataInvoiceReturnNoTotal.concat(foundDataInvoiceReturnTotal).sort((a, b) => b.time - a.time);

        count = foundData.length;

        break;
      default:
        break;
    }

    return exits.success({ status: true, data: foundData, count: count });
  }

}