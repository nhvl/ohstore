module.exports = {
  description: 'list product on return report',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    }
  },

  fn: async function (inputs, exits) {
    let { branchId, id } = inputs.data;
    let { req } = inputs;
    let foundProduct = [];

    let foundInvoiceReturn = await ImportCard.findOne(req, { id, branchId }).intercept({ name: 'UsageError' }, () => {
      return exits.success({ status: false, message: sails.__(sails.config.constant.INTERCEPT.UsageError) });
    })

    if (!foundInvoiceReturn) {
      return exits.success({ message: sails.__("Không tìm thấy phiếu trả hàng"), status: false })
    }

    let foundProductInvoiceReturn = await ImportCardProduct.find(req, { importCardId: id });

    _.forEach(foundProductInvoiceReturn, item => {
      if (foundInvoiceReturn.totalAmount !== 0) {
        item.discount = (item.discount + foundInvoiceReturn.discountAmount / foundInvoiceReturn.totalAmount * item.finalAmount) * item.quantity;
        item.finalAmount = (item.finalAmount - foundInvoiceReturn.discountAmount / foundInvoiceReturn.totalAmount * item.finalAmount) * item.quantity;
      }
      else {
        item.discount = item.discount * item.quantity;
        item.finalAmount = item.finalAmount * item.quantity;
      }
    });

    foundProduct = foundProductInvoiceReturn.filter(item => item.quantity !== 0);

    return exits.success({ status: true, data: foundProduct })
  }
}