module.exports = {
  description: 'get invoice return card',

  inputs: {
    req: {
      type: "ref"
    },
    id: {
      type: "number"
    },
    branchId: {
      type: "number"
    },
  },

  fn: async function (inputs, exits) {
    let { req, id , branchId} = inputs;
    
    foundInvoiceReturn= await ImportCard.findOne(req, {
        where: { id: id, reason: sails.config.constant.IMPORT_CARD_REASON.INVOICE_RETURN}
      }).populate('recipientId')
      .intercept({ name: 'UsageError' }, () => {
          return exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
        });
    
    let checkBanch = await sails.helpers.checkBranch(foundInvoiceReturn.branchId, req);

    if(!checkBanch){
      return exits.success({status: false, message: sails.__('Không có quyền thực hiện thao tác này')});
    }

    let [foundInvoiceCard, createdBy, invoiceReturnProductArray, productUnitArray, foundExpenseDetail] = await Promise.all([
      Invoice.findOne(req, { where: { code: foundInvoiceReturn.reference,}}).populate('customerId').populate('invoiceProducts'),
      User.findOne(req, { where: { id: foundInvoiceReturn.createdBy }, select: ["id", "fullName"]}),
      ImportCardProduct.find(req, { importCardId: foundInvoiceReturn.id }).populate("productId"),
      ProductUnit.find(req, { deletedAt: 0 }),
      sails.helpers.income.getCardDetail(req, { cardId: foundInvoiceReturn.id, incomeExpenseCardTypeCode: sails.config.constant.DEFAULT_INCOME_EXPENSE_CARD_TYPES.INVOICE_RETURN.code })
    ])
    
    // lấy thông tin khách hàng
    if(foundInvoiceCard) {
      foundInvoiceReturn.invoice = foundInvoiceCard;
    }
    
    if (createdBy) foundInvoiceReturn.createdBy = createdBy;

    let productUnit = {};
    productUnitArray.forEach(item => productUnit[item.id] = item);
    let productStock = {};
    (await ProductStock.find(req, { productId: {in: _.uniq(invoiceReturnProductArray.map(item => item.productId.id))}, branchId })).forEach(item => productStock[item.productId] = item);

    if ( productUnit ) {
      _.forEach(invoiceReturnProductArray, item => {
        item.type = item.productId.type;
        item.unit = productUnit[item.productId.unitId].name;
        item.productId = item.productId.id;
        item.productStock = productStock[item.productId];
      });
    }

    //lấy danh sách phiếu chi liên quan đến phiếu trả hàng

    let expenseCards = [];
    if (foundExpenseDetail.status) {
      expenseCards = foundExpenseDetail.data.incomeExpenseCardDetail
    }
    else {
      return exits.success({ status: false, message: sails.config.constant.INTERCEPT.UsageError });
    }

    exits.success({ status: true, data: foundInvoiceReturn, invoiceReturnProductArray, expenseCards });
  }
}