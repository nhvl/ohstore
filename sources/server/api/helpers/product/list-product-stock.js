module.exports = {
    description: 'get products stock',
  
    inputs: {
      req: {
        type: "ref"
      },
      data: {
        type: "ref",
        required: true
      }
    },
  
    fn: async function (inputs, exits) {
      let { branchId } = inputs.data;
      let { req } = inputs;
      
      let productStocks = await ProductStock.find(req, { branchId })
  
      return exits.success({ status: true, data: productStocks });
  
    }
  }