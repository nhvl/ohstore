module.exports = {
  description: 'update max discount',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    },
  },

  fn: async function (inputs, exits) {
    let {id, maxDiscount} = inputs.data;
    let {req} = inputs;
    
    if(maxDiscount > 100)
      return exits.success({status: false, message: sails.__("Chiết khấu không được quá 100%")});

    let foundProduct = await Product.findOne(req, {
      where: {
        id: id,
      }
    }).intercept({ name: 'UsageError' }, () => {
      return exits.success({status: false, message: sails.__(sails.config.constant.INTERCEPT.UsageError)});
    });
    
    if (!foundProduct) {
      return exits.success({status: false, message: sails.__(sails.config.constant.INTERCEPT.NOT_FOUND_PRODUCT)});
    }

    await Product.update(req, { id: id }).set({
      maxDiscount: Math.round(maxDiscount*100)/100
    }).intercept({ name: 'UsageError' }, () => {
      return exits.success({status: false, message: sails.__(sails.config.constant.INTERCEPT.UsageError)});
    }).fetch();

    return exits.success({status: true})
  }

}