module.exports = {
  description: 'delete multiple stock card info',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    }
  },

  fn: async function (inputs, exits) {

    let { ids, branchId, updatedBy } = inputs.data

    let { req } = inputs

    if (!ids || !Array.isArray(ids) || !ids.length) {
      this.res.status(400).json({
        status: false,
        error: sails.__('Thông tin yêu cầu không hợp lệ')
      });
      return;
    }

    // Kiểm tra các phiếu có tồn tại không

    let foundStock = await Promise.all(ids.map(id => {
      return ProductStock.findOne(req, { productId: id, branchId }).intercept({ name: 'UsageError' }, () => {
        return exits.success({ status: false, message: sails.__(sails.config.constant.INTERCEPT.UsageError) });
      })
    }))
    
    for (let item of foundStock ) {

      if (item.manufacturingQuantity > 0) {
        return exits.success({
          status: false,
          message: sails.__(sails.config.constant.INTERCEPT.STILL_INVENTORY)
        })
      }

      let updatedStockCard = await ProductStock.update(req, { id: item.id, branchId }).set({
        isView: sails.config.constant.IS_VIEW_WARE_HOUSE.notView,
        updatedBy
      }).intercept({ name: 'UsageError' }, () => {
        return exits.success({ status: false, message: sails.__(sails.config.constant.INTERCEPT.UsageError) });
      }).fetch()

    }

    return exits.success({
      status: true
    })
  }
}