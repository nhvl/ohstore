module.exports = {
  description: 'get invoice return card',

  inputs: {
    req:{
      type: "ref"
    },
    id: {
      type: "number"
    },
    branchId: {
      type: "number"
    },

  },

  fn: async function (inputs, exits) {
    let { req, id , branchId} = inputs;
    
    foundImportReturn= await ExportCard.findOne(req, {
        where: { id: id, reason: sails.config.constant.EXPORT_CARD_REASON.RETURN_PROVIDER}
      }).intercept({ name: 'UsageError' }, () => {
          exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
      });

    if (!foundImportReturn) {
      exits.success({ message: sails.__("Không tìm thấy phiếu trả hàng nhập"), status: false });
    }

    let checkBanch = await sails.helpers.checkBranch(foundImportReturn.branchId, req);

    if(!checkBanch){
      return exits.success({status: false, message: sails.__('Không có quyền thực hiện thao tác này')});
    }

    let [productStock, productPrice, foundIncomeDetail, createdBy, customers, importReturnProductArray, productUnit] = await Promise.all([
      ProductStock.find(req, {branchId}),
      ProductPrice.find(req, {branchId}),
      sails.helpers.income.getCardDetail(req, { cardId: foundImportReturn.id, incomeExpenseCardTypeCode: sails.config.constant.DEFAULT_INCOME_EXPENSE_CARD_TYPES.IMPORT_RETURN.code }),
      User.findOne(req, { where: { id: foundImportReturn.createdBy }, select: ["id", "fullName"] }),
      Customer.findOne(req, { where: { id: foundImportReturn.recipientId, type: sails.config.constant.CUSTOMER_TYPE.TYPE_SUPPLIER }}),
      ExportCardProduct.find(req, { exportCardId: foundImportReturn.id }).populate("productId"),
      ProductUnit.find(req, { deletedAt: 0 }),      
    ]);

    if (createdBy) foundImportReturn.createdBy = createdBy;
      
    foundImportReturn.customerId = customers;

    let objProductPrices = {};
    let objProductStocks = {};

    if ( productStock ) {
      _.forEach(productStock, item => objProductStocks[item.productId] = item)
    }

    if ( productPrice ) {
      _.forEach(productPrice, item => objProductPrices[item.productId] = item)
    }

    if ( productUnit ) {
      _.forEach(importReturnProductArray, item => {
        let stocks = Object.values(sails.config.constant.STOCK_QUANTITY_LIST);
        
        stocks.map(stock => {
          if (productStock) {
            item.productId[stock] = objProductStocks[item.productId.id] && objProductStocks[item.productId.id][stock] || 0;
          } else {
            item.productId[stock] = 0;
          }
        });

        item.productId.lastImportPrice = objProductPrices[item.productId.id] && objProductPrices[item.productId.id].lastImportPrice || 0;

        _.forEach(productUnit, elem => {          
          if ( item.productId.unitId && item.productId.unitId == elem.id ) {
            item.unit = elem.name;
            //item.productId = item.productId.id;
            return;
          }
        });
      });
    }

    //lấy danh sách phiếu thu liên quan đến phiếu trả hàng nhập
    let incomeCards = [];
    if (foundIncomeDetail.status) {
      incomeCards = foundIncomeDetail.data.incomeExpenseCardDetail
    }
    else {
      return exits.success({ status: false, message: sails.config.constant.INTERCEPT.UsageError });
    } 

    exits.success({ status: true, data: foundImportReturn, importReturnProductArray, incomeCards });
  }
}