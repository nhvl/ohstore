const Datasource = require('mutilTenant/datasource');

module.exports = {
  description: 'Lấy kết nối cửa hàng',

  inputs: {
    data: {
      type: "ref",
      required: true
    }
  },

  fn: async function (inputs, exits) {
    let { storeId } = inputs.data;
    let foundTenant;
    // lấy datasource
    if(storeId === 0){
      foundTenant = {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        schema: true,
        adapter: 'sails-mysql',
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        identity: process.env.DB_IDENTITY,
      }
    } else {
      foundTenant = await Tenants.findOne({id: storeId});
      if(!foundTenant) {
         return exits.success({
           status: false,
           message: sails.__("Không tìm thấy cửa hàng")
         });
      }
    }
    
    let datasource = new Datasource(_.pick(foundTenant, ['host', 'port', 'schema', 'adapter', 'user', 'password', 'database', 'identity']));
    
    return exits.success({
      status: true, 
      data: datasource,
      foundTenant: foundTenant,
    });
  }

}