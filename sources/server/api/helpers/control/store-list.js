const Datasource = require('mutilTenant/datasource');

module.exports = {
  description: 'Danh sách sửa hàng',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    }
  },

  fn: async function (inputs, exits) {
    let { filter, sort, limit, skip, manualFilter, manualSort } = inputs.data;

    let {req} = inputs;
    
    filter = _.extend(filter || {}, { deletedAt: 0 });

    let foundTenants = await sails.sendNativeQuery("select * from tenants INNER join (SELECT schema_name FROM information_schema.schemata) db on db.schema_name = tenants.database");
    foundTenants = foundTenants.rows;

    let mainStore = await sails.helpers.control.getDatasource({storeId: 0});
    if(!mainStore.status) {
      return exits.success(mainStore);
    }
    mainStore.data.id = 0;
    
    return exits.success({status: true, data: [mainStore.data].concat(foundTenants)});
  }

}