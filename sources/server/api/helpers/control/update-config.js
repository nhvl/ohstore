const Datasource = require('mutilTenant/datasource');

module.exports = {
  description: 'Lấy thông tin cửa hàng',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    }
  },

  fn: async function (inputs, exits) {
    let { language_product, manufacturing_stock, manufacturing, is_active, storeId } = inputs.data;
    let {req} = inputs;
    
    // lấy datasource
    let getDatasource = await sails.helpers.control.getDatasource({storeId});
    if(!getDatasource.status) {
      return exits.success(getDatasource);
    }
    let datasource = getDatasource.data;
    
    // lấy store_info
    let getStoreInfo = await StoreConfig.findOne(datasource, {type: 'store_info'});
    let store_info = _.isJson(getStoreInfo.value) ? JSON.parse(getStoreInfo.value) : {};
    
    let expirydate = store_info.expirydate;
    if(is_active !== (expirydate && _.moment(parseFloat(expirydate)) < _.moment() ? 0 : 1)) {
      if(is_active) expirydate = _.moment().add(10, 'days').valueOf()
      else expirydate = _.moment().valueOf()
    }
    
    // cập nhật config
    await Promise.all([
      StoreConfig.update(datasource, {type: "language_product"}).set({value: language_product}),
      StoreConfig.update(datasource, {type: "manufacturing_stock"}).set({value: manufacturing_stock}),
      StoreConfig.update(datasource, {type: "manufacturing"}).set({value: manufacturing}),
      StoreConfig.update(datasource, {type: "store_info"}).set({
        value: JSON.stringify({
          ...store_info, 
          expirydate
        })
      }),
    ]);
    
    return exits.success({
      status: true, 
    });
  }

}