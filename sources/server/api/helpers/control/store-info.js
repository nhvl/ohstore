const Datasource = require('mutilTenant/datasource');

module.exports = {
  description: 'Lấy thông tin cửa hàng',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    }
  },

  fn: async function (inputs, exits) {
    let { storeId } = inputs.data;
    let {req} = inputs;
    
    // lấy datasource
    let getDatasource = await sails.helpers.control.getDatasource({storeId});
    if(!getDatasource.status) {
      return exits.success(getDatasource);
    }
    let datasource = getDatasource.data;
    
    // Lấy thông tin cửa hàng
    let foundStoreInfo = await sails.helpers.storeConfig.get(datasource, {
      types: ["store_info", "manufacturing", "manufacturing_stock", "language_product"]
    })
    if(!foundStoreInfo.status) {
      return exits.success(foundStoreInfo);
    }
    
    // // Lấy dánh sách khách hàng
    // let foundCustomer = await sails.helpers.customer.list({})
    
    return exits.success({status: true, data: {
      storeConfig: foundStoreInfo.data,
      tenant: getDatasource.foundTenant
    }});
  }

}