const Datasource = require('mutilTenant/datasource');

module.exports = {
  description: 'Lấy thông tin khách hàng sửa hàng',

  inputs: {
    req: {
      type: "ref"
    },
    data: {
      type: "ref",
      required: true
    }
  },

  fn: async function (inputs, exits) {
    let { storeId, filter, sort, limit, skip, select, isBranch, branchId, type } = inputs.data;
    let {req} = inputs;
    
    // lấy datasource
    let getDatasource = await sails.helpers.control.getDatasource({storeId});
    if(!getDatasource.status) {
      return exits.success(getDatasource);
    }
    let datasource = getDatasource.data;
    
    // Lấy dánh sách khách hàng
    filter = _.extend(filter || {}, { deletedAt: 0, type });

    let options = {
      select: select || [
        '*',
        "branch.name as branchName"
      ],
      model: Customer, 
      filter: {...filter },
      limit,
      skip,
      customPopulates: `left join branch on branch.id = m.branchId`,
      sort: sort.concat(sort.find(item => Object.keys(item)[0] === 'branchId') ? [] : [{branchId: 'asc'}]).concat(sort.find(item => Object.keys(item)[0] === 'createdAt') ? [] : [{createdAt: 'desc'}]),
      count: true
    };

    let {foundData, count} = await sails.helpers.customSendNativeQuery(datasource, options);
    
    return exits.success({ data: foundData, status: true, count: count});
  }

}