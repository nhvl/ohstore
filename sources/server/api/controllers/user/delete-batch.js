module.exports = {
    friendlyName: "Delete Multiple User",
  
    description: "Delete multiple user",
  
    inputs: {
      ids: {
        type: 'json'
      },
    },
  
    fn: async function (inputs, exits) {
      let ids = inputs.ids;
      let branchId = this.req.headers['branch-id'];

      if (!ids || !Array.isArray(ids) || !ids.length) {
        this.res.json({
          status: false,
          error: sails.__('Thông tin yêu cầu không hợp lệ')
        });
        return;
      }

      //kiểm tra xem user có tự xóa chính mình không
      let findLoggedInUser = ids.find(item => item === this.req.loggedInUser.id);
      if(findLoggedInUser){
        this.res.json({
          status: false,
          error: sails.__('Bạn không được xóa tài khoản của chính mình')
        });
        return;
      }

      let findAdmin = ids.find(item => item === 1);

      if (findAdmin) {
        this.res.json({
          status: false,
          error: sails.__('Bạn không được quyền xóa tài khoản Admin')
        });
        return;
      }
      // Hóa đơn
      let findCustomerInvoice = await Invoice.find(this.req, {
        where: { createdBy: { in: ids } , branchId, deletedAt: 0}
      }).intercept({ name: 'UsageError' }, () => {
        return exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
      });

      if(findCustomerInvoice.length > 0){
        return exits.success({ message: sails.__("Không thể xóa vì đã có hóa đơn"), status: false });
      }
      // Phiếu trả hàng
      let findCustomerInvoiceReturn = await ImportCard.find(this.req, {
        where: { createdBy: { in: ids }, branchId, deletedAt: 0 }
      }).intercept({ name: 'UsageError' }, () => {
          return exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
      })
      if(findCustomerInvoiceReturn.length > 0){
        return exits.success({ message: sails.__("Không thể xóa vì đã có phiếu trả hàng"), status: false });
      }
      // Phiếu đơn hàng
      let findCustomerOrderCard = await OrderCard.find(this.req, {
        where: { createdBy: { in: ids }, branchId, deletedAt: 0 }
      }).intercept({ name: 'UsageError' }, () => {
          return exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
      })
      if(findCustomerOrderCard.length > 0){
        return exits.success({ message: sails.__("Không thể xóa vì đã có phiếu đơn hàng"), status: false });
      }
      // Phiếu trả hàng nhập
      let findCustomerImportReturn = await ExportCard.find(this.req, {
        where: { createdBy: { in: ids }, branchId, deletedAt: 0 }
      }).intercept({ name: 'UsageError' }, () => {
          return exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
      })
      if(findCustomerImportReturn.length > 0){
        return exits.success({ message: sails.__("Không thể xóa vì đã có phiếu trả hàng nhập"), status: false });
      }
      // Sản phẩm
      let findCustomerProduct = await Product.find(this.req, {
        where:{createdBy: { in: ids }, deletedAt: 0}
      }).intercept({ name: 'UsageError' }, () => {
        return exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
      });

      if(findCustomerProduct.length > 0){
        return exits.success({ message: sails.__("Không thể xóa vì đang là nhà cung cấp của một sản phẩm"), status: false });
      }
      // Phiếu ký gửi
      let findCustomerDeposit = await DepositCard.find(this.req, {
        where:{createdBy: { in: ids }, branchId, deletedAt: 0}
      }).intercept({ name: 'UsageError' }, () => {
        return exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
      });
  
      if(findCustomerDeposit.length > 0){
        return exits.success({ message: sails.__("Không thể xóa vì đã có phiếu ký gửi"), status: false });
      }
      // Công nợ
      let findCustomerDebt = await Debt.find(this.req, {
        where:{createdBy: { in: ids }, deletedAt: 0}
      }).intercept({ name: 'UsageError' }, () => {
        return exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
      });
  
      if(findCustomerDebt.length > 0){
        return exits.success({ message: sails.__("Không thể xóa vì đã có công nợ"), status: false });
      }
      // Phiếu thu chi
      let findCustomerIncExp = await IncomeExpenseCard.find(this.req, {
        where:{createdBy: { in: ids }, branchId, deletedAt: 0}
      }).intercept({ name: 'UsageError' }, () => {
        return exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
      });
  
      if(findCustomerIncExp.length > 0){
        return exits.success({ message: sails.__("Không thể xóa vì đã có phiếu thu chi"), status: false });
      }
      // Kho
      let findStock = await Stock.find(this.req, {
        where:{createdBy: { in: ids }, branchId, deletedAt: 0}
      }).intercept({ name: 'UsageError' }, () => {
        return exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
      });
  
      if(findStock.length > 0){
        return exits.success({ message: sails.__("Không thể xóa vì đã tạo kho"), status: false });
      }
      // Phiếu kiểm kho
      let findStockCheckCard = await StockCheckCard.find(this.req, {
        where:{createdBy: { in: ids }, branchId, deletedAt: 0}
      }).intercept({ name: 'UsageError' }, () => {
        return exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
      });
  
      if(findStockCheckCard.length > 0){
        return exits.success({ message: sails.__("Không thể xóa vì đã có phiếu kiểm kho"), status: false });
      }
      // Phiếu chuyển kho
      let findMoveStockCard = await MoveStockCard.find(this.req, {
        where:{createdBy: { in: ids }, branchId, deletedAt: 0}
      }).intercept({ name: 'UsageError' }, () => {
        return exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
      });
  
      if(findMoveStockCard.length > 0){
        return exits.success({ message: sails.__("Không thể xóa vì đã có phiếu chuyển kho"), status: false });
      }
      // Phiếu sản xuất
      let findManuFacturingCard = await ManufacturingCard.find(this.req, {
        where:{createdBy: { in: ids }, branchId, deletedAt: 0}
      }).intercept({ name: 'UsageError' }, () => {
        return exits.success({ message: sails.__("Thông tin không hợp lệ"), status: false });
      });
  
      if(findManuFacturingCard.length > 0){
        return exits.success({ message: sails.__("Không thể xóa vì đã có phiếu sản xuất"), status: false });
      }
      
      let foundUser = await User.find(this.req, { id: { in: ids } })

      let deletedUsers = await User.update(this.req, { id: { in: ids } }).set({
        deletedAt: new Date().getTime(),
        updatedBy: this.req.loggedInUser.id
      }).intercept({ name: 'UsageError' }, () => {
        this.res.status(400).json({
          status: false,
          error: sails.__('Thông tin yêu cầu không hợp lệ')
        });
        return;
      }).fetch();
      
      let promises = [];
      for(let user of deletedUsers) {
        promises.push(User.update(this.req, {id: user.id}).set({
          email: user.email.replace('@', `_deleted_${new Date().getTime()}@`)
        }).fetch());
      }
      
      deletedUsers = await Promise.all(promises);

      // tạo nhật kí
      let createActionLog = await sails.helpers.actionLog.create(this.req, {
        userId: this.req.loggedInUser.id,
        functionNumber: sails.config.constant.ACTION_LOG_TYPE.USER,
        action: sails.config.constant.ACTION.DELETE,
        objectContentOld: foundUser.map(item => ({...item, password: "", avatar: ""})),
        objectContentNew: deletedUsers.map(item => ({...item[0], password: "", avatar: ""})),
        deviceInfo: { ip: this.req.ip, userAgent: this.req.headers['user-agent'] || "" },
        branchId: this.req.headers['branch-id']
      })

      if (!createActionLog.status) {
        return createActionLog
      }
  
      this.res.json({
        status: true,
        data: deletedUsers
      });
    }
  };
  