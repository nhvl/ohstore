module.exports = {
    description: 'Lấy danh sách cửa hàng',
  
    inputs: {
      filter: {
        type: "json"
      },
      sort: {
        type: 'json',
      },
      limit: {
        type: "number"
      },
      skip: {
        type: "number"
      },
    },
  
    fn: async function (inputs) {
      let { filter, sort, limit, skip, manualFilter, manualSort } = inputs;
  
      let foundData = await sails.helpers.control.storeList(this.req, { filter, sort, limit, skip, manualFilter, manualSort }) 

      return foundData;
    }
  };