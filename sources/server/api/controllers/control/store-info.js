module.exports = {
    description: 'Lấy thông tin cửa hàng',
  
    inputs: {
      filter: {
        type: "json"
      },
      sort: {
        type: 'json',
      },
      limit: {
        type: "number"
      },
      skip: {
        type: "number"
      },
    },
  
    fn: async function (inputs) {
      let { filter, sort, limit, skip, manualFilter, manualSort } = inputs;
  
      let foundData = await sails.helpers.control.storeInfo(this.req, { 
        storeId: parseFloat(this.req.params.storeId)
      }) 

      return foundData
    }
  };