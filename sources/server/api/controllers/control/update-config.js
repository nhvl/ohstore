module.exports = {
    description: 'Lấy thông tin cửa hàng',
  
    inputs: {
      language_product: {
        type: "number"
      },
      manufacturing_stock: {
        type: "number"
      },
      manufacturing: {
        type: "number"
      },
      is_active: {
        type: "number"
      },
      storeId: {
        type: "number"
      },
    },
  
    fn: async function (inputs) {
      let { language_product, manufacturing_stock, manufacturing, is_active, storeId } = inputs;
  
      let updatedData = await sails.helpers.control.updateConfig(this.req, { 
        language_product, manufacturing_stock, manufacturing, is_active,storeId
      }) 

      return updatedData
    }
  };