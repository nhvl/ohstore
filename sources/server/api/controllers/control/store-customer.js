module.exports = {
    description: 'Lấy danh sách cửa hàng',
  
    inputs: {
      storeId: {
        type: "number"
      },
      filter: {
        type: "json"
      },
      sort: {
        type: 'json',
      },
      limit: {
        type: "number"
      },
      skip: {
        type: "number"
      },
      type: {
        type: "number"
      },
      isBranch: {
        type: "ref"
      },
      select: {
        type: "json"
      }
    },
  
    fn: async function (inputs) {
      let { storeId, filter, sort, limit, skip, select, isBranch, branchId, type } = inputs;
  
      let foundData = await sails.helpers.control.storeCustomer(this.req, { storeId, filter, sort, limit, skip, select, isBranch, branchId, type });

      return foundData;
    }
  };