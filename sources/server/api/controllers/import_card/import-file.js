module.exports = {

  friendlyName: 'Import file',

  description: 'Importfile',

  inputs: {
    card: {
      type: "json"
    },
    product: {
      type: "json"
    },
    payment: {
      type: "json"
    },
    stopOnCodeDuplicateError: {
      type: "number"
    },
  },

  fn: async function (inputs) {
    let {
      card,
      product,
      payment,
      stopOnCodeDuplicateError,
    } = inputs;

    let result = await sails.helpers.import.importFile(this.req, {
      card,
      product,
      payment,
      stopOnCodeDuplicateError,
      branchId: this.req.headers['branch-id'],
      createdBy: this.req.loggedInUser.id,
    })

    return result;
  }
}