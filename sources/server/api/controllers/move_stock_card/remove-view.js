module.exports = {
  friendlyName: "Delete Multiple stock card",

  description: "Delete multiple stock card",

  inputs: {
    ids: {
      type: 'json',
      required: true
    },
  },

  fn: async function (inputs) {

    let { ids} = inputs;

    let branchId = this.req.headers['branch-id'] 

    let removeMoveStockCard = await sails.helpers.moveStockCard.removeView(this.req, {
      ids,
      branchId,
      updatedBy: this.req.loggedInUser.id
    })

    return removeMoveStockCard

  }
};