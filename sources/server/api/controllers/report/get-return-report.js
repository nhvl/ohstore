
module.exports = {

  friendlyName: 'Get Return Report Data',

  description: 'Get Return Report Data',

  inputs: {
    startDate: {
      type: "number"
    },
    endDate: {
      type: "number"
    },
    options: {
      type: "number"
    },
    selects: {
      type: "ref"
    }
  },

  fn: async function (inputs) {
    let { startDate, endDate, options, selects } = inputs;
    let branchId = this.req.headers['branch-id'];
    let reportReturn = await sails.helpers.report.getReturnReport(this.req, { startDate, endDate, options, branchId, selects })

    return reportReturn;

  }
};
