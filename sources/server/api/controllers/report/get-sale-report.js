
module.exports = {

  friendlyName: 'Get Dash board data',

  description: 'Get dash board data',

  inputs: {
    filter: {
      type: 'json',
    },
    startDate: {
      type: "number"
    },
    endDate: {
      type: "number"
    },
    options: {
      type: "number"
    },
    selects: {
      type: "ref"
    }
  },

  fn: async function (inputs) {
    let { filter, startDate, endDate, options, selects } = inputs;    
    let branchId = this.req.headers['branch-id'];
    let reportSale = await sails.helpers.report.getSaleReport(this.req ,{ filter, startDate, endDate, options, branchId, selects })

    return reportSale;

  }
};
