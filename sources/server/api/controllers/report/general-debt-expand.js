module.exports = {

  friendlyName: 'Get General Debt Expand',

  description: 'Get General Debt Expand',

  inputs: {
    customerId: {
      type: 'number',
      required: true
    },
    startDate: {
      type: "number"
    },
    endDate: {
      type: "number"
    },
  },

  fn: async function (inputs) {
    let { customerId, startDate, endDate } = inputs;
    let branchId = this.req.headers['branch-id'];

    let reportDebt = await sails.helpers.report.generalDebtExpand(this.req, { customerId, startDate, endDate, branchId });

    this.res.json(reportDebt)
  }
}