module.exports = {

  friendlyName: 'Get Product List From Sale Report',

  description: 'Get Product List From Sale Report',

  inputs: {
    isReturn: {
      type: "boolean"
    },
    options: {
      type: "number"
    }
  },

  fn: async function (inputs) {
    let { isReturn, options } = inputs;
    let branchId = this.req.headers['branch-id'];

    let getInvoiceList = await sails.helpers.report.expand2SaleReport(this.req, { isReturn, options, branchId, id: this.req.params.id });

    return getInvoiceList;

  }
};
