module.exports = {

  friendlyName: 'Get manufacturing Report',

  description: 'Get manufacturing report',

  inputs: {
    startDate: {
      type: "number"
    },
    endDate: {
      type: "number"
    },
    categories: {
      type: "ref"
    },
    selects: {
      type: "ref"
    },
  },

  fn: async function (inputs) {
    let { startDate, endDate, categories, selects } = inputs;
    let branchId = this.req.headers['branch-id'];
    let ManufacturingProductReport = await sails.helpers.report.manufacturingProduct(this.req, { startDate, endDate, branchId, categories, selects })

    this.res.json(ManufacturingProductReport);
  }
};
