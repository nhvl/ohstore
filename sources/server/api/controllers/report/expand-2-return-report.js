module.exports = {

  friendlyName: 'Get Product List From Return Report',

  description: 'Get Product List From Return Report',

  inputs: {
    options: {
      type: "number"
    }
  },

  fn: async function (inputs) {
    let { options } = inputs;
    let branchId = this.req.headers['branch-id'];

    let getInvoiceList = await sails.helpers.report.expand2ReturnReport(this.req, { options, branchId, id: this.req.params.id });

    return getInvoiceList;

  }
};
