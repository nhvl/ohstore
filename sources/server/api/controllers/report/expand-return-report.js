module.exports = {

  friendlyName: 'Get Invoice Return List From Report',

  description: 'Get Invoice Return List From Report',

  inputs: {
    filter: {
      type: "json"
    },
    options: {
      type: "number"
    }
  },

  fn: async function (inputs) {
    let { filter, options } = inputs;
    let branchId = this.req.headers['branch-id'];

    let getInvoiceList = await sails.helpers.report.expandReturnReport(this.req, { filter, options, branchId });

    return getInvoiceList;

  }
};
