module.exports = {

  friendlyName: 'Get Inventory Report data',

  description: 'Get Inventory data',

  inputs: {
    selectProduct: {
      type: 'json',
    },
    type: {
      type: 'json',
      required: true
    },
    stockId: { // truyen arr
      type: 'json',
      required: true 
    },
    startDate: {
      type: "number"
    },
    endDate: {
      type: "number"
    },
    skip: {
      type: "number"
    },
    limit: {
      type: "number"
    },
  },

  fn: async function (inputs) {
    let { type, stockId, startDate, endDate, selectProduct, skip, limit } = inputs;
    let branchId = this.req.headers['branch-id'];
    
    let ImportExportReport = await sails.helpers.report.importExport(this.req, { selectProduct, type, startDate, endDate, branchId, stockId, skip, limit })

    this.res.json(ImportExportReport);
  }
};
