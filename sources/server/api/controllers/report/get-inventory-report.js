module.exports = {

  friendlyName: 'Get Inventory Report data',

  description: 'Get Inventory data',

  inputs: {
    type: {
      type: 'json',
      required: true
    },
    stockId: { // truyen arr
      type: 'json',
      required: true
    },
    selectProduct: {
      type: 'json',
    },
    startDate: {
      type: "number"
    },
    endDate: {
      type: "number"
    },
    skip: {
      type: "number"
    },
    limit: {
      type: "number"
    },
  },

  fn: async function (inputs) {
    let { type, stockId, startDate, endDate, skip, limit, selectProduct } = inputs;
    let branchId = this.req.headers['branch-id'];

    let sumQuantity = '';
    let sumStockQuantity = '';
    let stockDetail = '';
    let stockQuantityList = [];
    let selectProducts = '';
    let skipLimit = "";

    if (selectProduct.length && type.length){
      selectProducts = ` AND (p.productTypeId IN (${type.join(',')}) OR p.id IN (${selectProduct.join(',')}))` 
    } else if (selectProduct.length && !type.length) {
      selectProducts = ` AND p.id IN (${selectProduct.join(',')})`
    } else if (!selectProduct.length && type.length) 
      selectProducts = ` AND p.productTypeId IN (${type.join(',')})`

    if (stockId.length) {
      let foundStock = await Stock.find(this.req, { id: { in: stockId }, deletedAt: 0 });

      if (!foundStock) {
        return exits.success({ status: false, message: sails.__(sails.config.constant.INTERCEPT.NOT_FOUND_STOCK) });
      }

      if (foundStock.length) {
        foundStock.map(stock => {
          stockQuantityList.push(`ps.` + sails.config.constant.STOCK_QUANTITY_LIST[stock.stockColumnIndex]);
        })
        sumQuantity = `(` + stockQuantityList.join(` + `) + `) as totalStockQuantity`;
        sumStockQuantity = `(` + stockQuantityList.join(` + `) + `)`;
        stockDetail = stockQuantityList.join(', ')
      }
    }

    if (limit) {
      skipLimit = ` limit ${limit} offset ${skip}`
    }
 
    let COUNT_PRODUCT_SQL = `SELECT COUNT(*) AS NumberOfProducts FROM product`

    let PRODUCT_STOCK_SQL = `SELECT ps.productId as productId, p.name, p.code, pu.name as unitName, ${stockDetail}, ps.manufacturingQuantity, pp.costUnitPrice, ${sumQuantity}, (pp.costUnitPrice * ${sumStockQuantity}) as total
                              from productstock ps
                              left join product p on p.id = ps.productId
                              left join productunit pu on pu.id = p.unitId
                              left join stock s on s.branchId = ps.branchId
                              left join productprice pp on pp.productId = p.id
                              where p.deletedAt = 0 and pp.branchId= ${branchId} AND ps.branchId= ${branchId}
                                and p.type = ${sails.config.constant.PRODUCT_TYPES.merchandise} ${selectProducts}
                              group by p.id
                              order by p.createdAt desc ${skipLimit}`;

    let TOTAL_PRODUCT_STOCK_SQL = `SELECT SUM(pp.costUnitPrice * ${sumStockQuantity}) as total
                              from productstock ps
                              left join product p on p.id = ps.productId
                              left join productprice pp on pp.productId = p.id
                              where p.deletedAt = 0 and pp.branchId= ${branchId} AND ps.branchId= ${branchId}
                                and p.type = ${sails.config.constant.PRODUCT_TYPES.merchandise} ${selectProducts}`;                          
    
    let [productstock, countProductstock, countTotal] = await Promise.all([
      sails.sendNativeQuery(this.req, PRODUCT_STOCK_SQL),
      sails.sendNativeQuery(this.req, COUNT_PRODUCT_SQL)
    ].concat((limit && skip === 0 ? [sails.sendNativeQuery(this.req, TOTAL_PRODUCT_STOCK_SQL)] : [])))

    let newArrData = {};

    _.forEach(productstock.rows, item => {
      newArrData[item.productId] = { ...newArrData[item.productId], ...item }
    })

    let totalAmountStock = 0;
    if (countTotal) totalAmountStock = countTotal.rows[0].total
    else totalAmountStock = _.reduce(Object.values(newArrData), (total, item) => total += item.total, 0)

    this.res.json({
      status: true,
      data: Object.values(newArrData),
      totalAmountStock: totalAmountStock ? totalAmountStock : 0,
      totalProductStock: countProductstock.rows[0].NumberOfProducts
    });
  }
};
