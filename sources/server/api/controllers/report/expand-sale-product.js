module.exports = {

  friendlyName: 'Get Invoice List From Report',

  description: 'Get Invoice List From Report',

  inputs: {
    filterInvoice: {
      type: "json"
    },
    filterInvoiceReturn: {
      type: "json"
    },
    options: {
      type: "number"
    }
  },

  fn: async function (inputs) {
    let { filterInvoice, filterInvoiceReturn, options } = inputs;
    let branchId = this.req.headers['branch-id'];
    
    let getInvoiceList = await sails.helpers.report.expandSaleProduct(this.req, { filterInvoice, filterInvoiceReturn, options, branchId });

    return getInvoiceList;

  }
};
