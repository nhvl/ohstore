module.exports = {
  friendlyName: "Get StockCheck Cards",

  description: "Get many stockcheck cards",

  inputs: {
    filter: {
      type: "json"
    },
    manualFilter: {
      type: "json"
    },
    manualSort: {
      type: "json"
    },
    sort: {
      type: 'string',
    },
    limit: {
      type: "number"
    },
    skip: {
      type: "number"
    },
    select: {
      type: "json"
    }
  },

  fn: async function (inputs) {
    let { filter, sort, limit, skip, manualFilter, manualSort, select } = inputs;
    let branchId = this.req.headers['branch-id'];

    filter = _.extend(filter || {}, { deletedAt: 0, branchId: branchId });
    
    let options = {
      select: select || [
        '*',
        "createdBy.fullName as userName",
        "stockId.name as stockName",
        "stockId.deletedAt as deleteStockAt",
        "st.realQuantity as realQuantity",
        "branchId.name as branchName",
        "st.differenceQuantity as differenceQuantity",
         "st.stockQuantity as stockQuantity"
      ],
      model: StockCheckCard, 
      filter: {...filter, ...manualFilter },
      customPopulates: `left join user createdBy on createdBy.id = m.createdBy left join stock stockId on stockId.id = m.stockId left join stockcheckcardproduct st on st.stockCheckCardId = m.id left join branch branchId on branchId.id = m.branchId`,
      limit,
      skip,
      sort: sort || 'updatedAt DESC',
      count: true
    };
  
    let {foundData, count} = await sails.helpers.customSendNativeQuery(this.req, options);

    this.res.json({
      status: true,
      data: foundData,
      count: count
    });
  }
};
