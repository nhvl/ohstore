module.exports = {
  friendlyName: "Get StockCheckCard",

  description: "Get one stockcheck card",

  fn: async function(inputs) {
    let branchId = this.req.headers['branch-id'];

    let stockCheckCard = await StockCheckCard.findOne(this.req, {
      where: { id: this.req.params.id }
    })
      .populate("stockCheckCardProducts")
      .intercept({ name: "UsageError" }, () => {
        return{
          status: false,
          error: sails.__("Thông tin không hợp lệ")
        };
      });
    
    let checkBanch = await sails.helpers.checkBranch(stockCheckCard.branchId, this.req);

    if(!checkBanch){
      return{
        status: false,
        error: sails.__('Không có quyền thực hiện thao tác này')
      };
    }
    else{
      let createdBy = await User.findOne(this.req, {
        where: { id: stockCheckCard.createdBy },
        select: ["id", "fullName"]
      })

      if (createdBy) stockCheckCard.createdBy = createdBy;

      let fail = [];
      for (let i = 0; i < stockCheckCard.stockCheckCardProducts.length; i++) {
        let [foundProduct, foundProductStock] = await Promise.all([
          Product.findOne(this.req, { where: {id: stockCheckCard.stockCheckCardProducts[i].productId},}).populate("unitId"),
          sails.helpers.product.getQuantity(this.req, {productId: stockCheckCard.stockCheckCardProducts[i].productId, branchId: branchId})
        ])
    
        if (!foundProduct) {
          fail.push('error');
        }

        foundProduct = _.extend({
          ...foundProduct,
        }, _.pick(foundProductStock, ['stockMin', 'manufacturingQuantity', ...Object.values(sails.config.constant.STOCK_QUANTITY_LIST)]))

        stockCheckCard.stockCheckCardProducts[i].product = foundProduct;
      }

      if(fail.length){
        return{
          status: false,
          error: sails.__('Sản phẩm không tồn tại trong hệ thống')
        };
      }
      else
        return{
          status: true,
          data: stockCheckCard
        };
    }
  }
};
