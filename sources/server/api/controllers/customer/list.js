module.exports = {

  friendlyName: 'Get Customer',

  description: 'Get list of customers',

  inputs: {
    filter: {
      type: "json"
    },
    manualFilter: {
      type: "json"
    },
    manualSort: {
      type: "json"
    },
    sort: {
      type: 'string',
    },
    limit: {
      type: "number"
    },
    skip: {
      type: "number"
    },
    isBranch: {
      type: "ref"
    },
    select: {
      type: "json"
    },
    isNotGetDefault: {
      type: "ref"
    },
  },

  fn: async function (inputs) {
    let { filter, sort, limit, skip, manualFilter, manualSort, isBranch, select, isNotGetDefault } = inputs;
    
    let type = parseInt(this.req.params.type);
    let branchId = this.req.headers['branch-id'];    

    let customers = await sails.helpers.customer.list(this.req, { filter, sort, limit, skip, manualFilter, manualSort, select, isBranch, branchId, type, isNotGetDefault })

    return customers;
  }
};
