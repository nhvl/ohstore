module.exports = {

  friendlyName: 'Update Product Type',

  description: 'Update Product Type.',

  inputs: {
    name: {
      required: true,
      type: 'string',
      maxLength: 255,
    },

    notes: {
      type: 'string',
      maxLength: 255,
    },

  },

  fn: async function (inputs) {
    var { name, notes } = inputs;


    let foundProductType = await ProductType.findOne(this.req, {
      where: { id: this.req.params.id, deletedAt: 0 }
    }).intercept({ name: 'UsageError' }, () => {
      this.res.json({
        status: false,

        error: sails.__('Thông tin yêu cầu không hợp lệ')
      });
      return;
    });

    let foundProductTypes = await ProductType.find(this.req, { id: { '!=': foundProductType.id }, deletedAt: 0 })

    let index = foundProductTypes.filter(item => item.name.toLowerCase() === name.toLowerCase())

    if (index.length > 0) {
      this.res.json({
        status: false,
        error: sails.__('Tên nhóm sản phẩm đã tồn tại')
      });
      return;
    }

    let updateProductType = await ProductType.update(this.req, { id: foundProductType.id }).set({
      name,
      notes
    }).fetch();

    
    //tạo nhật kí
    let createActionLog = await sails.helpers.actionLog.create(this.req, {
      userId: this.req.loggedInUser.id,
      functionNumber: sails.config.constant.ACTION_LOG_TYPE.PRODUCT_TYPE,
      action: sails.config.constant.ACTION.UPDATE,
      objectId: this.req.params.id,
      objectContentOld: foundProductType,
      objectContentNew: updateProductType[0],
      deviceInfo: { ip: this.req.ip, userAgent: this.req.headers['user-agent'] || "" },
      branchId: this.req.headers['branch-id']
    })

    if (!createActionLog.status) {
      return createActionLog
    }

    this.res.json({
      status: true,
      data: updateProductType[0]
    });
  }
};
