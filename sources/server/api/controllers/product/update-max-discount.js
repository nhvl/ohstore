module.exports = {


  friendlyName: 'Update Max Discount Of Product',


  description: 'Update max discount of product.',

  inputs: {
    id: {
      type: 'number'
    },
    maxDiscount: {
      type: 'string'
    },
  },

  fn: async function (inputs) {
    let {id, maxDiscount} = inputs;

    let result = await sails.helpers.product.updateMaxDiscount(this.req, {id, maxDiscount, createdBy: this.req.loggedInUser.id})
    return result;
  }

};