module.exports = {

  friendlyName: 'Get Products Stock',

  description: 'Get list of products stock',

  inputs: {
  },

  fn: async function (inputs) {
    let branchId = this.req.headers['branch-id'];

    let foundProducts = await sails.helpers.product.listProductStock(this.req, { branchId });

    this.res.json(foundProducts);
  }
};