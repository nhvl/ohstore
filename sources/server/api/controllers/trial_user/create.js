module.exports = {

  friendlyName: 'Create Trial User',

  inputs: {
    phoneNumber: {
      required: true,
      type: 'string',
    },

  },


  fn: async function (inputs) {
    const {
      phoneNumber
    } = inputs;
    
    let createdUser = await sails.helpers.trialUser.create({
      phoneNumber
    });

    return createdUser;
  }

};
