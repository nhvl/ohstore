let fs = require("fs");
const path = require('path');
let pathImage = path.join("assets/images", "ohstore_logo.png");

module.exports = {

  friendlyName: 'Get Print Template',

  description: 'Get type of print template',

  inputs: {
    data: {
      type: "json"
    },
    type: {
      type: "string"
    }
  },

  fn: async function (inputs) {
    let { data, type } = inputs;
    let configStore = await StoreConfig.find(this.req, { where: { type: { in: ["store_logo", "print_logo", "store_info", `print_template_${type}`, "print_debt"] } } })

    if (!configStore) {
      this.res.json({
        status: false,
        error: sails.__("Không tìm thấy thông tin cửa hàng")
      })
    }
    
    let templateHTMLContent = "";
    let template;
    let isPrintDebt = Number(configStore.find(item => item.type === "print_debt")['value'])

    for (let item of configStore) {

      if (item.type === `print_template_${type}`) {
        template = JSON.parse(item.value)
        templateHTMLContent = template[template.default] || "A4"
        templateHTMLContent = template.default === 'K80' && data.store_counter ? templateHTMLContent.split("<!--</store_counter>-->")[0] + templateHTMLContent.split("<!--</store_counter>-->")[2] : templateHTMLContent;
        let dataImage = fs.readFileSync(pathImage, { encoding:"base64" })

        if (dataImage) {
          dataImage = "data:image/png;base64," + dataImage
        }
        data = {
          ...data,
          store_logo: dataImage ? `<img style="height: ${sails.config.constant.LOGO_SIZE[template.default].height}px;max-width: ${sails.config.constant.LOGO_SIZE[template.default].maxWidth}px;" alt="Chưa có logo" src="${dataImage}" />` : "",
          print_logo: dataImage ? `<img style="height: ${sails.config.constant.LOGO_SIZE[template.default].height}px;max-width: ${sails.config.constant.LOGO_SIZE[template.default].maxWidth}px;" alt="Chưa có logo" src="${dataImage}" />` : "",
        }
      }

      if (item.type === "store_logo" && templateHTMLContent !== "") {
        data = {
          ...data,
          store_logo: item.value ? `<img style="height: ${sails.config.constant.LOGO_SIZE[template.default].height}px;max-width: ${sails.config.constant.LOGO_SIZE[template.default].maxWidth}px;" alt="Chưa có logo" src=${item.value} />`: ""
        }
      }
      if (item.type === "store_info") {
        let dataInfo = JSON.parse(item.value);
        data = {
          ...data,
          store_name: dataInfo.name || "",
          store_address: dataInfo.address || "",
          store_phone_number: dataInfo.tel || "",
          store_email: dataInfo.email || "",

        }
      }

      if (item.type === "print_logo" && templateHTMLContent !== "") {
        data = {
          ...data,
          print_logo: item.value ? `<img style="height: ${sails.config.constant.LOGO_SIZE[template.default].height}px;max-width: ${sails.config.constant.LOGO_SIZE[template.default].maxWidth}px;" alt="Chưa có logo" src=${item.value} />`: ""
        }
      }     

    }
    templateHTMLContent = data.customer_name !== 'Khách lẻ' && isPrintDebt ? templateHTMLContent : templateHTMLContent.split("<!--</walk-in guest>-->")[0] + (templateHTMLContent.split("<!--</walk-in guest>-->")[2] || "");
    
    if( !((type === 'incomeexponse_payment' || type === 'incomeexponse_receipt') && (data.type_code === sails.config.constant.DEFAULT_INCOME_EXPENSE_CARD_TYPES.OTHER_INCOME.code || data.type_code === sails.config.constant.DEFAULT_INCOME_EXPENSE_CARD_TYPES.OTHER_EXPENSE.code)) ){
      templateHTMLContent = templateHTMLContent.split("<!--<explain>-->")[0] + (templateHTMLContent.split("<!--<explain>-->")[2] || "");
    }
    
    let printContent = sails.helpers.replaceHtmlEditor(data, templateHTMLContent);

    this.res.json({
      status: true,
      data: printContent
    })
  }
};
