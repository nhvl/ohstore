module.exports = {

    friendlyName: 'Get Config',
  
    description: 'Get store config.',
  
    inputs: {
      types: {
        type: 'ref'
      }
    },
  
    fn: async function (inputs) {
      let { types } = inputs
      let foundConfig = await sails.helpers.storeConfig.get(this.req, {
        types
      })
      
      return foundConfig;
    }
  };
  
